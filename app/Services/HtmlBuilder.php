<?php

namespace App\Services;

class HtmlBuilder extends \Collective\Html\HtmlBuilder 
{
    public function icon($class) 
    {
        return "<i class='$class'></i>";
    }

    /**
     * Generate a HTML link.
     *
     * @param string $url
     * @param string $title
     * @param array  $attributes
     * @param bool   $secure
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function link($url, $title = null, $attributes = [], $secure = null)
    {
        $url = $this->url->to($url, [], $secure);

        if (is_null($title) || $title === false) {
            $title = $url;
        }

        return $this->toHtmlString('<a href="' . $url . '"' . $this->attributes($attributes) . '>' . $title . '</a>');
    }

    public function viewLabel($model, $field, $value = null)
    {
        return '<div class="form-group">
                <b class="control-label col-md-4">'.$model->label($field).'</b>
                <div class="col-md-8">: '.(isset($value) ? $value : $model->{$field}).'</div>
            </div>';
    }
}