<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Vendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vendir';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change base directory project from vendor composer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Updating baseDir path\r\n";
        $composer_dir = VENDOR_DIR."/composer";

        foreach (glob($composer_dir."/*.php") as $filepath) {
            $filename = basename($filepath);

            if (!in_array($filename, [
                "autoload_classmap.php", 
                "autoload_files.php", 
                "autoload_namespaces.php", 
                "autoload_psr4.php"])
            )
                continue;

            $open = fopen($filepath, "r+");

            if ($open == false)
                exit("Could not open file '$filepath'");

            // loop through file
            while (!feof($open)) {
                $str = fgets($open);

                // replace $baseDir value
                if (preg_match('/\\$baseDir/', $str)) {
                    $len = strlen($str);
                    fseek($open, ftell($open)-$len);
                    fwrite($open, str_pad("\$baseDir = TENANT_DIR;\r\n", strlen($str)));
                    break;
                }
            }

            fclose($open);
        }

        echo "done\r\n";
    }
}
