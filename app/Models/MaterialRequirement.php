<?php

namespace App\Models;

class MaterialRequirement extends Model
{
    protected $table = "material_requirement";

    public $timestamps = false;

    protected $fillable = [
        "material_name",
        "qty",
    ];

    public static $rules = [
        "material_name" => "max:24",
        "qty" => "numeric",
    ];
}
