<?php

namespace App\Models;

class Customer extends Model
{
    protected $table = "customer";

    public $timestamps = false;

    protected $fillable = [
        "address",
        "contact_name",
        "cust_name",
        "description",
        "fax",
        "mail",
        "npwp",
        "phone",
        "post_code",
    ];

    public static $rules = [
        "address" => "max:48",
        "contact_name" => "max:16",
        "cust_name" => "max:24",
        "description" => "max:32",
        "fax" => "max:13",
        "mail" => "max:24",
        "npwp" => "max:20",
        "phone" => "max:13",
    ];

    public function attributeLabels()
    {
        return [
            "address" => "Alamat",
            "contact_name" => "Nama Kontak",
            "cust_name" => "Nama",
            "description" => "Deskripsi",
            "fax" => "Fax",
            "mail" => "Email",
            "npwp" => "NPWP",
            "phone" => "Phone",
            "post_code" => "Kode Pos",
        ];
    }
}
