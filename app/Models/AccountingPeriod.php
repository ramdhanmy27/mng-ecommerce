<?php

namespace App\Models;

class AccountingPeriod extends Model
{
    protected $table = "accounting_period";

    protected $primaryKey = "year";

    public $timestamps = false;

    protected $fillable = [
        "description",
    ];

    public static $rules = [
        "description" => "max:32",
    ];
}
