<?php

namespace App\Models;

class BankOffice extends Model
{
    protected $table = "bank_office";

    public $timestamps = false;

    protected $fillable = [
        "address",
        "contact_person",
        "phone",
    ];

    public static $rules = [
        "address" => "max:255",
        "contact_person" => "max:16",
        "phone" => "max:14",
    ];
}
