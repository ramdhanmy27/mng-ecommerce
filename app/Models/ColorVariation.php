<?php

namespace App\Models;

class ColorVariation extends Model
{
    protected $table = "color_variation";

    public $timestamps = false;

    protected $fillable = [
        "color_name",
    ];

    public static $rules = [
        "color_name" => "max:24",
    ];
}
