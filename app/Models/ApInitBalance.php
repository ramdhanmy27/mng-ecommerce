<?php

namespace App\Models;

class ApInitBalance extends Model
{
    protected $table = "ap_init_balance";

    public $timestamps = false;

    protected $fillable = [
        "init_value",
    ];

    public static $rules = [
        "init_value" => "numeric",
    ];
}
