<?php

namespace App\Models;

class MaterialTransaction extends Model
{
    protected $table = "material_transaction";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "reff_no",
        "trans_date",
        "trans_no",
        "trans_type",
    ];

    public static $rules = [
        "description" => "max:24",
        "reff_no" => "max:10",
        "trans_no" => "max:10",
    ];
}
