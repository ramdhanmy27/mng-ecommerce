<?php

namespace App\Models;

class ReadyProduct extends Model
{
    protected $table = "ready_product";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "prod_name",
        "total_qty",
    ];

    public static $rules = [
        "description" => "max:32",
        "prod_name" => "max:255",
    ];
}
