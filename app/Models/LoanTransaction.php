<?php

namespace App\Models;

class LoanTransaction extends Model
{
    protected $table = "loan_transaction";

    protected $primaryKey = "trans_no";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "trans_date",
        "trans_type",
        "trans_value",
    ];

    public static $rules = [
        "description" => "max:32",
        "trans_value" => "numeric",
    ];
}
