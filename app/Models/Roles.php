<?php

namespace App\Models;

class Roles extends Model
{
    protected $table = "roles";

    public $timestamps = false;

    protected $fillable = [
        "created_at",
        "description",
        "display_name",
        "name",
        "updated_at",
    ];

    public static $rules = [
        "description" => "max:255",
        "display_name" => "max:255",
        "name" => "required|max:255",
    ];
}
