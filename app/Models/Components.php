<?php

namespace App\Models;

class Components extends Model
{
    protected $table = "components";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_has_detail",
        "is_helper_material",
        "partname",
        "required_qty",
    ];

    public static $rules = [
        "description" => "max:32",
        "partname" => "max:24",
    ];
}
