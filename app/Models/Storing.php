<?php

namespace App\Models;

class Storing extends Model
{
    protected $table = "storing";

    public $timestamps = false;

    protected $fillable = [
        "is_picking",
        "pack_qty",
        "qty",
        "store_date",
    ];

    public static $rules = [
        "qty" => "numeric",
    ];
}
