<?php

namespace App\Models;

class UnstoredStock extends Model
{
    protected $table = "unstored_stock";

    public $timestamps = false;

    protected $fillable = [
        "pack_qty",
        "qty",
    ];

    public static $rules = [
        "qty" => "numeric",
    ];
}
