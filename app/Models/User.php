<?php 

namespace App\Models;

// use App\Models\Authenticatable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    protected $fillable = ["name", "email", "password", "kode_penyedia"];

    protected $hidden = ["password", "remember_token"];

    public static $rules = [
        "name" => "required|max:255",
        "email" => "required|email|max:255",
    ];

    public function registerRoles(array $roles) 
    {
        $data = [];
        $id = $this->getKey();

        foreach ($roles as $role_id)
            $data[] = ["role_id" => $role_id, "user_id" => $id];

        DB::table("role_user")->where("user_id", $id)->delete();
        DB::table("role_user")->insert($data);
    }
}