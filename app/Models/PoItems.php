<?php

namespace App\Models;

class PoItems extends Model
{
    protected $table = "po_items";

    public $timestamps = false;

    protected $fillable = [
        "item_name",
        "po_number",
        "qty",
        "subtotal",
        "unit_price",
        "unit_code",
        "material_id",
    ];

    public static $rules = [
        "item_name" => "max:16",
        "qty" => "integer",
        "subtotal" => "numeric",
        "unit_price" => "numeric",
    ];

    protected $attributes = [
        "qty" => 0,
        "subtotal" => 0,
        "unit_price" => 0,
    ];

    public function attributeLabels()
    {
        return [
            "item_name" => "Name",
            "qty" => "Qty",
            "subtotal" => "Sub Total",
            "unit_price" => "Price",
            "unit_code" => "Unit",
            "material_id" => "Material",

            "unit_name" => "Unit",
        ];
    }
}
