<?php

namespace App\Models;

class GlAccount extends Model
{
    protected $table = "gl_account";

    protected $primaryKey = "account_code";

    public $timestamps = false;

    protected $fillable = [
        "account_name",
        "balance",
        "is_sys_account",
    ];

    public static $rules = [
        "account_name" => "max:24",
        "balance" => "numeric",
    ];
}
