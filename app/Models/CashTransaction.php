<?php

namespace App\Models;

class CashTransaction extends Model
{
    protected $table = "cash_transaction";

    protected $primaryKey = "trans_no";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "party_name",
        "trans_date",
        "trans_type",
        "trans_value",
    ];

    public static $rules = [
        "description" => "max:32",
        "party_name" => "max:16",
        "trans_value" => "numeric",
    ];
}
