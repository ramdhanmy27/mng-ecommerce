<?php

namespace App\Models;

class ArBill extends Model
{
    protected $table = "ar_bill";

    public $timestamps = false;

    protected $fillable = [
        "bill_date",
        "bill_no",
        "bill_value",
        "description",
        "due_date",
        "is_paid",
    ];

    public static $rules = [
        "bill_no" => "max:12",
        "bill_value" => "numeric",
        "description" => "max:36",
    ];
}
