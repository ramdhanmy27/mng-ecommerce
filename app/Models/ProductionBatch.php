<?php

namespace App\Models;

class ProductionBatch extends Model
{
    protected $table = "production_batch";

    public $timestamps = false;

    protected $fillable = [
        "batch_no",
        "mfg_finish_date",
        "mfg_start_date",
        "product_name",
        "total_qty",
    ];

    public static $rules = [
        "batch_no" => "max:10",
        "product_name" => "max:24",
    ];
}
