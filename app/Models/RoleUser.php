<?php

namespace App\Models;

class RoleUser extends Model
{
    protected $table = "role_user";

    protected $primaryKey = "user_id";

    public $timestamps = false;

    protected $fillable = [];

    public static $rules = [
        "description" => "max:36",
        "rcv_number" => "max:10",
    ];
}
