<?php

namespace App\Models;

class CashAccount extends Model
{
    protected $table = "cash_account";

    protected $primaryKey = "account_code";

    public $timestamps = false;

    protected $fillable = [
        "account_name",
        "cash_type",
        "currency",
        "description",
    ];

    public static $rules = [
        "account_name" => "max:16",
        "description" => "max:32",
    ];
}
