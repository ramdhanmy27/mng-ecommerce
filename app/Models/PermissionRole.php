<?php

namespace App\Models;

class PermissionRole extends Model
{
    protected $table = "permission_role";

    protected $primaryKey = "role_id";

    public $timestamps = false;

    protected $fillable = [];

    public static $rules = [
        "created_at" => "required",
        "email" => "required|max:255",
        "token" => "required|max:255",
    ];
}
