<?php

namespace App\Models;

class Warehouse extends Model
{
    protected $table = "warehouse";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "height",
        "length",
        "location",
        "wh_code",
        "wh_name",
        "width",
    ];

    public static $rules = [
        "description" => "max:32",
        "location" => "max:24",
        "wh_name" => "max:16",
    ];

    public function attributeLabels() 
    {
        return [
            "description" => "Description",
            "height" => "Height",
            "length" => "Length",
            "location" => "Location",
            "wh_code" => "Kode",
            "wh_name" => "Name",
            "width" => "Width",
        ];
    }
}
