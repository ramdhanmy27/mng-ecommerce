<?php

namespace App\Models;

class PackingBox extends Model
{
    protected $table = "packing_box";

    public $timestamps = false;

    protected $fillable = [
        "total_qty",
    ];

    public static $rules = [];
}
