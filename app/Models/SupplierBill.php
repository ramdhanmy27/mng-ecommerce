<?php

namespace App\Models;

class SupplierBill extends Model
{
    protected $table = "supplier_bill";

    public $timestamps = false;

    protected $fillable = [
        "attached_file",
        "bill_date",
        "bill_no",
        "currency",
        "description",
        "due_date",
        "payment_type",
        "total_value",
    ];

    public static $rules = [
        "attached_file" => "max:30",
        "bill_no" => "max:16",
        "description" => "max:32",
        "total_value" => "numeric",
    ];
}
