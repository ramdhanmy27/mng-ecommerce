<?php

namespace App\Models;

class ChequeInHand extends Model
{
    protected $table = "cheque_in_hand";

    public $timestamps = false;

    protected $fillable = [
        "account_owner",
        "cheque_date",
        "cheque_value",
        "giver_name",
        "is_bilyet",
        "serial_no",
        "status",
    ];

    public static $rules = [
        "account_owner" => "max:16",
        "cheque_value" => "numeric",
        "giver_name" => "max:255",
        "serial_no" => "max:12",
    ];
}
