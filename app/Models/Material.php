<?php

namespace App\Models;

class Material extends Model
{
    protected $table = "material";

    public $timestamps = false;

    protected $fillable = [
        "attr1_value",
        "attr2_value",
        "attr3_value",
        "attr4_value",
        "attr5_value",
        "description",
        "mat_code",
        "unit_code",
        "mat_name",
        "grp_code",
    ];

    public static $rules = [
        "grp_code" => "required",
        "attr1_value" => "max:20",
        "attr2_value" => "max:20",
        "attr3_value" => "max:20",
        "attr4_value" => "max:20",
        "attr5_value" => "max:20",
        "description" => "max:32",
        "mat_code" => "required|max:12",
        "mat_name" => "required|max:24",
    ];

    public function attributeLabels()
    {
        return [
            "grp_code" => "Grup Material",
            "mat_code" => "Kode Barang",
            "mat_name" => "Nama Barang",
            "unit_code" => "Unit",
            "attr1_value" => "Atribut 1",
            "attr2_value" => "Atribut 2",
            "attr3_value" => "Atribut 3",
            "attr4_value" => "Atribut 4",
            "attr5_value" => "Atribut 5",
            "description" => "Deskripsi",
        ];
    }
}
