<?php

namespace App\Models;

class RackSlot extends Model
{
    protected $table = "rack_slot";

    public $timestamps = false;

    protected $fillable = [
        "capacity",
        "shelf_level",
        "slot_code",
    ];

    public static $rules = [];
}
