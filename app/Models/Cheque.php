<?php

namespace App\Models;

class Cheque extends Model
{
    protected $table = "cheque";

    public $timestamps = false;

    protected $fillable = [
        "cheque_value",
        "holder_name",
        "serial_no",
        "status",
    ];

    public static $rules = [
        "cheque_value" => "numeric",
        "holder_name" => "max:16",
        "serial_no" => "max:12",
    ];
}
