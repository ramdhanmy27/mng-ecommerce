<?php

namespace App\Models;

class LoanAccount extends Model
{
    protected $table = "loan_account";

    public $timestamps = false;

    protected $fillable = [
        "account_name",
        "account_no",
        "balance",
        "currency",
        "description",
    ];

    public static $rules = [
        "account_name" => "max:24",
        "account_no" => "max:16",
        "balance" => "numeric",
        "description" => "max:255",
    ];
}
