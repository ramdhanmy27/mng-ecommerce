<?php

namespace App\Models;

class Brand extends Model
{
    protected $table = "brand";

    public $timestamps = false;

    protected $fillable = [
        "brand_name",
        "description",
    ];

    public static $rules = [
        "brand_name" => "max:16",
        "description" => "max:32",
    ];
}
