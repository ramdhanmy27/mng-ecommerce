<?php

namespace App\Models;

class MaterialGroup extends Model
{
    protected $table = "material_group";

    public $timestamps = false;

    protected $fillable = [
        "attr1_name",
        "attr2_name",
        "attr3_name",
        "attr4_name",
        "attr5_name",
        "grp_code",
        "grp_name",
    ];

    public static $rules = [
        "attr1_name" => "max:12",
        "attr2_name" => "max:12",
        "attr3_name" => "max:12",
        "attr4_name" => "max:12",
        "attr5_name" => "max:12",
        "grp_name" => "max:16",
    ];

    public function attributeLabels()
    {
        return [
            "grp_code" => "Kode",
            "grp_name" => "Nama",
            "attr1_name" => "Nama Attribut 1",
            "attr2_name" => "Nama Attribut 2",
            "attr3_name" => "Nama Attribut 3",
            "attr4_name" => "Nama Attribut 4",
            "attr5_name" => "Nama Attribut 5",
        ];
    }

    public function beforeSave()
    {
        $this->more_attr_cnt = count(array_filter([
            $this->attr1_name,
            $this->attr2_name,
            $this->attr3_name,
            $this->attr4_name,
            $this->attr5_name,
        ]));
    }
}
