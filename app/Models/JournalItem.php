<?php

namespace App\Models;

class JournalItem extends Model
{
    protected $table = "journal_item";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "journal_value",
    ];

    public static $rules = [
        "description" => "max:24",
        "journal_value" => "numeric",
    ];
}
