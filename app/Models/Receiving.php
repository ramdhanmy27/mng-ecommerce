<?php

namespace App\Models;

class Receiving extends Model
{
    protected $table = "receiving";

    public $timestamps = false;

    protected $fillable = [
        "po_number",
        "rcv_number",
        "rcv_date",
        "description",
    ];

    public static $rules = [
        "po_number" => "required",
        "rcv_number" => "required|string|max:10|unique:receiving,rcv_number",
        "rcv_date" => "required|date",
        "description" => "max:36",
    ];

    public function attributeLabels()
    {
        return [
            "po_number" => "PO Number",
            "rcv_number" => "Number",
            "rcv_date" => "Date",
            "description" => "Description",
        ];
    }
}
