<?php

namespace App\Models;

class ProductVariation extends Model
{
    protected $table = "product_variation";

    public $timestamps = false;

    protected $fillable = [
        "qty",
        "size",
    ];

    public static $rules = [];
}
