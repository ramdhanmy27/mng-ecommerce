<?php

namespace App\Models;

class Migrations extends Model
{
    protected $table = "migrations";

    public $timestamps = false;

    protected $fillable = [
        "batch",
        "migration",
    ];

    public static $rules = [
        "batch" => "required",
        "migration" => "required|max:255",
    ];
}
