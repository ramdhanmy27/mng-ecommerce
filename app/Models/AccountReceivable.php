<?php

namespace App\Models;

class AccountReceivable extends Model
{
    protected $table = "account_receivable";

    protected $primaryKey = "ar_code";

    public $timestamps = false;

    protected $fillable = [
        "ar_name",
        "balance",
        "curr",
    ];

    public static $rules = [
        "ar_name" => "max:16",
        "balance" => "numeric",
    ];
}
