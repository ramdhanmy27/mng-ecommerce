<?php

namespace App\Models;

class BankAccount extends Model
{
    protected $table = "bank_account";

    protected $primaryKey = "account_code";

    public $timestamps = false;

    protected $fillable = [
        "account_name",
        "account_number",
        "currency",
        "description",
    ];

    public static $rules = [
        "account_name" => "max:24",
        "account_number" => "max:16",
        "description" => "max:32",
    ];
}
