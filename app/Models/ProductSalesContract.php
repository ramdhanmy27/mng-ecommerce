<?php

namespace App\Models;

class ProductSalesContract extends Model
{
    protected $table = "product_sales_contract";

    public $timestamps = false;

    protected $fillable = [
        "attached_file",
        "billing_address",
        "buyer_name",
        "currency",
        "delivery_address",
        "description",
        "due_date",
        "payment_type",
        "reff_number",
        "sc_date",
        "sc_number",
        "total_value",
    ];

    public static $rules = [
        "attached_file" => "max:30",
        "billing_address" => "max:32",
        "buyer_name" => "max:16",
        "delivery_address" => "max:32",
        "description" => "max:32",
        "reff_number" => "max:12",
        "sc_number" => "max:12",
        "total_value" => "numeric",
    ];
}
