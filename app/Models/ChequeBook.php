<?php

namespace App\Models;

class ChequeBook extends Model
{
    protected $table = "cheque_book";

    protected $primaryKey = "serial_no";

    public $timestamps = false;

    protected $fillable = [
        "is_activated",
        "is_bilyet",
        "page_count",
    ];

    public static $rules = [];
}
