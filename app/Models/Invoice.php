<?php

namespace App\Models;

class Invoice extends Model
{
    protected $table = "invoice";

    public $timestamps = false;

    protected $fillable = [
        "currency",
        "description",
        "due_date",
        "invoice_date",
        "invoice_no",
        "payment_bank_account",
        "payment_type",
        "total_value",
    ];

    public static $rules = [
        "description" => "max:32",
        "invoice_no" => "max:12",
        "payment_bank_account" => "max:48",
        "total_value" => "numeric",
    ];
}
