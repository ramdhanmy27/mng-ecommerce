<?php

namespace App\Models;

class BoxContent extends Model
{
    protected $table = "box_content";

    public $timestamps = false;

    protected $fillable = [
        "qty",
    ];

    public static $rules = [];
}
