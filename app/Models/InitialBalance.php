<?php

namespace App\Models;

class InitialBalance extends Model
{
    protected $table = "initial_balance";

    public $timestamps = false;

    protected $fillable = [
        "init_value",
    ];

    public static $rules = [
        "init_value" => "numeric",
    ];
}
