<?php

namespace App\Models;

class Journal extends Model
{
    protected $table = "journal";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "journal_date",
    ];

    public static $rules = [
        "description" => "max:32",
    ];
}
