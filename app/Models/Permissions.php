<?php

namespace App\Models;

class Permissions extends Model
{
    protected $table = "permissions";

    public $timestamps = false;

    protected $fillable = [
        "created_at",
        "description",
        "display_name",
        "name",
        "updated_at",
    ];

    public static $rules = [
        "description" => "max:255",
        "display_name" => "max:255",
        "name" => "required|max:255",
    ];
}
