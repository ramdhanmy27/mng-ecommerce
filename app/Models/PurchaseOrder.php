<?php

namespace App\Models;

class PurchaseOrder extends Model
{
    const STATUS_OPEN = 0;
    const STATUS_CLOSE = 1;

    const METHOD_TRANSFER = 0;
    const METHOD_CASH = 1;

    const TYPE_JASA = 0;
    const TYPE_BARANG = 1;

    protected $table = "purchase_order";

    public $timestamps = false;

    protected $fillable = [
        "po_number",
        "currency",
        "deliv_address",
        "deliv_due_date",
        "description",
        "pay_method",
        "po_date",
        "po_status",
        "po_type",
        "total_value",
    ];

    protected $attributes = [
        "currency" => "IDR",
        "po_status" => 0,
        "total_value" => 0,
    ];

    public static $rules = [
        "po_number" => "required|string|max:12|unique:purchase_order,po_number",
        "deliv_due_date" => "required|date",
        "currency" => "required|string|max:3",
        "pay_method" => "required",
        "po_type" => "required",
        "po_date" => "required|date",
        "deliv_address" => "required|string|max:32",
        "description" => "string|max:32",
        "total_value" => "numeric",
    ];

    public function attributeLabels()
    {
        return [
            "currency" => "Currency",
            "deliv_address" => "Address",
            "deliv_due_date" => "Due Date",
            "description" => "Description",
            "pay_method" => "Pay Method",
            "po_date" => "Date",
            "po_status" => "Status",
            "po_type" => "Type",
            "total_value" => "Total Value",
            "supplier_id" => "Supplier",
        ];
    }

    public function config($index)
    {
        return array_get([
            "status" => [
                self::STATUS_OPEN => "Open",
                self::STATUS_CLOSE => "Closed",
            ],
            "type" => [
                self::TYPE_JASA => "Jasa",
                self::TYPE_BARANG => "Barang",
            ],
            "method" => [
                self::METHOD_TRANSFER => "Transfer",
                self::METHOD_CASH => "Cash",
            ],
        ], $index);
    }
}
