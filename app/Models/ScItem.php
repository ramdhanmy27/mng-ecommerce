<?php

namespace App\Models;

class ScItem extends Model
{
    protected $table = "sc_item";

    public $timestamps = false;

    protected $fillable = [
        "item_name",
        "qty",
        "sub_total",
        "unit_price",
    ];

    public static $rules = [
        "item_name" => "max:16",
        "sub_total" => "numeric",
        "unit_price" => "numeric",
    ];
}
