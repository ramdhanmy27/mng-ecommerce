<?php

namespace App\Models;

class BillOfMaterial extends Model
{
    protected $table = "bill_of_material";

    public $timestamps = false;

    protected $fillable = [
        "bo_m_no",
        "description",
        "is_multilevel",
    ];

    public static $rules = [
        "bo_m_no" => "max:10",
        "description" => "max:32",
    ];
}
