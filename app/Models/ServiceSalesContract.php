<?php

namespace App\Models;

class ServiceSalesContract extends Model
{
    protected $table = "service_sales_contract";

    public $timestamps = false;

    protected $fillable = [
        "attached_file",
        "buyer_name",
        "description",
        "due_date",
        "reff_number",
        "sc_date",
        "sc_number",
        "scope_of_work",
        "total_value",
    ];

    public static $rules = [
        "attached_file" => "max:30",
        "buyer_name" => "max:16",
        "description" => "max:32",
        "reff_number" => "max:12",
        "sc_number" => "max:12",
        "total_value" => "numeric",
    ];
}
