<?php

namespace App\Models;

class PasswordResets extends Model
{
    protected $table = "password_resets";

    public $timestamps = false;

    protected $fillable = [
        "created_at",
        "email",
        "token",
    ];

    public static $rules = [
        "created_at" => "required",
        "email" => "required|max:255",
        "token" => "required|max:255",
    ];
}
