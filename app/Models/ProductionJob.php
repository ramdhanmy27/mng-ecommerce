<?php

namespace App\Models;

class ProductionJob extends Model
{
    protected $table = "production_job";

    public $timestamps = false;

    protected $fillable = [
        "duration",
        "finish_date",
        "start_date",
    ];

    public static $rules = [
        "duration" => "max:255",
    ];
}
