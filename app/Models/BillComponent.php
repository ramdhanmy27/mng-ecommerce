<?php

namespace App\Models;

class BillComponent extends Model
{
    protected $table = "bill_component";

    public $timestamps = false;

    protected $fillable = [];

    public static $rules = [
        "description" => "max:32",
        "reff_no" => "max:10",
        "trans_value" => "numeric",
    ];
}
