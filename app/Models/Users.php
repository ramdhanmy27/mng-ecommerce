<?php

namespace App\Models;

class Users extends Model
{
    protected $table = "users";

    public $timestamps = false;

    protected $fillable = [
        "created_at",
        "email",
        "kode",
        "name",
        "password",
        "remember_token",
        "updated_at",
    ];

    public static $rules = [
        "email" => "required|max:255",
        "kode" => "max:20",
        "name" => "required|max:255",
        "password" => "required|max:255",
        "remember_token" => "max:100",
    ];
}
