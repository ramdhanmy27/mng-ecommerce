<?php

namespace App\Models;

class LoanFacility extends Model
{
    protected $table = "loan_facility";

    public $timestamps = false;

    protected $fillable = [
        "admin_fee",
        "agreement_date",
        "agreement_no",
        "attached_file",
        "currency",
        "description",
        "interest_rate",
        "plafond_value",
        "provision_fee",
        "valid_from",
        "valid_until",
    ];

    public static $rules = [
        "admin_fee" => "numeric",
        "agreement_no" => "max:12",
        "attached_file" => "max:30",
        "description" => "max:32",
        "interest_rate" => "numeric",
        "plafond_value" => "numeric",
        "provision_fee" => "numeric",
    ];
}
