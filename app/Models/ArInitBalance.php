<?php

namespace App\Models;

class ArInitBalance extends Model
{
    protected $table = "ar_init_balance";

    public $timestamps = false;

    protected $fillable = [
        "init_value",
    ];

    public static $rules = [
        "init_value" => "numeric",
    ];
}
