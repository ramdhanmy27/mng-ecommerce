<?php

namespace App\Models;

class Supplier extends Model
{
    protected $table = "supplier";

    public $timestamps = false;

    protected $fillable = [
        "address",
        "contact_name",
        "description",
        "fax",
        "mail",
        "npwp",
        "phone",
        "post_code",
        "supp_name",
    ];

    public static $rules = [
        "address" => "max:48",
        "contact_name" => "max:16",
        "description" => "max:32",
        "fax" => "max:13",
        "mail" => "max:24",
        "npwp" => "max:20",
        "phone" => "max:13",
        "supp_name" => "required|max:24",
    ];

    public function attributeLabels()
    {
        return [
            "supp_name" => "Supplier Name",
            "mail" => "Email",
            "contact_name" => "Kontak Name",
            "phone" => "Phone",
            "fax" => "Fax",
            "address" => "Alamat",
            "description" => "Deskrisi",
            "npwp" => "NPWP",
            "post_code" => "Kode Pos",
        ];
    }
}
