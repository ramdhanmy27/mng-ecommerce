<?php

namespace App\Models;

class JobOrder extends Model
{
    protected $table = "job_order";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "job_date",
        "job_no",
        "qty",
    ];

    public static $rules = [
        "description" => "max:255",
        "job_no" => "max:255",
    ];
}
