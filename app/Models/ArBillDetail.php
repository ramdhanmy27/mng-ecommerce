<?php

namespace App\Models;

class ArBillDetail extends Model
{
    protected $table = "ar_bill_detail";

    public $timestamps = false;

    protected $fillable = [
        "bill_value",
        "description",
    ];

    public static $rules = [
        "bill_value" => "numeric",
        "description" => "max:36",
    ];
}
