<?php

namespace App\Models;

class RecurrentBill extends Model
{
    protected $table = "recurrent_bill";

    public $timestamps = false;

    protected $fillable = [];

    public static $rules = [
        "description" => "max:36",
        "rcv_number" => "max:10",
    ];
}
