<?php

namespace App\Models;

class SizeSet extends Model
{
    protected $table = "size_set";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "size_list",
    ];

    public static $rules = [
        "description" => "max:32",
        "size_list" => "max:48",
    ];
}
