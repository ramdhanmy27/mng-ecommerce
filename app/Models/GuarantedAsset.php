<?php

namespace App\Models;

class GuarantedAsset extends Model
{
    protected $table = "guaranted_asset";

    public $timestamps = false;

    protected $fillable = [
        "asset_name",
        "asset_no",
        "asset_value",
        "avalist_name",
    ];

    public static $rules = [
        "asset_name" => "max:24",
        "asset_no" => "max:12",
        "asset_value" => "numeric",
        "avalist_name" => "max:24",
    ];
}
