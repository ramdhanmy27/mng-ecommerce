<?php

namespace App\Models;

class WhRack extends Model
{
    protected $table = "wh_rack";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "height",
        "length",
        "rack_code",
        "shelf_count",
        "width",
    ];

    public static $rules = [
        "description" => "max:16",
    ];
}
