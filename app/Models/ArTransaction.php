<?php

namespace App\Models;

class ArTransaction extends Model
{
    protected $table = "ar_transaction";

    protected $primaryKey = "trans_no";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "is_journalized",
        "trans_date",
        "trans_type",
        "trans_value",
    ];

    public static $rules = [
        "description" => "max:32",
        "trans_value" => "numeric",
    ];
}
