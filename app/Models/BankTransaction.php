<?php

namespace App\Models;

class BankTransaction extends Model
{
    protected $table = "bank_transaction";

    protected $primaryKey = "trans_no";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "is_reconciled",
        "reff_no",
        "trans_date",
        "trans_type",
        "trans_value",
    ];

    public static $rules = [
        "description" => "max:32",
        "reff_no" => "max:10",
        "trans_value" => "numeric",
    ];
}
