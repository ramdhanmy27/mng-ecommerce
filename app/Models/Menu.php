<?php

namespace App\Models;

class Menu extends Model
{
    protected $table = "menu";

    public $timestamps = false;

    protected $fillable = [
        "created_at",
        "enable",
        "order",
        "param",
        "parent",
        "title",
        "updated_at",
        "url",
    ];

    public static $rules = [
        "enable" => "required",
        "order" => "required",
        "title" => "required|max:100",
    ];
}
