<?php

namespace App\Models;

class GlAccountGroup extends Model
{
    protected $table = "gl_account_group";

    protected $primaryKey = "group_code";

    public $timestamps = false;

    protected $fillable = [
        "group_name",
        "is_balance_sheet",
        "is_debetpositive",
    ];

    public static $rules = [
        "group_name" => "max:16",
    ];
}
