<?php

namespace App\Models;

class PackingList extends Model
{
    protected $table = "packing_list";

    public $timestamps = false;

    protected $fillable = [
        "delivery_date",
        "description",
        "total_qty",
    ];

    public static $rules = [
        "description" => "max:32",
    ];
}
