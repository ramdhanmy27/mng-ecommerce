<?php

namespace App\Models;

class ReceiveItem extends Model
{
    protected $table = "receive_item";

    public $timestamps = false;

    protected $fillable = [
        "rcv_number",
        "qty",
        "unit_price",
        "unit_code",
        "material_id",
    ];

    public static $rules = [
        "qty" => "integer",
        "unit_price" => "numeric",
    ];

    protected $attributes = [
        "qty" => 0,
        "unit_price" => 0,
    ];

    public function attributeLabels()
    {
        return [
            "qty" => "Qty",
            "unit_price" => "Price",
            "unit_code" => "Unit",
            "material_id" => "Material",

            "unit_name" => "Unit",
            "mat_name" => "Nama",
            "mat_code" => "Kode",
        ];
    }

    public function scopeDetails($query, $rcv_number)
    {
        return $query->where("receive_item.rcv_number", $rcv_number)
            ->join("material as m", "m.id", "=", "receive_item.material_id")
                ->addSelect("m.mat_code", "m.mat_name", "receive_item.*")
            ->join("units as u", "u.unit_code", "=", "receive_item.unit_code")
                ->addSelect("u.unit_name");
    }
}
