<?php

namespace App\Models;

class Units extends Model
{
    protected $table = "units";

    public $timestamps = false;

    protected $fillable = [
        "base_conversion",
        "dimension_type",
        "unit_code",
        "unit_name",
    ];

    public static $rules = [
        "base_conversion" => "numeric",
        "unit_name" => "max:16",
    ];

    public function attributeLabels()
    {
        return [
            "unit_code" => "Kode",
            "unit_name" => "Name",
            "dimension_type" => "Dimension Type",
            "base_conversion" => "Base Conversion",
        ];
    }

    public function config($index)
    {
        return array_get([
            "type" => [
                1 => "Panjang",
                2 => "Lebar",
                3 => "Massa",
                4 => "Diskrit",
                5 => "Volume",
            ],
        ], $index);
    }
}
