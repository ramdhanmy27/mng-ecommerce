<?php

namespace App\Models;

class MatTransItem extends Model
{
    protected $table = "mat_trans_item";

    public $timestamps = false;

    protected $fillable = [
        "external_qty",
        "pack_qty",
        "qty",
    ];

    public static $rules = [
        "external_qty" => "numeric",
        "qty" => "numeric",
    ];
}
