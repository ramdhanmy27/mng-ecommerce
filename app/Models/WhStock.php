<?php

namespace App\Models;

class WhStock extends Model
{
    protected $table = "wh_stock";

    public $timestamps = false;

    protected $fillable = [
        "pack_qty",
        "qty",
    ];

    public static $rules = [
        "qty" => "numeric",
    ];
}
