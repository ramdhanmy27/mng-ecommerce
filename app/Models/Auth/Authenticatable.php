<?php 

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Authenticatable extends Model
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;
}