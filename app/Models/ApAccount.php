<?php

namespace App\Models;

class ApAccount extends Model
{
    protected $table = "ap_account";

    protected $primaryKey = "ap_code";

    public $timestamps = false;

    protected $fillable = [
        "ap_name",
        "balance",
        "curr",
    ];

    public static $rules = [
        "ap_name" => "max:16",
        "balance" => "numeric",
    ];
}
