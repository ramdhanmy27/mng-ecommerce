<?php

namespace App\Http\Helpers;

class Date 
{
    public static function parse($time, $format = null)
    {
        return date(config("app.date.display", $format), strtotime($time));
    }
}