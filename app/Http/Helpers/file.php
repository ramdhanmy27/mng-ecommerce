<?php

function rglob($pattern, $flags = 0, $callback = null)
{
    $files = glob($pattern, $flags);

    if (is_callable($callback)) {
        $callback(dirname($pattern), $files);
    }

    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }

    return $files;
}

function make_dir($path, $split = "/") 
{
    $tmp = "";

    foreach (filter_empty(explode($split, $path)) as $dir) {
        $tmp .= $dir.'/';

        if (!file_exists($tmp) && is_writable(dirname($tmp))) {
            mkdir($tmp);
        }
    }

    return $tmp;
}