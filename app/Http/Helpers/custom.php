<?php

/**
 * convert postgre array to php array
 * @param  string $str
 * @return array
 */
function pg2array($str)
{
    preg_match_all("/\"(.*)\"|[\w\s]+/", $str, $matches);

    return $matches[0];

    // harus di cek lebih lanjut data yang bisa masuk apa aja
    $data = [];

    foreach ($matches[0] as $i => $value) {
        $data[] = $value{0}=='"' ? $matches[1][$i] : $value;
    }

    return $data;
}