<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use Illuminate\Http\Request;
use Datatables;

class RolesController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Roles::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Roles::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Roles::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Roles::createOrFail($req->all());

        return redirect("roles/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Roles::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Roles::findOrFail($id)->updateOrFail($req->all());

        return redirect("roles/edit/$id");
    }

    public function getView($id)
    {
        $model = Roles::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Roles::findOrFail($id)->delete();

        return redirect("roles");
    }
}
