<?php

namespace App\Http\Controllers;

use App\Models\ProductionJob;
use Illuminate\Http\Request;
use Datatables;

class ProductionJobController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ProductionJob::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ProductionJob::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ProductionJob::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ProductionJob::createOrFail($req->all());

        return redirect("production-job/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ProductionJob::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ProductionJob::findOrFail($id)->updateOrFail($req->all());

        return redirect("production-job/edit/$id");
    }

    public function getView($id)
    {
        $model = ProductionJob::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ProductionJob::findOrFail($id)->delete();

        return redirect("production-job");
    }
}
