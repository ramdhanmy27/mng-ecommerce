<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Datatables;

class BrandController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Brand::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Brand::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Brand::createOrFail($req->all());

        return redirect("brand/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Brand::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Brand::findOrFail($id)->updateOrFail($req->all());

        return redirect("brand/edit/$id");
    }

    public function getView($id)
    {
        $model = Brand::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Brand::findOrFail($id)->delete();

        return redirect("brand");
    }
}
