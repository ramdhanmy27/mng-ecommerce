<?php

namespace App\Http\Controllers;

use App\Models\ProductSalesContract;
use Illuminate\Http\Request;
use Datatables;

class ProductSalesContractController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ProductSalesContract::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ProductSalesContract::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ProductSalesContract::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ProductSalesContract::createOrFail($req->all());

        return redirect("product-sales-contract/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ProductSalesContract::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ProductSalesContract::findOrFail($id)->updateOrFail($req->all());

        return redirect("product-sales-contract/edit/$id");
    }

    public function getView($id)
    {
        $model = ProductSalesContract::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ProductSalesContract::findOrFail($id)->delete();

        return redirect("product-sales-contract");
    }
}
