<?php

namespace App\Http\Controllers;

use App\Models\InitialBalance;
use Illuminate\Http\Request;
use Datatables;

class InitialBalanceController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => InitialBalance::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(InitialBalance::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => InitialBalance::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = InitialBalance::createOrFail($req->all());

        return redirect("initial-balance/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = InitialBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        InitialBalance::findOrFail($id)->updateOrFail($req->all());

        return redirect("initial-balance/edit/$id");
    }

    public function getView($id)
    {
        $model = InitialBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        InitialBalance::findOrFail($id)->delete();

        return redirect("initial-balance");
    }
}
