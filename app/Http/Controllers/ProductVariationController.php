<?php

namespace App\Http\Controllers;

use App\Models\ProductVariation;
use Illuminate\Http\Request;
use Datatables;

class ProductVariationController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ProductVariation::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ProductVariation::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ProductVariation::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ProductVariation::createOrFail($req->all());

        return redirect("product-variation/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ProductVariation::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ProductVariation::findOrFail($id)->updateOrFail($req->all());

        return redirect("product-variation/edit/$id");
    }

    public function getView($id)
    {
        $model = ProductVariation::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ProductVariation::findOrFail($id)->delete();

        return redirect("product-variation");
    }
}
