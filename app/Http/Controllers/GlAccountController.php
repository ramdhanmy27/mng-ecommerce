<?php

namespace App\Http\Controllers;

use App\Models\GlAccount;
use Illuminate\Http\Request;
use Datatables;

class GlAccountController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => GlAccount::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(GlAccount::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => GlAccount::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = GlAccount::createOrFail($req->all());

        return redirect("gl-account/edit/$model->account_code");
    }

    public function getEdit($id)
    {
        $model = GlAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        GlAccount::findOrFail($id)->updateOrFail($req->all());

        return redirect("gl-account/edit/$id");
    }

    public function getView($id)
    {
        $model = GlAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        GlAccount::findOrFail($id)->delete();

        return redirect("gl-account");
    }
}
