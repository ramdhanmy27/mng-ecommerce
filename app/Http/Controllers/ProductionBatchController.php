<?php

namespace App\Http\Controllers;

use App\Models\ProductionBatch;
use Illuminate\Http\Request;
use Datatables;

class ProductionBatchController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ProductionBatch::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ProductionBatch::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ProductionBatch::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ProductionBatch::createOrFail($req->all());

        return redirect("production-batch/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ProductionBatch::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ProductionBatch::findOrFail($id)->updateOrFail($req->all());

        return redirect("production-batch/edit/$id");
    }

    public function getView($id)
    {
        $model = ProductionBatch::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ProductionBatch::findOrFail($id)->delete();

        return redirect("production-batch");
    }
}
