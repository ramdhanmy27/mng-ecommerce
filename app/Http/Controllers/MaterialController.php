<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;
use Datatables;

class MaterialController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Material::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => new Material,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Material::createOrFail($req->all());

        return redirect("material/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Material::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Material::findOrFail($id)->updateOrFail($req->all());

        return redirect("material/edit/$id");
    }

    public function getView($id)
    {
        $model = Material::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Material::findOrFail($id)->delete();

        return redirect("material");
    }
}
