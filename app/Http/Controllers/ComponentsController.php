<?php

namespace App\Http\Controllers;

use App\Models\Components;
use Illuminate\Http\Request;
use Datatables;

class ComponentsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Components::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Components::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Components::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Components::createOrFail($req->all());

        return redirect("components/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Components::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Components::findOrFail($id)->updateOrFail($req->all());

        return redirect("components/edit/$id");
    }

    public function getView($id)
    {
        $model = Components::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Components::findOrFail($id)->delete();

        return redirect("components");
    }
}
