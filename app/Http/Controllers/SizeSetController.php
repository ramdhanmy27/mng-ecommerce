<?php

namespace App\Http\Controllers;

use App\Models\SizeSet;
use Illuminate\Http\Request;
use Datatables;

class SizeSetController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => SizeSet::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(SizeSet::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => SizeSet::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = SizeSet::createOrFail($req->all());

        return redirect("size-set/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = SizeSet::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        SizeSet::findOrFail($id)->updateOrFail($req->all());

        return redirect("size-set/edit/$id");
    }

    public function getView($id)
    {
        $model = SizeSet::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        SizeSet::findOrFail($id)->delete();

        return redirect("size-set");
    }
}
