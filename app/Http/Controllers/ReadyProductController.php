<?php

namespace App\Http\Controllers;

use App\Models\ReadyProduct;
use Illuminate\Http\Request;
use Datatables;

class ReadyProductController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ReadyProduct::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ReadyProduct::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ReadyProduct::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ReadyProduct::createOrFail($req->all());

        return redirect("ready-product/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ReadyProduct::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ReadyProduct::findOrFail($id)->updateOrFail($req->all());

        return redirect("ready-product/edit/$id");
    }

    public function getView($id)
    {
        $model = ReadyProduct::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ReadyProduct::findOrFail($id)->delete();

        return redirect("ready-product");
    }
}
