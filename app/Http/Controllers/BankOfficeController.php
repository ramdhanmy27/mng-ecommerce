<?php

namespace App\Http\Controllers;

use App\Models\BankOffice;
use Illuminate\Http\Request;
use Datatables;

class BankOfficeController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BankOffice::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BankOffice::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BankOffice::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BankOffice::createOrFail($req->all());

        return redirect("bank-office/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = BankOffice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BankOffice::findOrFail($id)->updateOrFail($req->all());

        return redirect("bank-office/edit/$id");
    }

    public function getView($id)
    {
        $model = BankOffice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BankOffice::findOrFail($id)->delete();

        return redirect("bank-office");
    }
}
