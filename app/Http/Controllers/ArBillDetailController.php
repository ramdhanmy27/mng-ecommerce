<?php

namespace App\Http\Controllers;

use App\Models\ArBillDetail;
use Illuminate\Http\Request;
use Datatables;

class ArBillDetailController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ArBillDetail::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ArBillDetail::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ArBillDetail::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ArBillDetail::createOrFail($req->all());

        return redirect("ar-bill-detail/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ArBillDetail::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ArBillDetail::findOrFail($id)->updateOrFail($req->all());

        return redirect("ar-bill-detail/edit/$id");
    }

    public function getView($id)
    {
        $model = ArBillDetail::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ArBillDetail::findOrFail($id)->delete();

        return redirect("ar-bill-detail");
    }
}
