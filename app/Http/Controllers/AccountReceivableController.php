<?php

namespace App\Http\Controllers;

use App\Models\AccountReceivable;
use Illuminate\Http\Request;
use Datatables;

class AccountReceivableController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => AccountReceivable::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(AccountReceivable::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => AccountReceivable::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = AccountReceivable::createOrFail($req->all());

        return redirect("account-receivable/edit/$model->ar_code");
    }

    public function getEdit($id)
    {
        $model = AccountReceivable::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        AccountReceivable::findOrFail($id)->updateOrFail($req->all());

        return redirect("account-receivable/edit/$id");
    }

    public function getView($id)
    {
        $model = AccountReceivable::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        AccountReceivable::findOrFail($id)->delete();

        return redirect("account-receivable");
    }
}
