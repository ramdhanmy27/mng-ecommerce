<?php

namespace App\Http\Controllers;

use App\Models\CashTransaction;
use Illuminate\Http\Request;
use Datatables;

class CashTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => CashTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(CashTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => CashTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = CashTransaction::createOrFail($req->all());

        return redirect("cash-transaction/edit/$model->trans_no");
    }

    public function getEdit($id)
    {
        $model = CashTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        CashTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("cash-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = CashTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        CashTransaction::findOrFail($id)->delete();

        return redirect("cash-transaction");
    }
}
