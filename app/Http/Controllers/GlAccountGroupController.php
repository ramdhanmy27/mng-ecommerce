<?php

namespace App\Http\Controllers;

use App\Models\GlAccountGroup;
use Illuminate\Http\Request;
use Datatables;

class GlAccountGroupController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => GlAccountGroup::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(GlAccountGroup::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => GlAccountGroup::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = GlAccountGroup::createOrFail($req->all());

        return redirect("gl-account-group/edit/$model->group_code");
    }

    public function getEdit($id)
    {
        $model = GlAccountGroup::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        GlAccountGroup::findOrFail($id)->updateOrFail($req->all());

        return redirect("gl-account-group/edit/$id");
    }

    public function getView($id)
    {
        $model = GlAccountGroup::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        GlAccountGroup::findOrFail($id)->delete();

        return redirect("gl-account-group");
    }
}
