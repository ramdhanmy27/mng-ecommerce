<?php

namespace App\Http\Controllers;

use App\Models\BranchOffice;
use Illuminate\Http\Request;
use Datatables;

class BranchOfficeController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BranchOffice::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BranchOffice::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BranchOffice::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BranchOffice::createOrFail($req->all());

        return redirect("branch-office/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = BranchOffice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BranchOffice::findOrFail($id)->updateOrFail($req->all());

        return redirect("branch-office/edit/$id");
    }

    public function getView($id)
    {
        $model = BranchOffice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BranchOffice::findOrFail($id)->delete();

        return redirect("branch-office");
    }
}
