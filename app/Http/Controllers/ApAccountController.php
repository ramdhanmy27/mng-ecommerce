<?php

namespace App\Http\Controllers;

use App\Models\ApAccount;
use Illuminate\Http\Request;
use Datatables;

class ApAccountController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ApAccount::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ApAccount::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ApAccount::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ApAccount::createOrFail($req->all());

        return redirect("ap-account/edit/$model->ap_code");
    }

    public function getEdit($id)
    {
        $model = ApAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ApAccount::findOrFail($id)->updateOrFail($req->all());

        return redirect("ap-account/edit/$id");
    }

    public function getView($id)
    {
        $model = ApAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ApAccount::findOrFail($id)->delete();

        return redirect("ap-account");
    }
}
