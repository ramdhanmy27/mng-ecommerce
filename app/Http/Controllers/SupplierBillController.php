<?php

namespace App\Http\Controllers;

use App\Models\SupplierBill;
use Illuminate\Http\Request;
use Datatables;

class SupplierBillController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => SupplierBill::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(SupplierBill::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => SupplierBill::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = SupplierBill::createOrFail($req->all());

        return redirect("supplier-bill/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = SupplierBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        SupplierBill::findOrFail($id)->updateOrFail($req->all());

        return redirect("supplier-bill/edit/$id");
    }

    public function getView($id)
    {
        $model = SupplierBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        SupplierBill::findOrFail($id)->delete();

        return redirect("supplier-bill");
    }
}
