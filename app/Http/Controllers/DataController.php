<?php 

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\MaterialGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller 
{
    public function postMaterial(Request $req)
    {
        return Material::findOrFail($req->id);
    }

    public function postMaterialGroup(Request $req)
    {
        $query = MaterialGroup::where("material_group.grp_code", $req->id);

        if ($req->has("material_id"))
            $query->join("material as m", "m.grp_code", "=", "material_group.grp_code")
                ->where("m.id", $req->material_id)
                ->select("m.*", "material_group.*");

        return $query->first();
    }
}