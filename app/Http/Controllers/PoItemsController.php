<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\PoItems;
use App\Models\PurchaseOrder;
use Datatables;
use Illuminate\Http\Request;

class PoItemsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex($po_id)
    {
        return view([
            "po_id" => $po_id,
        ]);
    }

    public function anyData($po_id)
    {
        return Datatables::of(
            PoItems::select("po_items.*")
                ->join("purchase_order as po", "po.po_number", "=", "po_items.po_number")
                ->where("po.id", $po_id)
        )->make(true);
    }

    public function getAdd($po_id)
    {
        $model = PurchaseOrder::findOrFail($po_id);

        return view([
            "po_number" => $model->po_number,
            "model" => PoItems::class,
        ]);
    }

    public function postAdd(Request $req, $po_id)
    {
        $data = $req->all();
        $data["material_id"] = $this->createMaterial($req);
        $model = PoItems::createOrFail($data);

        return redirect("purchase-order/edit/$po_id");
    }

    public function getEdit($po_id, $id)
    {
        $model = PoItems::findOrFail($id);

        return view([
            "po_number" => $model->po_number,
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $po_id, $id)
    {
        $data = $req->all();
        $data["material_id"] = $this->createMaterial($req);
        PoItems::findOrFail($id)->updateOrFail($data);

        return redirect("purchase-order/edit/$po_id");
    }

    public function getView($po_id, $id)
    {
        $model = PoItems::findOrFail($id);

        return view([
            "po_id" => $po_id,
            "model" => $model,
        ]);
    }

    public function getDelete($po_id, $id)
    {
        PoItems::findOrFail($id)->delete();

        return redirect("purchase-order/edit/$po_id");
    }

    private function createMaterial(Request $req)
    {
        if (!$req->has("material_id")) {
            if (!($material = Material::where("mat_name", $req->item_name)->first())) {
                $material = Material::create([
                    "mat_name" => $req->item_name,
                    "unit_code" => $req->unit_code,
                ]);
            }

            return $material->id;
        }

        return $req->material_id;
    }
}
