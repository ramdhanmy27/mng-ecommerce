<?php

namespace App\Http\Controllers;

use App\Models\Cheque;
use Illuminate\Http\Request;
use Datatables;

class ChequeController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Cheque::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Cheque::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Cheque::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Cheque::createOrFail($req->all());

        return redirect("cheque/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Cheque::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Cheque::findOrFail($id)->updateOrFail($req->all());

        return redirect("cheque/edit/$id");
    }

    public function getView($id)
    {
        $model = Cheque::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Cheque::findOrFail($id)->delete();

        return redirect("cheque");
    }
}
