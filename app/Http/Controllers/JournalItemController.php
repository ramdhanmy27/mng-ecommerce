<?php

namespace App\Http\Controllers;

use App\Models\JournalItem;
use Illuminate\Http\Request;
use Datatables;

class JournalItemController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => JournalItem::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(JournalItem::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => JournalItem::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = JournalItem::createOrFail($req->all());

        return redirect("journal-item/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = JournalItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        JournalItem::findOrFail($id)->updateOrFail($req->all());

        return redirect("journal-item/edit/$id");
    }

    public function getView($id)
    {
        $model = JournalItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        JournalItem::findOrFail($id)->delete();

        return redirect("journal-item");
    }
}
