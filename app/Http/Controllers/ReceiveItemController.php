<?php

namespace App\Http\Controllers;

use App\Models\ReceiveItem;
use App\Models\Receiving;
use Datatables;
use Illuminate\Http\Request;

class ReceiveItemController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex($receive_id)
    {
        return view([
            "receive_id" => $receive_id,
        ]);
    }

    public function anyData($receive_id)
    {
        return Datatables::of(
            ReceiveItem::select("receive_item.*")
                ->join("material as m", "receive_item.material_id", "=", "m.id")
                ->addSelect("m.mat_name")
        )->make(true);
    }

    public function getAdd($receive_id)
    {
        $model = Receiving::findOrFail($receive_id);

        return view([
            "rcv_number" => $model->rcv_number,
            "model" => ReceiveItem::class,
        ]);
    }

    public function postAdd($receive_id, Request $req)
    {
        $model = ReceiveItem::createOrFail($req->all());

        return redirect("receiving/edit/$receive_id");
    }

    public function getEdit($receive_id, $id)
    {
        $model = Receiving::findOrFail($receive_id);
        $model = ReceiveItem::findOrFail($id);

        return view([
            "rcv_number" => $model->rcv_number,
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $receive_id, $id)
    {
        ReceiveItem::findOrFail($id)->updateOrFail($req->all());

        return redirect("receiving/edit/$receive_id");
    }

    public function getView($receive_id, $id)
    {
        $model = ReceiveItem::findOrFail($id);

        return view([
            "receive_id" => $receive_id,
            "model" => $model,
        ]);
    }

    public function getDelete($receive_id, $id)
    {
        ReceiveItem::findOrFail($id)->delete();

        return redirect("receiving/edit/$receive_id");
    }
}
