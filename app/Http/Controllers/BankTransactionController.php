<?php

namespace App\Http\Controllers;

use App\Models\BankTransaction;
use Illuminate\Http\Request;
use Datatables;

class BankTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BankTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BankTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BankTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BankTransaction::createOrFail($req->all());

        return redirect("bank-transaction/edit/$model->trans_no");
    }

    public function getEdit($id)
    {
        $model = BankTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BankTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("bank-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = BankTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BankTransaction::findOrFail($id)->delete();

        return redirect("bank-transaction");
    }
}
