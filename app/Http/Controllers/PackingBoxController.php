<?php

namespace App\Http\Controllers;

use App\Models\PackingBox;
use Illuminate\Http\Request;
use Datatables;

class PackingBoxController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => PackingBox::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(PackingBox::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => PackingBox::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = PackingBox::createOrFail($req->all());

        return redirect("packing-box/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = PackingBox::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        PackingBox::findOrFail($id)->updateOrFail($req->all());

        return redirect("packing-box/edit/$id");
    }

    public function getView($id)
    {
        $model = PackingBox::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        PackingBox::findOrFail($id)->delete();

        return redirect("packing-box");
    }
}
