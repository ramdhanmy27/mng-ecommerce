<?php

namespace App\Http\Controllers;

use App\Models\UnstoredStock;
use Illuminate\Http\Request;
use Datatables;

class UnstoredStockController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => UnstoredStock::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(UnstoredStock::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => UnstoredStock::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = UnstoredStock::createOrFail($req->all());

        return redirect("unstored-stock/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = UnstoredStock::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        UnstoredStock::findOrFail($id)->updateOrFail($req->all());

        return redirect("unstored-stock/edit/$id");
    }

    public function getView($id)
    {
        $model = UnstoredStock::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        UnstoredStock::findOrFail($id)->delete();

        return redirect("unstored-stock");
    }
}
