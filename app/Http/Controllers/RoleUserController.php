<?php

namespace App\Http\Controllers;

use App\Models\RoleUser;
use Illuminate\Http\Request;
use Datatables;

class RoleUserController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => RoleUser::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(RoleUser::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => RoleUser::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = RoleUser::createOrFail($req->all());

        return redirect("role-user/edit/$model->user_id");
    }

    public function getEdit($id)
    {
        $model = RoleUser::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        RoleUser::findOrFail($id)->updateOrFail($req->all());

        return redirect("role-user/edit/$id");
    }

    public function getView($id)
    {
        $model = RoleUser::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        RoleUser::findOrFail($id)->delete();

        return redirect("role-user");
    }
}
