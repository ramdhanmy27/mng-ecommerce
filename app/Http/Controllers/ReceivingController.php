<?php

namespace App\Http\Controllers;

use App\Models\ReceiveItem;
use App\Models\Receiving;
use Datatables;
use Illuminate\Http\Request;

class ReceivingController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Receiving::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => new Receiving([
                "rcv_date" => date("Y/m/d"),
            ]),
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Receiving::createOrFail($req->all());

        return redirect("receiving/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Receiving::findOrFail($id);

        return view([
            "id" => $id,
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Receiving::findOrFail($id)->updateOrFail($req->all());

        return redirect("receiving/edit/$id");
    }

    public function getView($id)
    {
        $model = Receiving::findOrFail($id);

        return view([
            "model" => $model,
            "items" => ReceiveItem::details($model->rcv_number)->get(),
        ]);
    }

    public function getDelete($id)
    {
        Receiving::findOrFail($id)->delete();

        return redirect("receiving");
    }
}
