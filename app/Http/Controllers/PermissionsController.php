<?php

namespace App\Http\Controllers;

use App\Models\Permissions;
use Illuminate\Http\Request;
use Datatables;

class PermissionsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Permissions::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Permissions::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Permissions::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Permissions::createOrFail($req->all());

        return redirect("permissions/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Permissions::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Permissions::findOrFail($id)->updateOrFail($req->all());

        return redirect("permissions/edit/$id");
    }

    public function getView($id)
    {
        $model = Permissions::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Permissions::findOrFail($id)->delete();

        return redirect("permissions");
    }
}
