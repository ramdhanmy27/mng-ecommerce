<?php

namespace App\Http\Controllers;

use App\Models\Journal;
use Illuminate\Http\Request;
use Datatables;

class JournalController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Journal::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Journal::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Journal::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Journal::createOrFail($req->all());

        return redirect("journal/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Journal::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Journal::findOrFail($id)->updateOrFail($req->all());

        return redirect("journal/edit/$id");
    }

    public function getView($id)
    {
        $model = Journal::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Journal::findOrFail($id)->delete();

        return redirect("journal");
    }
}
