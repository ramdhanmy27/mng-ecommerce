<?php

namespace App\Http\Controllers;

use App\Models\ArBill;
use Illuminate\Http\Request;
use Datatables;

class ArBillController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ArBill::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ArBill::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ArBill::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ArBill::createOrFail($req->all());

        return redirect("ar-bill/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ArBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ArBill::findOrFail($id)->updateOrFail($req->all());

        return redirect("ar-bill/edit/$id");
    }

    public function getView($id)
    {
        $model = ArBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ArBill::findOrFail($id)->delete();

        return redirect("ar-bill");
    }
}
