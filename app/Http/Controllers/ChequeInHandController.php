<?php

namespace App\Http\Controllers;

use App\Models\ChequeInHand;
use Illuminate\Http\Request;
use Datatables;

class ChequeInHandController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ChequeInHand::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ChequeInHand::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ChequeInHand::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ChequeInHand::createOrFail($req->all());

        return redirect("cheque-in-hand/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ChequeInHand::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ChequeInHand::findOrFail($id)->updateOrFail($req->all());

        return redirect("cheque-in-hand/edit/$id");
    }

    public function getView($id)
    {
        $model = ChequeInHand::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ChequeInHand::findOrFail($id)->delete();

        return redirect("cheque-in-hand");
    }
}
