<?php

namespace App\Http\Controllers;

use App\Models\LoanTransaction;
use Illuminate\Http\Request;
use Datatables;

class LoanTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => LoanTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(LoanTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => LoanTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = LoanTransaction::createOrFail($req->all());

        return redirect("loan-transaction/edit/$model->trans_no");
    }

    public function getEdit($id)
    {
        $model = LoanTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        LoanTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("loan-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = LoanTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        LoanTransaction::findOrFail($id)->delete();

        return redirect("loan-transaction");
    }
}
