<?php

namespace App\Http\Controllers;

use App\Models\Storing;
use Illuminate\Http\Request;
use Datatables;

class StoringController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "model" => new Storing(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Storing::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Storing::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Storing::createOrFail($req->all());

        return redirect("storing/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Storing::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Storing::findOrFail($id)->updateOrFail($req->all());

        return redirect("storing/edit/$id");
    }

    public function getView($id)
    {
        $model = Storing::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Storing::findOrFail($id)->delete();

        return redirect("storing");
    }
}
