<?php

namespace App\Http\Controllers;

use App\Models\PermissionRole;
use Illuminate\Http\Request;
use Datatables;

class PermissionRoleController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => PermissionRole::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(PermissionRole::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => PermissionRole::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = PermissionRole::createOrFail($req->all());

        return redirect("permission-role/edit/$model->role_id");
    }

    public function getEdit($id)
    {
        $model = PermissionRole::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        PermissionRole::findOrFail($id)->updateOrFail($req->all());

        return redirect("permission-role/edit/$id");
    }

    public function getView($id)
    {
        $model = PermissionRole::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        PermissionRole::findOrFail($id)->delete();

        return redirect("permission-role");
    }
}
