<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use Datatables;

class InvoiceController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Invoice::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Invoice::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Invoice::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Invoice::createOrFail($req->all());

        return redirect("invoice/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Invoice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Invoice::findOrFail($id)->updateOrFail($req->all());

        return redirect("invoice/edit/$id");
    }

    public function getView($id)
    {
        $model = Invoice::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Invoice::findOrFail($id)->delete();

        return redirect("invoice");
    }
}
