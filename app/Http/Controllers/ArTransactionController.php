<?php

namespace App\Http\Controllers;

use App\Models\ArTransaction;
use Illuminate\Http\Request;
use Datatables;

class ArTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ArTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ArTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ArTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ArTransaction::createOrFail($req->all());

        return redirect("ar-transaction/edit/$model->trans_no");
    }

    public function getEdit($id)
    {
        $model = ArTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ArTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("ar-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = ArTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ArTransaction::findOrFail($id)->delete();

        return redirect("ar-transaction");
    }
}
