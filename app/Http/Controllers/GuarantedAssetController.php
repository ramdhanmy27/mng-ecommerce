<?php

namespace App\Http\Controllers;

use App\Models\GuarantedAsset;
use Illuminate\Http\Request;
use Datatables;

class GuarantedAssetController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => GuarantedAsset::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(GuarantedAsset::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => GuarantedAsset::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = GuarantedAsset::createOrFail($req->all());

        return redirect("guaranted-asset/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = GuarantedAsset::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        GuarantedAsset::findOrFail($id)->updateOrFail($req->all());

        return redirect("guaranted-asset/edit/$id");
    }

    public function getView($id)
    {
        $model = GuarantedAsset::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        GuarantedAsset::findOrFail($id)->delete();

        return redirect("guaranted-asset");
    }
}
