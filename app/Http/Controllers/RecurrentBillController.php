<?php

namespace App\Http\Controllers;

use App\Models\RecurrentBill;
use Illuminate\Http\Request;
use Datatables;

class RecurrentBillController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => RecurrentBill::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(RecurrentBill::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => RecurrentBill::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = RecurrentBill::createOrFail($req->all());

        return redirect("recurrent-bill/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = RecurrentBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        RecurrentBill::findOrFail($id)->updateOrFail($req->all());

        return redirect("recurrent-bill/edit/$id");
    }

    public function getView($id)
    {
        $model = RecurrentBill::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        RecurrentBill::findOrFail($id)->delete();

        return redirect("recurrent-bill");
    }
}
