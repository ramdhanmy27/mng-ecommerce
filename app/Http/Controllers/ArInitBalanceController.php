<?php

namespace App\Http\Controllers;

use App\Models\ArInitBalance;
use Illuminate\Http\Request;
use Datatables;

class ArInitBalanceController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ArInitBalance::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ArInitBalance::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ArInitBalance::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ArInitBalance::createOrFail($req->all());

        return redirect("ar-init-balance/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ArInitBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ArInitBalance::findOrFail($id)->updateOrFail($req->all());

        return redirect("ar-init-balance/edit/$id");
    }

    public function getView($id)
    {
        $model = ArInitBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ArInitBalance::findOrFail($id)->delete();

        return redirect("ar-init-balance");
    }
}
