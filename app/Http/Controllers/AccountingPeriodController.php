<?php

namespace App\Http\Controllers;

use App\Models\AccountingPeriod;
use Illuminate\Http\Request;
use Datatables;

class AccountingPeriodController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => AccountingPeriod::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(AccountingPeriod::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => AccountingPeriod::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = AccountingPeriod::createOrFail($req->all());

        return redirect("accounting-period/edit/$model->year");
    }

    public function getEdit($id)
    {
        $model = AccountingPeriod::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        AccountingPeriod::findOrFail($id)->updateOrFail($req->all());

        return redirect("accounting-period/edit/$id");
    }

    public function getView($id)
    {
        $model = AccountingPeriod::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        AccountingPeriod::findOrFail($id)->delete();

        return redirect("accounting-period");
    }
}
