<?php

namespace App\Http\Controllers;

use App\Models\BillOfMaterial;
use Illuminate\Http\Request;
use Datatables;

class BillOfMaterialController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BillOfMaterial::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BillOfMaterial::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BillOfMaterial::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BillOfMaterial::createOrFail($req->all());

        return redirect("bill-of-material/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = BillOfMaterial::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BillOfMaterial::findOrFail($id)->updateOrFail($req->all());

        return redirect("bill-of-material/edit/$id");
    }

    public function getView($id)
    {
        $model = BillOfMaterial::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BillOfMaterial::findOrFail($id)->delete();

        return redirect("bill-of-material");
    }
}
