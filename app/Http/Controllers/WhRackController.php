<?php

namespace App\Http\Controllers;

use App\Models\WhRack;
use Illuminate\Http\Request;
use Datatables;

class WhRackController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => WhRack::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(WhRack::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => WhRack::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = WhRack::createOrFail($req->all());

        return redirect("wh-rack/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = WhRack::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        WhRack::findOrFail($id)->updateOrFail($req->all());

        return redirect("wh-rack/edit/$id");
    }

    public function getView($id)
    {
        $model = WhRack::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        WhRack::findOrFail($id)->delete();

        return redirect("wh-rack");
    }
}
