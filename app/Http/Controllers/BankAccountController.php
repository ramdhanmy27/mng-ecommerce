<?php

namespace App\Http\Controllers;

use App\Models\BankAccount;
use Illuminate\Http\Request;
use Datatables;

class BankAccountController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BankAccount::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BankAccount::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BankAccount::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BankAccount::createOrFail($req->all());

        return redirect("bank-account/edit/$model->account_code");
    }

    public function getEdit($id)
    {
        $model = BankAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BankAccount::findOrFail($id)->updateOrFail($req->all());

        return redirect("bank-account/edit/$id");
    }

    public function getView($id)
    {
        $model = BankAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BankAccount::findOrFail($id)->delete();

        return redirect("bank-account");
    }
}
