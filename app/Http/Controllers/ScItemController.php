<?php

namespace App\Http\Controllers;

use App\Models\ScItem;
use Illuminate\Http\Request;
use Datatables;

class ScItemController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ScItem::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ScItem::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ScItem::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ScItem::createOrFail($req->all());

        return redirect("sc-item/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ScItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ScItem::findOrFail($id)->updateOrFail($req->all());

        return redirect("sc-item/edit/$id");
    }

    public function getView($id)
    {
        $model = ScItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ScItem::findOrFail($id)->delete();

        return redirect("sc-item");
    }
}
