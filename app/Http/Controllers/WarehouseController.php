<?php

namespace App\Http\Controllers;

use App\Models\Warehouse;
use Illuminate\Http\Request;
use Datatables;

class WarehouseController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Warehouse::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Warehouse::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Warehouse::createOrFail($req->all());

        return redirect("warehouse/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Warehouse::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Warehouse::findOrFail($id)->updateOrFail($req->all());

        return redirect("warehouse/edit/$id");
    }

    public function getView($id)
    {
        $model = Warehouse::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Warehouse::findOrFail($id)->delete();

        return redirect("warehouse");
    }
}
