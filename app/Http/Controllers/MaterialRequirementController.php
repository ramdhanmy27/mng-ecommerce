<?php

namespace App\Http\Controllers;

use App\Models\MaterialRequirement;
use Illuminate\Http\Request;
use Datatables;

class MaterialRequirementController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => MaterialRequirement::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(MaterialRequirement::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => MaterialRequirement::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = MaterialRequirement::createOrFail($req->all());

        return redirect("material-requirement/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = MaterialRequirement::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        MaterialRequirement::findOrFail($id)->updateOrFail($req->all());

        return redirect("material-requirement/edit/$id");
    }

    public function getView($id)
    {
        $model = MaterialRequirement::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        MaterialRequirement::findOrFail($id)->delete();

        return redirect("material-requirement");
    }
}
