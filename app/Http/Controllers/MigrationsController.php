<?php

namespace App\Http\Controllers;

use App\Models\Migrations;
use Illuminate\Http\Request;
use Datatables;

class MigrationsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Migrations::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Migrations::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Migrations::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Migrations::createOrFail($req->all());

        return redirect("migrations/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Migrations::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Migrations::findOrFail($id)->updateOrFail($req->all());

        return redirect("migrations/edit/$id");
    }

    public function getView($id)
    {
        $model = Migrations::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Migrations::findOrFail($id)->delete();

        return redirect("migrations");
    }
}
