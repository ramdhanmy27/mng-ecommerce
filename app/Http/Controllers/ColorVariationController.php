<?php

namespace App\Http\Controllers;

use App\Models\ColorVariation;
use Illuminate\Http\Request;
use Datatables;

class ColorVariationController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ColorVariation::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ColorVariation::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ColorVariation::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ColorVariation::createOrFail($req->all());

        return redirect("color-variation/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ColorVariation::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ColorVariation::findOrFail($id)->updateOrFail($req->all());

        return redirect("color-variation/edit/$id");
    }

    public function getView($id)
    {
        $model = ColorVariation::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ColorVariation::findOrFail($id)->delete();

        return redirect("color-variation");
    }
}
