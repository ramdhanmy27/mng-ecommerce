<?php

namespace App\Http\Controllers;

use App\Models\CashAccount;
use Illuminate\Http\Request;
use Datatables;

class CashAccountController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => CashAccount::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(CashAccount::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => CashAccount::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = CashAccount::createOrFail($req->all());

        return redirect("cash-account/edit/$model->account_code");
    }

    public function getEdit($id)
    {
        $model = CashAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        CashAccount::findOrFail($id)->updateOrFail($req->all());

        return redirect("cash-account/edit/$id");
    }

    public function getView($id)
    {
        $model = CashAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        CashAccount::findOrFail($id)->delete();

        return redirect("cash-account");
    }
}
