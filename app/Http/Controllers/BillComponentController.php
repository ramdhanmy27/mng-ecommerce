<?php

namespace App\Http\Controllers;

use App\Models\BillComponent;
use Illuminate\Http\Request;
use Datatables;

class BillComponentController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BillComponent::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BillComponent::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BillComponent::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BillComponent::createOrFail($req->all());

        return redirect("bill-component/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = BillComponent::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BillComponent::findOrFail($id)->updateOrFail($req->all());

        return redirect("bill-component/edit/$id");
    }

    public function getView($id)
    {
        $model = BillComponent::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BillComponent::findOrFail($id)->delete();

        return redirect("bill-component");
    }
}
