<?php

namespace App\Http\Controllers;

use App\Models\ChequeBook;
use Illuminate\Http\Request;
use Datatables;

class ChequeBookController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ChequeBook::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ChequeBook::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ChequeBook::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ChequeBook::createOrFail($req->all());

        return redirect("cheque-book/edit/$model->serial_no");
    }

    public function getEdit($id)
    {
        $model = ChequeBook::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ChequeBook::findOrFail($id)->updateOrFail($req->all());

        return redirect("cheque-book/edit/$id");
    }

    public function getView($id)
    {
        $model = ChequeBook::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ChequeBook::findOrFail($id)->delete();

        return redirect("cheque-book");
    }
}
