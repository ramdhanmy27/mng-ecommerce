<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Datatables;

class SupplierController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "model" => new Supplier,
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Supplier::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => new Supplier,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Supplier::createOrFail($req->all());

        return redirect("supplier/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Supplier::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Supplier::findOrFail($id)->updateOrFail($req->all());

        return redirect("supplier/edit/$id");
    }

    public function getView($id)
    {
        $model = Supplier::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Supplier::findOrFail($id)->delete();

        return redirect("supplier");
    }
}
