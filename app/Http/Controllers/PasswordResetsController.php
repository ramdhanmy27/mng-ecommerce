<?php

namespace App\Http\Controllers;

use App\Models\PasswordResets;
use Illuminate\Http\Request;
use Datatables;

class PasswordResetsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => PasswordResets::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(PasswordResets::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => PasswordResets::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = PasswordResets::createOrFail($req->all());

        return redirect("password-resets/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = PasswordResets::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        PasswordResets::findOrFail($id)->updateOrFail($req->all());

        return redirect("password-resets/edit/$id");
    }

    public function getView($id)
    {
        $model = PasswordResets::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        PasswordResets::findOrFail($id)->delete();

        return redirect("password-resets");
    }
}
