<?php

namespace App\Http\Controllers;

use App\Models\RackSlot;
use Illuminate\Http\Request;
use Datatables;

class RackSlotController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => RackSlot::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(RackSlot::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => RackSlot::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = RackSlot::createOrFail($req->all());

        return redirect("rack-slot/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = RackSlot::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        RackSlot::findOrFail($id)->updateOrFail($req->all());

        return redirect("rack-slot/edit/$id");
    }

    public function getView($id)
    {
        $model = RackSlot::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        RackSlot::findOrFail($id)->delete();

        return redirect("rack-slot");
    }
}
