<?php

namespace App\Http\Controllers;

use App\Models\WhStock;
use Illuminate\Http\Request;
use Datatables;

class WhStockController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => WhStock::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(WhStock::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => WhStock::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = WhStock::createOrFail($req->all());

        return redirect("wh-stock/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = WhStock::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        WhStock::findOrFail($id)->updateOrFail($req->all());

        return redirect("wh-stock/edit/$id");
    }

    public function getView($id)
    {
        $model = WhStock::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        WhStock::findOrFail($id)->delete();

        return redirect("wh-stock");
    }
}
