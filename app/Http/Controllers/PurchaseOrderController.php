<?php

namespace App\Http\Controllers;

use App\Models\PoItems;
use App\Models\PurchaseOrder;
use DB;
use Datatables;
use Illuminate\Http\Request;

class PurchaseOrderController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(
            PurchaseOrder::get()->map(function($model, $i) {
                $model->po_type = $model->config("type.$model->po_type");
                $model->po_status = $model->config("status.$model->po_status", "Open");

                return $model;
            })
        )->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => new PurchaseOrder([
                "po_date" => date("Y/m/d"),
                "deliv_address" => "Bank Office",
                "pay_method" => PurchaseOrder::METHOD_TRANSFER,
                "po_type" => PurchaseOrder::TYPE_BARANG,
            ]),
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = PurchaseOrder::createOrFail($req->all());

        return redirect("purchase-order/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = PurchaseOrder::findOrFail($id);

        return view([
            "id" => $id,
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        PurchaseOrder::findOrFail($id)->updateOrFail($req->all());

        return redirect("purchase-order/edit/$id");
    }

    public function getView($id)
    {
        $model = PurchaseOrder::findOrFail($id);

        return view([
            "model" => $model,
            "po_items" => PoItems::where("po_number", $model->po_number)
                ->join("units as u", "u.unit_code", "=", "po_items.unit_code")
                ->select("po_items.*", "u.unit_name")
                ->get(),
        ]);
    }

    public function getDelete($id)
    {
        PurchaseOrder::findOrFail($id)->delete();

        return redirect("purchase-order");
    }

    public function getClose($id)
    {
        PurchaseOrder::findOrFail($id)->updateOrFail([
            "po_status" => PurchaseOrder::STATUS_CLOSE,
        ]);

        return redirect("purchase-order");
    }
}
