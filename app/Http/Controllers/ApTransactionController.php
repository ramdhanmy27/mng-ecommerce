<?php

namespace App\Http\Controllers;

use App\Models\ApTransaction;
use Illuminate\Http\Request;
use Datatables;

class ApTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ApTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ApTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ApTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ApTransaction::createOrFail($req->all());

        return redirect("ap-transaction/edit/$model->trans_no");
    }

    public function getEdit($id)
    {
        $model = ApTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ApTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("ap-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = ApTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ApTransaction::findOrFail($id)->delete();

        return redirect("ap-transaction");
    }
}
