<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Datatables;

class UsersController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Users::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Users::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Users::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Users::createOrFail($req->all());

        return redirect("users/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Users::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Users::findOrFail($id)->updateOrFail($req->all());

        return redirect("users/edit/$id");
    }

    public function getView($id)
    {
        $model = Users::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Users::findOrFail($id)->delete();

        return redirect("users");
    }
}
