<?php

namespace App\Http\Controllers;

use App\Models\MaterialTransaction;
use Illuminate\Http\Request;
use Datatables;

class MaterialTransactionController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => MaterialTransaction::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(MaterialTransaction::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => MaterialTransaction::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = MaterialTransaction::createOrFail($req->all());

        return redirect("material-transaction/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = MaterialTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        MaterialTransaction::findOrFail($id)->updateOrFail($req->all());

        return redirect("material-transaction/edit/$id");
    }

    public function getView($id)
    {
        $model = MaterialTransaction::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        MaterialTransaction::findOrFail($id)->delete();

        return redirect("material-transaction");
    }
}
