<?php

namespace App\Http\Controllers;

use App\Models\ApInitBalance;
use Illuminate\Http\Request;
use Datatables;

class ApInitBalanceController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ApInitBalance::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ApInitBalance::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ApInitBalance::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ApInitBalance::createOrFail($req->all());

        return redirect("ap-init-balance/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ApInitBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ApInitBalance::findOrFail($id)->updateOrFail($req->all());

        return redirect("ap-init-balance/edit/$id");
    }

    public function getView($id)
    {
        $model = ApInitBalance::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ApInitBalance::findOrFail($id)->delete();

        return redirect("ap-init-balance");
    }
}
