<?php

namespace App\Http\Controllers;

use App\Models\LoanFacility;
use Illuminate\Http\Request;
use Datatables;

class LoanFacilityController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => LoanFacility::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(LoanFacility::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => LoanFacility::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = LoanFacility::createOrFail($req->all());

        return redirect("loan-facility/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = LoanFacility::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        LoanFacility::findOrFail($id)->updateOrFail($req->all());

        return redirect("loan-facility/edit/$id");
    }

    public function getView($id)
    {
        $model = LoanFacility::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        LoanFacility::findOrFail($id)->delete();

        return redirect("loan-facility");
    }
}
