<?php

namespace App\Http\Controllers;

use App\Models\PackingList;
use Illuminate\Http\Request;
use Datatables;

class PackingListController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => PackingList::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(PackingList::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => PackingList::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = PackingList::createOrFail($req->all());

        return redirect("packing-list/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = PackingList::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        PackingList::findOrFail($id)->updateOrFail($req->all());

        return redirect("packing-list/edit/$id");
    }

    public function getView($id)
    {
        $model = PackingList::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        PackingList::findOrFail($id)->delete();

        return redirect("packing-list");
    }
}
