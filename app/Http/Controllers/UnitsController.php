<?php

namespace App\Http\Controllers;

use App\Models\Units;
use Illuminate\Http\Request;
use Datatables;

class UnitsController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(Units::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => new Units,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Units::createOrFail($req->all());

        return redirect("units/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Units::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Units::findOrFail($id)->updateOrFail($req->all());

        return redirect("units/edit/$id");
    }

    public function getView($id)
    {
        $model = Units::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Units::findOrFail($id)->delete();

        return redirect("units");
    }
}
