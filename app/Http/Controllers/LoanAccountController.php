<?php

namespace App\Http\Controllers;

use App\Models\LoanAccount;
use Illuminate\Http\Request;
use Datatables;

class LoanAccountController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => LoanAccount::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(LoanAccount::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => LoanAccount::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = LoanAccount::createOrFail($req->all());

        return redirect("loan-account/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = LoanAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        LoanAccount::findOrFail($id)->updateOrFail($req->all());

        return redirect("loan-account/edit/$id");
    }

    public function getView($id)
    {
        $model = LoanAccount::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        LoanAccount::findOrFail($id)->delete();

        return redirect("loan-account");
    }
}
