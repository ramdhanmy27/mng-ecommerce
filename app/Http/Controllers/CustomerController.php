<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Datatables;

class CustomerController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "model" => new Customer,
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Customer::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Customer::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Customer::createOrFail($req->all());

        return redirect("customer/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Customer::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Customer::findOrFail($id)->updateOrFail($req->all());

        return redirect("customer/edit/$id");
    }

    public function getView($id)
    {
        $model = Customer::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Customer::findOrFail($id)->delete();

        return redirect("customer");
    }
}
