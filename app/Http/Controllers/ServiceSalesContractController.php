<?php

namespace App\Http\Controllers;

use App\Models\ServiceSalesContract;
use Illuminate\Http\Request;
use Datatables;

class ServiceSalesContractController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => ServiceSalesContract::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(ServiceSalesContract::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => ServiceSalesContract::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = ServiceSalesContract::createOrFail($req->all());

        return redirect("service-sales-contract/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = ServiceSalesContract::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        ServiceSalesContract::findOrFail($id)->updateOrFail($req->all());

        return redirect("service-sales-contract/edit/$id");
    }

    public function getView($id)
    {
        $model = ServiceSalesContract::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        ServiceSalesContract::findOrFail($id)->delete();

        return redirect("service-sales-contract");
    }
}
