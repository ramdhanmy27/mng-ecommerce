<?php

namespace App\Http\Controllers;

use App\Models\MatTransItem;
use Illuminate\Http\Request;
use Datatables;

class MatTransItemController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => MatTransItem::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(MatTransItem::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => MatTransItem::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = MatTransItem::createOrFail($req->all());

        return redirect("mat-trans-item/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = MatTransItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        MatTransItem::findOrFail($id)->updateOrFail($req->all());

        return redirect("mat-trans-item/edit/$id");
    }

    public function getView($id)
    {
        $model = MatTransItem::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        MatTransItem::findOrFail($id)->delete();

        return redirect("mat-trans-item");
    }
}
