<?php

namespace App\Http\Controllers;

use App\Models\BoxContent;
use Illuminate\Http\Request;
use Datatables;

class BoxContentController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => BoxContent::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(BoxContent::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => BoxContent::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = BoxContent::createOrFail($req->all());

        return redirect("box-content/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = BoxContent::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        BoxContent::findOrFail($id)->updateOrFail($req->all());

        return redirect("box-content/edit/$id");
    }

    public function getView($id)
    {
        $model = BoxContent::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        BoxContent::findOrFail($id)->delete();

        return redirect("box-content");
    }
}
