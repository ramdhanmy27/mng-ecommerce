<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Datatables;

class MenuController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => Menu::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Menu::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Menu::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Menu::createOrFail($req->all());

        return redirect("menu/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = Menu::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Menu::findOrFail($id)->updateOrFail($req->all());

        return redirect("menu/edit/$id");
    }

    public function getView($id)
    {
        $model = Menu::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Menu::findOrFail($id)->delete();

        return redirect("menu");
    }
}
