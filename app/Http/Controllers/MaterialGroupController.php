<?php

namespace App\Http\Controllers;

use App\Models\MaterialGroup;
use Illuminate\Http\Request;
use Datatables;

class MaterialGroupController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([]);
    }

    public function anyData()
    {
        return Datatables::of(MaterialGroup::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => MaterialGroup::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = MaterialGroup::createOrFail($req->all());

        return redirect("material-group/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = MaterialGroup::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        MaterialGroup::findOrFail($id)->updateOrFail($req->all());

        return redirect("material-group/edit/$id");
    }

    public function getView($id)
    {
        $model = MaterialGroup::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        MaterialGroup::findOrFail($id)->delete();

        return redirect("material-group");
    }
}
