<?php

namespace App\Http\Controllers;

use App\Models\JobOrder;
use Illuminate\Http\Request;
use Datatables;

class JobOrderController extends\App\Http\Controllers\Controller
{
    public static $permission = ["add", "edit", "delete", "view"];

    public function getIndex()
    {
        return view([
            "list" => JobOrder::all(),
        ]);
    }

    public function anyData()
    {
        return Datatables::of(JobOrder::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => JobOrder::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = JobOrder::createOrFail($req->all());

        return redirect("job-order/edit/$model->id");
    }

    public function getEdit($id)
    {
        $model = JobOrder::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        JobOrder::findOrFail($id)->updateOrFail($req->all());

        return redirect("job-order/edit/$id");
    }

    public function getView($id)
    {
        $model = JobOrder::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        JobOrder::findOrFail($id)->delete();

        return redirect("job-order");
    }
}
