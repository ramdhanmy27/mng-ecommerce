<?php

/*
|--------------------------------------------------------------------------
| ApplicationRoutes
|--------------------------------------------------------------------------
|
| Thisroutegroupappliesthe "web" middlewaregrouptoeveryroute
| itcontains. The "web" middlewaregroupisdefinedinyourHTTP
| kernelandincludessessionstate, CSRFprotection, andmore.
|
*/

Route::group(['middleware' => "web"], function() {
    Route::auth();

    Route::group(["prefix" => "service"], function() {
        Route::controller("data", "DataController");
    });

    Route::group(['middleware' => "auth"], function() {
        Route::get("/", function() {
            return view("app");
        });

        Route::controllers([
            "brand" => "BrandController",
            "guaranted-asset" => "GuarantedAssetController",
            "material" => "MaterialController",
            "material-group" => "MaterialGroupController",
            "material-requirement" => "MaterialRequirementController",
            "material-transaction" => "MaterialTransactionController",
            "packing-box" => "PackingBoxController",
            "packing-list" => "PackingListController",
            "product-sales-contract" => "ProductSalesContractController",
            "product-variation" => "ProductVariationController",
            "production-batch" => "ProductionBatchController",
            "production-job" => "ProductionJobController",
            "purchase-order" => "PurchaseOrderController",
            "rack-slot" => "RackSlotController",
            "ready-product" => "ReadyProductController",
            "receiving" => "ReceivingController",
            "recurrent-bill" => "RecurrentBillController",
            "sc-item" => "ScItemController",
            "service-sales-contract" => "ServiceSalesContractController",
            "size-set" => "SizeSetController",
            "storing" => "StoringController",
            "supplier" => "SupplierController",
            "supplier-bill" => "SupplierBillController",
            "units" => "UnitsController",
            "unstored-stock" => "UnstoredStockController",
            "warehouse" => "WarehouseController",
            "wh-rack" => "WhRackController",
            "wh-stock" => "WhStockController",
            "ar-bill-detail" => "ArBillDetailController",
            "account-receivable" => "AccountReceivableController",
            "accounting-period" => "AccountingPeriodController",
            "ap-account" => "ApAccountController",
            "ap-init-balance" => "ApInitBalanceController",
            "ap-transaction" => "ApTransactionController",
            "ar-bill" => "ArBillController",
            "ar-init-balance" => "ArInitBalanceController",
            "ar-transaction" => "ArTransactionController",
            "bank-account" => "BankAccountController",
            "bank-office" => "BankOfficeController",
            "bank-transaction" => "BankTransactionController",
            "bill-component" => "BillComponentController",
            "bill-of-material" => "BillOfMaterialController",
            "box-content" => "BoxContentController",
            "branch-office" => "BranchOfficeController",
            "cash-account" => "CashAccountController",
            "cash-transaction" => "CashTransactionController",
            "cheque" => "ChequeController",
            "cheque-book" => "ChequeBookController",
            "cheque-in-hand" => "ChequeInHandController",
            "color-variation" => "ColorVariationController",
            "components" => "ComponentsController",
            "customer" => "CustomerController",
            "gl-account" => "GlAccountController",
            "gl-account-group" => "GlAccountGroupController",
            "initial-balance" => "InitialBalanceController",
            "invoice" => "InvoiceController",
            "job-order" => "JobOrderController",
            "journal" => "JournalController",
            "journal-item" => "JournalItemController",
            "loan-account" => "LoanAccountController",
            "loan-facility" => "LoanFacilityController",
            "loan-transaction" => "LoanTransactionController",
            "mat-trans-item" => "MatTransItemController",
        ]);

        Route::controllers([
            "po-items/{po_number}" => "PoItemsController",
            "receive-item/{rcv_number}" => "ReceiveItemController",
        ]);
    });
});