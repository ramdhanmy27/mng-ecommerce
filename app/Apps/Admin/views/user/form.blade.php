<?php 
    use App\Models\User;
    use App\Models\Role; 
?>

@section("input", true)

{!! Form::model($model) !!}
    {!! Form::rules(User::$rules) !!}

	{!! Form::group('text', 'name', 'Name') !!}
	{!! Form::group('text', 'email', 'Email') !!}
	{!! Form::group('password', 'password', 'Password') !!}
	{!! Form::group('password', 'repassword', 'Confirm Password') !!}
	{!! Form::group('checkboxes', 'roles[]', 'Roles', Role::lists("display_name", "id"), isset($roles) ? $roles : []) !!}

    <div class="form-group">
    	<div class="col-md-offset-3 col-md-9">
    		{!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
	</div>
{!! Form::close() !!}