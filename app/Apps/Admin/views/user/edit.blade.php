@extends("app")

@section("title", trans("action.update")." | User")

@section("content")
    @include("user.form", [
    	"model" => $model,
    ])
@endsection
