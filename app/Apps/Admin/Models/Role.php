<?php

namespace App\Apps\Admin\Models;

class Role extends \App\Models\Model 
{
	protected $fillable = ["name", "display_name", "description"];

    public static $rules = [
        "display_name" => "required",
    	"description" => "max:255",
    ];

    protected function setNameAttribute($value)
    {
        $this->attributes["name"] = $value;
        $this->attributes["display_name"] = id2title($value);
    }

    protected function setDisplayNameAttribute($value)
    {
        $this->attributes["name"] = strtolower(str_replace(" ", "_", $value));
        $this->attributes["display_name"] = $value;
    }
}