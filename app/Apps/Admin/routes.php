<?php

Route::group(["middleware" => ["web", "auth"]], function() {
	Route::menu(["title" => "Home", "url" => "admin", "icon" => "fa fa-home"]);

	Route::menu(["title" => "Settings", "icon" => "fa fa-cog"], function() {
        Route::menu(["title" => "User Management", "icon" => "fa fa-user-circle"], function() {
            // Roles
            Route::controller("role", "RoleController")
                ->menu("Roles", "/", "fa fa-address-card");

            // Users, Permissions
            Route::controller("user", "UserController")
                ->menu("Users", "/", "fa fa-users")
                ->menu("Permissions", "permissions", "fa fa-lock");
        });

		Route::controller("menu", "MenuController")
			->menu("Menu", "/", "fa fa-bars");
			
		Route::controller("slider", "SliderController")
			->menu("Slider", "/", "fa fa-object-group");
	});

	Route::controller("/", "HomeController");
});