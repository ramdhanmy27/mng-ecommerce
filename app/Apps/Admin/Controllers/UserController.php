<?php

namespace App\Apps\Admin\Controllers;

use App\Exceptions\ValidatorException;
use App\Http\Controllers\Controller;
use App\Models\Model;
use App\Models\Role;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Permission;
use Yajra\Datatables\Facades\Datatables;

class UserController extends Controller 
{
    public static $permission = [
        "create", "view", "edit", "add", "delete", "permission",
    ];
    /**
     * Users List
     * @return View
     */
    public function getIndex() 
    {
        return view("user.index");
    }

    /**
     * DataTable Response
     * @return Response
     */
    public function anyData()
    {
        return Datatables::of(User::select("*"))->make(true);
    }

    /**
     * Create data Page
     * @return View
     */
    public function getAdd() 
    {
        User::$rules += ["password" => "required", "repassword" => "required"];

        return view("user.add", [
            "model" => new User,
        ]);
    }

    /**
     * Create new User
     * @param  Request $request
     * @return Response
     */
    public function postAdd(Request $req) 
    {
        $data = $req->except("_token");
        $this->validateUser($data);
        $data["password"] = bcrypt($data["password"]);

        $model = User::create($data);
        $model->registerRoles($req->input("roles", []));

        return redirect("admin/user");
    }

    /**
     * Edit page
     * @param  Integer $id
     * @return View
     */
    public function getEdit($id) 
    {
        return view("user.edit", [
            "model" => User::findOrFail($id),
            "roles" => DB::table("role_user")->where("user_id", $id)->pluck("role_id")
        ]);
    }

    /**
     * Update User
     * @param  Request $request
     * @return Response
     */
    public function postEdit(Request $req, $id) 
    {
        $data = $req->except("_token");
        $this->validateUser($data);

        // update password if available
        if (is_numeric($data["password"]) || !empty($data["password"])) 
            $data["password"] = bcrypt($data["password"]);
        else
            unset($data["password"]);

        $model = User::find($id);
        $model->update($data);
        $model->registerRoles($req->input("roles", []));

        return redirect("admin/user");
    }

    /**
     * Validate Data Input
     * @param  array $data
     * @throw ValidatorException
     */
    private function validateUser(array $data) 
    {
        if ($data["password"] != $data["repassword"])
            throw new ValidatorException("Password tidak sama");

        $validator = Model::makeValidator($data, User::$rules, [], []);

        if ($validator->fails())
            throw new ValidatorException($validator);
    }

    public function getDelete($id)
    {
        User::findOrFail($id)->delete();
        
        return redirect("admin/user");
    }

	/**
	 * Roles & permissions config
	 * @return View
	 */
    public function getPermissions() 
    {
        $this->reloadPermissions();

        return view([
        	"roles" => Role::pluck("display_name", "id"),
        	"permissions" => collect(DB::select(
	        	"SELECT p.id, p.display_name, p.description, 
                    regexp_replace(name, E':.+', '') as \"group\", 
					array_to_string(array_agg(role_id), ',') as roles 
				from permissions p
				left join permission_role pr on pr.permission_id=p.id
				group by p.id, p.display_name, p.name, p.description"
			))->groupBy("group"),
        ]);
    }

    /**
     * Save permission role
     * @param  Request $req
     * @return Response
     */
    public function postPermissions(Request $req) 
    {
        $data = [];

        foreach ($req->input("perm", []) as $role_id => $permissions) {
            foreach ($permissions as $perm) {
                $data[] = [
                    "permission_id" => $perm,
                    "role_id" => $role_id,
                ];
            }
        }

    	if (count($data) > 0) {
    		DB::transaction(function() use ($data) 
            {
	    		DB::table("permission_role")->delete();
		    	DB::table("permission_role")->insert($data);
    		});
    	}

    	return back();
    }

    private function reloadPermissions()
    {
        $dir = ["App\\Http\\Controllers" => base_path("app/Http/Controllers")];

        foreach (glob(base_path("app/Apps/*"), GLOB_ONLYDIR) as $module_path) {
            $module = basename($module_path);
            $dir["App\\Apps\\$module\\Controllers"] = $module_path."/Controllers";
        }

        app("permission")->initAllController($dir);
    }
}