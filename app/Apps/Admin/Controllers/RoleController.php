<?php

namespace App\Apps\Admin\Controllers;

use App\Apps\Admin\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class RoleController extends Controller
{
    public function getIndex()
    {
        return view([
            "model" => new Role,
        ]);
    }

    public function anyData()
    {
        return Datatables::of(Role::select("*"))->make(true);
    }

    public function getAdd()
    {
        return view([
            "model" => Role::class,
        ]);
    }

    public function postAdd(Request $req)
    {
        $model = Role::createOrFail(input_filter($req->all()));

        return redirect("admin/role");
    }

    public function getEdit($id)
    {
        $model = Role::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function postEdit(Request $req, $id)
    {
        Role::findOrFail($id)->updateOrFail(input_filter($req->all()));
        
        return redirect("admin/role");
    }

    public function getView($id)
    {
        $model = Role::findOrFail($id);

        return view([
            "model" => $model,
        ]);
    }

    public function getDelete($id)
    {
        Role::findOrFail($id)->delete();
        
        return redirect("admin/role");
    }
}
