<?php

namespace App\Apps\Admin\Controllers;

use App\Apps\Admin\Models\Menu;
use App\Exceptions\ServiceException;
use Illuminate\Http\Request;

class MenuController extends \App\Http\Controllers\Controller {

	public function getIndex() 
	{
		return view([]);
	}

	public function getAdd($parent = null) 
	{
		$model = new Menu;

		// default value
		$model->enable = true;
		$model->parent = $parent;

		return view("menu.add", [
			"model" => $model,
			"options" => [
				"parent" => $this->getMenuTreeOptions(),
			],
		]);
	}

	public function postAdd(Request $req) 
	{
        Menu::createOrFail($req->input());

        return redirect("admin/menu");
	}

	public function getEdit($id) 
	{
		return view("menu.edit", [
			"model" => Menu::findOrFail($id),
			"options" => [
				"parent" => $this->getMenuTreeOptions($id),
			],
		]);
	}

	public function postEdit(Request $req, $id) 
	{
        Menu::findOrFail($id)->update($req->input());

        return redirect("admin/menu");
	}

	public function postDelete($id) 
	{
		$model = Menu::findOrFail($id);

		// move entire child to grand parent
		Menu::where("parent", $model->id)->update(["parent" => $model->parent]);

		// delete menu and fix order
        $model->delete();
        \Menu::driver("db")->fixOrder();

		return back();
	}

	public function postSwitchEnable($id) 
	{
        $model = Menu::findOrFail($id);
        $model->update(["enable" => !$model->enable]);

		return back();
	}

	public function postOrder(Request $req) 
	{
		$success = \Menu::driver("db")->setOrder(
			$req->input("id"), 
			$req->input("parent"), 
			$req->input("order")
		);

		if (!$success)
			throw new ServiceException("Failed to update position");

        \Menu::driver("db")->fixOrder();
	}

	/**
	 * return tree menu as fetch key pair array
	 * @param  array|int $except
	 * @return array
	 */
	private function getMenuTreeOptions($except = null)
	{
		$data = [];

		foreach (\Menu::getCollection() as $id => $menu) {
			// skip branches
			if (count(array_intersect(is_array($except) ? $except : func_get_args(), $menu->path)) > 0)
				continue;

			$data[$id] = str_fill(" - - ", $menu->level).trans_label($menu->title);
		}

		return $data;
	}
}
