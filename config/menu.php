<?php

return [
    "default" => env("MENU_DRIVER", "db"),

	"separate" => env("MENU_SEPARATE", false),

	"drivers" => [
		"db" => env("DB_CONNECTION", "pgsql"),
	],
];