<?php

use Illuminate\Database\Seeder;

abstract class CSVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function seedFromCSV($file, $class, $delimiter = ",")
    {
        $file = fopen(base_path('storage/app/seeds/'.$file), "r");
        $loop = false;
        $data = [];
        $x = 0;

        // clear all data
        $this->clear($class);

        while (!feof($file)) {
            $csv = fgetcsv($file, 0, $delimiter);

            if (!$loop) {
                $column = $csv;
                $loop = true;
            }
            else {
                for ($i = 0; $i < count($csv); $i++) {
                    $data[$x][$column[$i]] = (utf8_encode($csv[$i])) ?: NULL;
                }

                $x++;
            }

            // insert data secara parsial
            if (count($data) > 1000) {
                $class::insert($data);
                $data = [];
            }
        }

        // last insert
        if (count($data) > 0)
            $class::insert($data);

        fclose($file);
    }

    public function clear($class)
    {
        $class::query()->delete();
    }
}
