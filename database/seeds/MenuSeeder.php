<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            1 => [
                "title" => "label.setting", 
                "param" => json_encode([
                    "icon" => "fa fa-cog",
                    "permission" => null,
                ]),
            ],
                2 => [
                    "parent" => 1, 
                    "title" => "Organization", 
                    "param" => json_encode([
                        "icon" => "fa fa-bank",
                        "permission" => null,
                    ]),
                ],
                    3 => [
                        "parent" => 2, 
                        "title" => "ref.lob.title", 
                        "url" => "ref/line-of-bussiness", 
                        "param" => json_encode([
                            "icon" => "fa fa-building",
                            "permission" => null,
                        ]),
                    ],
                    8 => [
                        "parent" => 2, 
                        "title" => "ref.wl.title", 
                        "url" => "ref/work-location", 
                        "param" => json_encode([
                            "icon" => "fa fa-map",
                            "permission" => null,
                        ]),
                    ],
                    9 => [
                        "parent" => 2, 
                        "title" => "ref.factory.title", 
                        "url" => "ref/factory", 
                        "param" => json_encode([
                            "icon" => "fa fa-industry",
                            "permission" => null,
                        ]),
                    ],
                4 => [
                    "parent" => 1, 
                    "title" => "System", 
                    "param" => json_encode([
                        "icon" => "fa fa-wrench",
                        "permission" => null,
                    ]),
                ],
                    5 => [
                        "parent" => 4, 
                        "title" => "User", 
                        "url" => "admin/user", 
                        "param" => json_encode([
                            "icon" => "fa fa-users",
                            "permission" => null,
                        ]),
                    ],
                    6 => [
                        "parent" => 4, 
                        "title" => "Permission", 
                        "url" => "admin/user/permissions", 
                        "param" => json_encode([
                            "icon" => "fa fa-lock",
                            "permission" => null,
                        ]),
                    ],
                    7 => [
                        "parent" => 4, 
                        "title" => "Menu", 
                        "url" => "admin/menu", 
                        "param" => json_encode([
                            "icon" => "fa fa-th-list",
                            "permission" => null,
                        ]),
                    ],
        ];

        $insert = [];

        foreach ($data as $id => $attr) {
            $attr["id"] = $id;
            $attr["parent"] = isset($attr["parent"]) ? $attr["parent"] : null;
            $attr["url"] = isset($attr["url"]) ? $attr["url"] : null;
            $attr["enable"] = isset($attr["enable"]) ? $attr["enable"] : true;
            $attr["order"] = isset($attr["order"]) ? $attr["order"] : 0;
            $attr["param"] = isset($attr["param"]) ? $attr["param"] : null;

            $insert[] = $attr;
        }

        $this->clear();
        DB::table("menu")->insert($insert);
        DB::select("SELECT setval('menu_id_seq', (SELECT MAX(id) FROM menu))");
    }

    public function clear()
    {
        DB::table("menu")->delete();
    }
}
