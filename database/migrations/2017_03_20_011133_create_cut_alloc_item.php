<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutAllocItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cut_alloc_item", function(Blueprint $table) {
            $table->increments("id");
            $table->decimal("qty", 15, 2)->nullable();

            $table->integer("cut_alloc_id");
            $table->string("raw_mat_code", 20);
            $table->integer("mat_color_id");
            $table->string("unit_code", 5);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("cut_alloc_item");
    }
}
