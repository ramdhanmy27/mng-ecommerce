<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialRequirement extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('material_requirement', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('material_name', 24)->nullable();
            $table->decimal('qty', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('material_requirement');
    }
}
