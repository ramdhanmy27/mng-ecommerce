<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSupplier extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::table("supplier", function(Blueprint $table) {
            $table->string("code", 10)->unique();
            $table->boolean("is_company")->nullable();
            $table->char("type", 1)->nullable();
            $table->string("city", 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::table("supplier", function(Blueprint $table) {
            $table->dropColumn([
                "code", "is_company", "type", "city",
            ]);
        });
    }
}
