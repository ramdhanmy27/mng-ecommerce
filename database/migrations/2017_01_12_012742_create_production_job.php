<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionJob extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('production_job', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamp('start_date')->nullable();
            $table->string('duration')->nullable();
            $table->timestamp('finish_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('production_job');
    }
}
