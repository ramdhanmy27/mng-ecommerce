<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTransaction extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('bank_transaction', function(Blueprint $table)
        {
            $table->string('trans_no', 255)->primary();
            $table->string('reff_no', 10)->nullable();
            $table->timestamp('trans_date')->nullable();
            $table->integer('trans_type')->nullable();
            $table->decimal('trans_value', 15, 2)->nullable();
            $table->boolean('is_debet')->nullable();
            $table->string('description', 32)->nullable();
            $table->boolean('is_reconciled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('bank_transaction');
    }
}
