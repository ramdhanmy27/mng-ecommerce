<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingAllocation extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cutting_allocation", function(Blueprint $table) {
            $table->increments("id");
            $table->string("book_number", 10);
            $table->date("book_date");

            $table->string("wh_code", 10);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("cutting_allocation");
    }
}
