<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashAccount extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('cash_account', function(Blueprint $table)
        {
            $table->char('account_code')->primary();
            $table->string('account_name', 16)->nullable();
            $table->string('description', 32)->nullable();
            $table->char('currency', 3)->nullable();
            $table->integer('cash_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cash_account');
    }
}
