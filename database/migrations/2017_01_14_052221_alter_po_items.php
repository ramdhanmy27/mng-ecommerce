<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPoItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("po_items", function(Blueprint $table) {
            $table->string("po_number")->nullable();
            $table->integer("material_id")->nullable();
            $table->char("unit_code", 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("po_items", function(Blueprint $table)
        {
            $table->dropColumn(["material_id", "unit_code", "po_number"]);
        });
    }
}
