<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoice extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('invoice', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('invoice_no', 12)->nullable();
            $table->date('invoice_date')->nullable();
            $table->integer('payment_type')->nullable();
            $table->date('due_date')->nullable();
            $table->char('currency', 3)->nullable();
            $table->decimal('total_value', 15, 2)->nullable();
            $table->string('payment_bank_account', 48)->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('invoice');
    }
}
