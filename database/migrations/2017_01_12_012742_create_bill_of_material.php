<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillOfMaterial extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('bill_of_material', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('bo_m_no', 10)->nullable();
            $table->boolean('is_multilevel')->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('bill_of_material');
    }
}
