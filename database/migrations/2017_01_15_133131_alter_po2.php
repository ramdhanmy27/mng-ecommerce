<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPo2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function(Blueprint $table) {
            $table->dropColumn("po_number");
            
            $table->string("po_number")->nullable();
            $table->increments("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("purchase_order", function(Blueprint $table) {
            $table->string("po_number")->primary();
            $table->dropColumn(["id"]);
        });
    }
}
