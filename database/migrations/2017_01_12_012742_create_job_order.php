<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrder extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('job_order', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('job_no')->nullable();
            $table->date('job_date')->nullable();
            $table->string('description')->nullable();
            $table->integer('qty')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('job_order');
    }
}
