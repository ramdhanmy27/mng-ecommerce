<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplier extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('supplier', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('supp_name', 24)->nullable();
            $table->string('address', 48)->nullable();
            $table->char('post_code', 6)->nullable();
            $table->string('phone', 13)->nullable();
            $table->string('fax', 13)->nullable();
            $table->string('mail', 24)->nullable();
            $table->string('contact_name', 16)->nullable();
            $table->string('description', 32)->nullable();
            $table->string('npwp', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('supplier');
    }
}
