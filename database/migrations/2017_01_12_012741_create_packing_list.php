<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackingList extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('packing_list', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('delivery_date')->nullable();
            $table->integer('total_qty')->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('packing_list');
    }
}
