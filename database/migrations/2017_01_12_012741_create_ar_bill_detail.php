<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArBillDetail extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('ar_bill_detail', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('description', 36)->nullable();
            $table->decimal('bill_value', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('ar_bill_detail');
    }
}
