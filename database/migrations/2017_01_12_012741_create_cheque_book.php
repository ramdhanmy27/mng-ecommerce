<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeBook extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('cheque_book', function(Blueprint $table)
        {
            $table->string('serial_no', 255)->primary();
            $table->integer('page_count')->nullable();
            $table->boolean('is_activated')->nullable();
            $table->boolean('is_bilyet')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cheque_book');
    }
}
