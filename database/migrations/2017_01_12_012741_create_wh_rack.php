<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhRack extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("wh_rack", function(Blueprint $table) {
            $table->increments("id");
            $table->char("rack_code", 6)->nullable();
            $table->integer("width")->nullable();
            $table->integer("length")->nullable();
            $table->integer("height")->nullable();
            $table->integer("shelf_count")->nullable();
            $table->string("description", 16)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('wh_rack');
    }
}
