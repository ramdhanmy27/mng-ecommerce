<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheque extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('cheque', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('serial_no', 12)->nullable();
            $table->integer('status')->nullable();
            $table->decimal('cheque_value', 15, 2)->nullable();
            $table->string('holder_name', 16)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cheque');
    }
}
