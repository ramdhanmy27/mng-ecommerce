<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanAccount extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('loan_account', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('account_no', 16)->nullable();
            $table->string('account_name', 24)->nullable();
            $table->string('description')->nullable();
            $table->char('currency', 3)->nullable();
            $table->decimal('balance', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('loan_account');
    }
}
