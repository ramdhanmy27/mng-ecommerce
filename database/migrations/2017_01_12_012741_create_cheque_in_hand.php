<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeInHand extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('cheque_in_hand', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('serial_no', 12)->nullable();
                $table->boolean('is_bilyet')->nullable();
                $table->decimal('cheque_value', 15, 2)->nullable();
                $table->date('cheque_date')->nullable();
                $table->string('account_owner', 16)->nullable();
                $table->integer('status')->nullable();
                $table->string('giver_name')->nullable();
            });
        }

        /**
         * Reverse the migrations.
         * @return  void
         */
        public function down()
        {
            Schema::drop('cheque_in_hand');
        }
    }
