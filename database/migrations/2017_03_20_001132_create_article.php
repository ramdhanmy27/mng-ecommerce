<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticle extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("article", function(Blueprint $table) {
            $table->string("number", 20)->primary();
            $table->string("name", 50);
            $table->boolean("any_embro")->nullable();
            $table->boolean("any_print")->nullable();
            $table->string("description")->nullable();

            $table->string("brand_code", 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('article');
    }
}
