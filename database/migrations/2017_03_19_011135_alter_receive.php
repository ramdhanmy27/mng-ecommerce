<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceive extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::table("receiving", function(Blueprint $table) {
            $table->string("supp_deliv_num", 12)->nullable();
            $table->date("supp_deliv_date")->nullable();

            // $table->string("po_number", 10);
            $table->string("supp_code", 10);
            $table->string("wh_code", 10);
        });

        Schema::table("receive_item", function(Blueprint $table) {
            $table->decimal("sub_total", 15, 2)->nullable();

            // $table->string("rcv_number", 10);
            $table->string("raw_mat_code", 20);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::table("receiving", function(Blueprint $table) {
            $table->dropColumn(["supp_deliv_num", "supp_deliv_date"]);
        });

        Schema::table("receive_item", function(Blueprint $table) {
            $table->dropColumn(["sub_total"]);
        });
    }
}
