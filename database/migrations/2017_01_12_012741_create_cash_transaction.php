<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashTransaction extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('cash_transaction', function(Blueprint $table)
        {
            $table->string('trans_no', 255)->primary();
            $table->date('trans_date')->nullable();
            $table->integer('trans_type')->nullable();
            $table->decimal('trans_value', 15, 2)->nullable();
            $table->boolean('is_debet')->nullable();
            $table->string('party_name', 16)->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cash_transaction');
    }
}
