<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponents extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('components', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('partname', 24)->nullable();
            $table->integer('required_qty')->nullable();
            $table->string('description', 32)->nullable();
            $table->boolean('is_has_detail')->nullable();
            $table->boolean('is_helper_material')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('components');
    }
}
