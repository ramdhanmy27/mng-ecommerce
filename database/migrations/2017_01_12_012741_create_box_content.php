<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxContent extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('box_content', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('qty')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('box_content');
    }
}
