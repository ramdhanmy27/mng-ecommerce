<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchOffice extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('branch_office', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('branch_name', 24)->nullable();
            $table->string('address', 48)->nullable();
            $table->char('post_code', 6)->nullable();
            $table->string('phone', 13)->nullable();
            $table->string('mail', 24)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('branch_office');
    }
}
