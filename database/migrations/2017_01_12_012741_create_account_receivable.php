<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountReceivable extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('account_receivable', function(Blueprint $table)
        {
            $table->string('ar_code', 255)->primary();
            $table->string('ar_name', 16)->nullable();
            $table->char('curr', 3)->nullable();
            $table->decimal('balance', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('account_receivable');
    }
}
