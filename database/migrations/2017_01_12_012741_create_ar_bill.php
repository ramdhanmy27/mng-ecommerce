<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArBill extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('ar_bill', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('bill_no', 12)->nullable();
            $table->string('description', 36)->nullable();
            $table->date('bill_date')->nullable();
            $table->decimal('bill_value', 15, 2)->nullable();
            $table->date('due_date')->nullable();
            $table->boolean('is_paid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('ar_bill');
    }
}
