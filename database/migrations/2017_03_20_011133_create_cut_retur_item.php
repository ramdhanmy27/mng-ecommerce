<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutReturItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cut_retur_item", function(Blueprint $table) {
            $table->increments("id");
            $table->decimal("qty", 15, 2)->nullable();

            $table->integer("cut_alloc_retur_id");
            $table->integer("cut_alloc_item_id");
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("cut_retur_item");
    }
}
