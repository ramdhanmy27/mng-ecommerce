<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPo2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function(Blueprint $table) {
            $table->dropColumn(["supplier_id"]);

            $table->string("po_number", 10)->unique();

            $table->string("supp_code", 10);
            $table->string("raw_mat_code", 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn(["supp_code"]);
        
        $table->integer("supplier_id");
    }
}
