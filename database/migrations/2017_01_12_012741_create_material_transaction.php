<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialTransaction extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('material_transaction', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('trans_no', 10)->nullable();
            $table->string('reff_no', 10)->nullable();
            $table->date('trans_date')->nullable();
            $table->integer('trans_type')->nullable();
            $table->boolean('is_debet')->nullable();
            $table->string('description', 24)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('material_transaction');
    }
}
