<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrand extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('brand', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('brand_name', 16)->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('brand');
    }
}
