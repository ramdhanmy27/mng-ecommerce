<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("purchase_order", function(Blueprint $table) {
            $table->string("po_number", 255)->primary();
            $table->date("po_date")->nullable();
            $table->integer("po_status")->nullable();
            $table->integer("po_type")->nullable();
            $table->string("deliv_address", 32)->nullable();
            $table->date("deliv_due_date")->nullable();
            $table->integer("pay_method")->nullable();
            $table->char("currency", 3)->nullable();
            $table->decimal("total_value", 15, 2)->nullable();
            $table->string("description", 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("purchase_order");
    }
}
