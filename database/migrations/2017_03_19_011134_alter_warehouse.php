<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWarehouse extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::table("warehouse", function(Blueprint $table) {
            $table->dropColumn(["width", "length", "height", ]);

            $table->decimal("capacity", 15, 2)->nullable();
            $table->string("description")->nullable();
            $table->string("location")->nullable();
            $table->string("wh_code", 10)->unique();
            $table->string("wh_name", 50);
        });

        Schema::table("wh_rack", function(Blueprint $table) {
            $table->dropColumn(["width", "length", "height", "rack_code", ]);

            $table->string("wh_code", 10);
            $table->string("rack_no", 10)->unique();
            $table->decimal("capacity", 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down() 
    {
        Schema::table("warehouse", function(Blueprint $table) {
            $table->integer("width")->nullable();
            $table->integer("length")->nullable();
            $table->integer("height")->nullable();

            $table->dropColumn("capacity");
        });

        Schema::table("wh_rack", function(Blueprint $table) {
            $table->integer("width")->nullable();
            $table->integer("length")->nullable();
            $table->integer("height")->nullable();

            $table->dropColumn("capacity");
        });
    }
}
