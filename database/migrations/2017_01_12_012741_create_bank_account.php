<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccount extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('bank_account', function(Blueprint $table)
        {
            $table->char('account_code')->primary();
            $table->string('account_number', 16)->nullable();
            $table->string('account_name', 24)->nullable();
            $table->char('currency', 3)->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('bank_account');
    }
}
