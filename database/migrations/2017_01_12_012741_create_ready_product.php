<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadyProduct extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('ready_product', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('prod_name')->nullable();
            $table->string('description', 32)->nullable();
            $table->integer('total_qty')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('ready_product');
    }
}
