<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatTransItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('mat_trans_item', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal('qty', 15, 2)->nullable();
            $table->integer('pack_qty')->nullable();
            $table->decimal('external_qty', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('mat_trans_item');
    }
}
