<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialColor extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("material_color", function(Blueprint $table) {
            $table->increments("id");
            $table->string("color_name", 50)->nullable();
            $table->string("description")->nullable();

            $table->string("raw_mat_code", 20);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("material_color");
    }
}
