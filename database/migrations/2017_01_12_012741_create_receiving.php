<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiving extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("receiving", function(Blueprint $table) {
            $table->increments("id");
            $table->string("rcv_number", 10)->nullable();
            $table->date("rcv_date")->nullable();
            $table->string("description", 36)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("receiving");
    }
}
