<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtColor extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("art_color", function(Blueprint $table) {
            $table->increments("id");
            $table->string("name", 20)->nullable();
            $table->string("description")->nullable();

            $table->string("number", 20)->primary();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("art_color");
    }
}
