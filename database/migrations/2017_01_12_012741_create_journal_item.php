<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('journal_item', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('description', 24)->nullable();
            $table->boolean('is_debet')->nullable();
            $table->decimal('journal_value', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('journal_item');
    }
}
