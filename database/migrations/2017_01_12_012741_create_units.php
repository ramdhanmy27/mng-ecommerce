<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnits extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("units", function(Blueprint $table) {
            $table->increments("id");
            $table->char("unit_code", 5)->nullable();
            $table->integer("dimension_type")->nullable();
            $table->string("unit_name", 16)->nullable();
            $table->decimal("base_conversion", 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('units');
    }
}
