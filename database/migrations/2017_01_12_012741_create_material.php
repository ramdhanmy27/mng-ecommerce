<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterial extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('material', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('mat_code', 12)->nullable();
            $table->string('mat_name', 24)->nullable();
            $table->string('description', 32)->nullable();
            $table->string('attr1_value', 20)->nullable();
            $table->string('attr2_value', 20)->nullable();
            $table->string('attr3_value', 20)->nullable();
            $table->string('attr4_value', 20)->nullable();
            $table->string('attr5_value', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('material');
    }
}
