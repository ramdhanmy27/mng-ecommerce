<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlAccount extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('gl_account', function(Blueprint $table)
        {
            $table->string('account_code', 255)->primary();
            $table->string('account_name', 24)->nullable();
            $table->decimal('balance', 15, 2)->nullable();
            $table->boolean('is_sys_account')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('gl_account');
    }
}
