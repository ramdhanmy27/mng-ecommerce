<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSalesContract extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('service_sales_contract', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('sc_number', 12)->nullable();
            $table->string('reff_number', 12)->nullable();
            $table->date('sc_date')->nullable();
            $table->string('buyer_name', 16)->nullable();
            $table->text('scope_of_work')->nullable();
            $table->decimal('total_value', 15, 2)->nullable();
            $table->date('due_date')->nullable();
            $table->string('description', 32)->nullable();
            $table->string('attached_file', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('service_sales_contract');
    }
}
