<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanFacility extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('loan_facility', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('agreement_no', 12)->nullable();
            $table->date('agreement_date')->nullable();
            $table->char('currency', 3)->nullable();
            $table->decimal('plafond_value', 15, 2)->nullable();
            $table->string('description', 32)->nullable();
            $table->date('valid_from')->nullable();
            $table->date('valid_until')->nullable();
            $table->decimal('admin_fee', 15, 2)->nullable();
            $table->decimal('provision_fee', 15, 2)->nullable();
            $table->decimal('interest_rate', 15, 2)->nullable();
            $table->string('attached_file', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('loan_facility');
    }
}
