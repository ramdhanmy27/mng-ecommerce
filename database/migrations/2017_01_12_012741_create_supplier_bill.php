<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierBill extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('supplier_bill', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('bill_no', 16)->nullable();
            $table->date('bill_date')->nullable();
            $table->integer('payment_type')->nullable();
            $table->date('due_date')->nullable();
            $table->char('currency', 3)->nullable();
            $table->decimal('total_value', 15, 2)->nullable();
            $table->string('description', 32)->nullable();
            $table->string('attached_file', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('supplier_bill');
    }
}
