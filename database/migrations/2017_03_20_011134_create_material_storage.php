<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialStorage extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("material_storage", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("qty")->default(0);
            $table->integer("booked_qty")->default(0);

            $table->string("wh_code", 10);
            $table->string("rack_no", 10);
            $table->string("raw_mat_code", 20);
            $table->integer("mat_color_id");
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("material_storage");
    }
}
