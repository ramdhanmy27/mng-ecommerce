<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoring extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('storing', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('store_date')->nullable();
            $table->decimal('qty', 15, 2)->nullable();
            $table->integer('pack_qty')->nullable();
            $table->boolean('is_picking')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('storing');
    }
}
