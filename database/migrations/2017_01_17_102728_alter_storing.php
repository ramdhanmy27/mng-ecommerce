<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('storing', function(Blueprint $table) {
            $table->string("mat_code", 12);
            $table->char("slot_code", 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("storing", function(Blueprint $table) {
            $table->char(["slot_code", "mat_code"]);
        });
    }
}
