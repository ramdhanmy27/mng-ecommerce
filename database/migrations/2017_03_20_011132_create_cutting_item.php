<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cutting_item", function(Blueprint $table) {
            $table->increments("id");
            $table->decimal("raw_kg_qty", 15, 2)->nullable();
            $table->decimal("raw_roll_qty", 15, 2)->nullable();
            $table->decimal("cut_qty", 15, 2)->nullable();
            $table->decimal("ratio", 15, 2)->nullable();
            $table->decimal("unit_price", 15, 2)->nullable();

            $table->integer("cutting_id");
            $table->string("raw_mat_code", 20);
            $table->integer("mat_color_id");
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("cutting_item");
    }
}
