<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRackSlot extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('rack_slot', function(Blueprint $table)
        {
            $table->increments('id');
            $table->char('slot_code', 8)->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('shelf_level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('rack_slot');
    }
}
