<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScItem extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('sc_item', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('item_name', 16)->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('unit_price', 15, 2)->nullable();
            $table->decimal('sub_total', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('sc_item');
    }
}
