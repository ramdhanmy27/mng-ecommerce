<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceving extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receiving', function(Blueprint $table) {
            $table->string('po_number', 12)->nullable();
        });

        Schema::table('receive_item', function(Blueprint $table) {
            $table->string('rcv_number', 10)->nullable();
            $table->integer('material_id')->nullable();
            $table->char('unit_code', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receiving', function(Blueprint $table) {
            $table->dropColumn(['po_number']);
        });

        Schema::table('receive_item', function(Blueprint $table) {
            $table->dropColumn(['rcv_number', 'material_id', 'unit_code']);
        });
    }
}
