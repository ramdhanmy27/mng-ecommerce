<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeSet extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('size_set', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('size_list', 48)->nullable();
            $table->string('description', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('size_set');
    }
}
