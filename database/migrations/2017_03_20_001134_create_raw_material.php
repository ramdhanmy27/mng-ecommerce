<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawMaterial extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("raw_material", function(Blueprint $table) {
            $table->string("code", 20)->primary();
            $table->string("name", 50)->nullable();
            $table->char("kind", 1)->nullable();
            $table->string("description")->nullable();

            $table->string("unit_code", 5);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("raw_material");
    }
}
