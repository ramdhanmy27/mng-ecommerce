<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutAllocRetur extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cut_alloc_retur", function(Blueprint $table) {
            $table->increments("id");
            $table->string("ret_number", 10);

            $table->integer("cut_alloc_id");
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cut_alloc_retur');
    }
}
