<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("brand", function(Blueprint $table) {
            $table->dropColumn(["brand_name"]);

            $table->string("code", 10)->unique();
            $table->string("name", 50);
            $table->string("description")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("brand", function(Blueprint $table) {
            $table->dropColumn(["code"]);
        });
    }
}
