<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialGroup extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('material_group', function(Blueprint $table)
        {
            $table->increments('id');
            $table->char('grp_code', 3)->nullable();
            $table->string('grp_name', 16)->nullable();
            $table->integer('more_attr_cnt')->nullable();
            $table->string('attr1_name', 12)->nullable();
            $table->string('attr2_name', 12)->nullable();
            $table->string('attr3_name', 12)->nullable();
            $table->string('attr4_name', 12)->nullable();
            $table->string('attr5_name', 12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('material_group');
    }
}
