<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlAccountGroup extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('gl_account_group', function(Blueprint $table)
        {
            $table->string('group_code', 255)->primary();
            $table->string('group_name', 16)->nullable();
            $table->boolean('is_balance_sheet')->nullable();
            $table->boolean('is_debetpositive')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('gl_account_group');
    }
}
