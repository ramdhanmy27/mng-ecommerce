<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhStock extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('wh_stock', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal('qty', 15, 2)->nullable();
            $table->integer('pack_qty')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('wh_stock');
    }
}
