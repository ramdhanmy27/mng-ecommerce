<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutting extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("cutting", function(Blueprint $table) {
            $table->increments("id");
            $table->string("wo_number", 10)->nullable();
            $table->date("wo_date")->nullable();
            $table->date("target_finish_date")->nullable();
            $table->date("actual_finish_date")->nullable();

            $table->string("article_number", 20)->nullable();
            $table->string("supp_code", 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('cutting');
    }
}
