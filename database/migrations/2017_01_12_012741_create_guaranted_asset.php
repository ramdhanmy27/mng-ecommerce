<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuarantedAsset extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('guaranted_asset', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('asset_no', 12)->nullable();
            $table->string('asset_name', 24)->nullable();
            $table->string('avalist_name', 24)->nullable();
            $table->decimal('asset_value', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('guaranted_asset');
    }
}
