<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionBatch extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('production_batch', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('batch_no', 10)->nullable();
            $table->string('product_name', 24)->nullable();
            $table->integer('total_qty')->nullable();
            $table->date('mfg_start_date')->nullable();
            $table->date('mfg_finish_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('production_batch');
    }
}
