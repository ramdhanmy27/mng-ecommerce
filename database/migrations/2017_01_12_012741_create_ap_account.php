<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApAccount extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('ap_account', function(Blueprint $table)
        {
            $table->string('ap_code', 255)->primary();
            $table->string('ap_name', 16)->nullable();
            $table->char('curr', 3)->nullable();
            $table->decimal('balance', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop('ap_account');
    }
}
