<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankOffice extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create('bank_office', function(Blueprint $table) {
                $table->increments('id');
                $table->string('address')->nullable(); 
                $table->string('phone', 14)->nullable(); 
                $table->string('contact_person', 16)->nullable();
            });
        }

        /**
         * Reverse the migrations.
         * @return  void
         */
        public function down()
        {
            Schema::drop('bank_office');
        }
    }
