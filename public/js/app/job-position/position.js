$(document).ready(function(){
    $('#id_employee_group').change(function(){

        $('#grades').html('');

        $.get(fn.url.base + "/job-position/employee-group/data/grades", {
            id: this.value
        }, function(res) {
            if (res) {
                if (! !!res)
                    $('#grades').parent().addClass('hide');
                else
                    $('#grades').parent().removeClass('hide');

                for (var key in res) {
                    // data.push({id: key, text: res[key]});
                    var ele = '<div class="checkbox-custom ">' +
                              '<input class="form-control" name="grades[]" type="checkbox" value="'+ key +'">' + 
                              '<label>' + res[key] + '</label>' + 
                              '</div>';
                    
                    $('#grades').append(ele);
                }

            }

        }) 
    });
});