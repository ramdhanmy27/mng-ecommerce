$(document).ready(function(){
	$('#id_employee_group').change(function(){
		$("#id_job_grade option").remove();
		
		$.get(fn.url.base + "/job-position/employee-group/data/grades", {
			id: this.value
		}, function(res) {
			if (res) {
				var data = [];

				for (var key in res)
					data.push({id: key, text: res[key]});

				$("#id_job_grade").select2({data: data});
			}

		}) 
	});
});