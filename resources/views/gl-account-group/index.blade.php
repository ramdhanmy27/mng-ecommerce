@extends("app")

@section("title", "Gl Account Group")

@section("content")
    <a href="{{ url("gl-account-group/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("gl-account-group/data") }}">
        <thead>
            <tr>
                                    <th dt-field="group_code"> Group Code </th>
                                    <th dt-field="group_name"> Group Name </th>
                                    <th dt-field="is_balance_sheet"> Is Balance Sheet </th>
                                    <th dt-field="is_debetpositive"> Is Debetpositive </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("gl-account-group/edit/[[group_code]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("gl-account-group/delete/[[group_code]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
