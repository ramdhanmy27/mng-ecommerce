@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "group_name") !!}
	{!! Form::group("text", "is_balance_sheet") !!}
	{!! Form::group("text", "is_debetpositive") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}