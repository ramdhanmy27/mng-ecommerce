@extends("app")

@section("title", trans("action.update")." | Gl Account Group")

@section("content")
    @include("gl-account-group.form", [
    	"model" => $model,
    ])
@endsection
