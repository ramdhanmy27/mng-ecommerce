@extends("app")

@section("title", "Gl Account Group")

@section("content")
    <div class="row">
        <a href="{{ url("gl-account-group") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("gl-account-group/delete/$model->group_code") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("gl-account-group/edit/$model->group_code") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Group Code</th>
                <td>{{ $model->group_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Group Name</th>
                <td>{{ $model->group_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Balance Sheet</th>
                <td>{{ $model->is_balance_sheet }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Debetpositive</th>
                <td>{{ $model->is_debetpositive }}</td>
            </tr>
                </table>
    </div>
@endsection