<?php


$class = [];

if (isset($menu->childView))
    $class[] = "nav-parent";

// if ($menu->active === true)
if ($active) 
    $class[] = "nav-active";

?>

<li {!! count($class) ? "class='".implode(" ", $class)."'" : "" !!}>
    <a href='{{ isset($menu->url) && !empty($menu->url) ? url($menu->url) : "#" }}'>
        @if (!empty($menu->icon))
            <i class='{{ $menu->icon }}'></i>
        @endif
        <span>{{ trans_label($menu->title) }}</span>
    </a>

    @if (isset($menu->childView))
	    <span class="toggler">
	    	<span class="glyphicon glyphicon-chevron-down"></span>
	    </span>
	    
        <ul class='nav nav-children'>{!! $menu->childView !!}</ul>
    @endif
</li>