@extends("app")

@section("title", trans("action.update")." | Journal Item")

@section("content")
    @include("journal-item.form", [
    	"model" => $model,
    ])
@endsection
