@extends("app")

@section("title", trans("action.create")." | Journal Item")

@section("content")
    @include("journal-item.form", [
        "model" => $model,
    ])
@endsection
