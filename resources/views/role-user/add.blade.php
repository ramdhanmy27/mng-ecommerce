@extends("app")

@section("title", trans("action.create")." | Role User")

@section("content")
    @include("role-user.form", [
        "model" => $model,
    ])
@endsection
