@extends("app")

@section("title", trans("action.update")." | Role User")

@section("content")
    @include("role-user.form", [
    	"model" => $model,
    ])
@endsection
