@extends("app")

@section("title", "Role User")

@section("content")
    <div class="row">
        <a href="{{ url("role-user") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("role-user/delete/$model->user_id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("role-user/edit/$model->user_id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Role Id</th>
                <td>{{ $model->role_id }}</td>
            </tr>
                    <tr>
                <th class="text-right">User Id</th>
                <td>{{ $model->user_id }}</td>
            </tr>
                </table>
    </div>
@endsection