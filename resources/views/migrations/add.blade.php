@extends("app")

@section("title", trans("action.create")." | Migrations")

@section("content")
    @include("migrations.form", [
        "model" => $model,
    ])
@endsection
