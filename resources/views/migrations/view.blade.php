@extends("app")

@section("title", "Migrations")

@section("content")
    <div class="row">
        <a href="{{ url("migrations") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("migrations/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("migrations/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Batch</th>
                <td>{{ $model->batch }}</td>
            </tr>
                    <tr>
                <th class="text-right">Migration</th>
                <td>{{ $model->migration }}</td>
            </tr>
                </table>
    </div>
@endsection