@extends("app")

@section("title", trans("action.update")." | Migrations")

@section("content")
    @include("migrations.form", [
    	"model" => $model,
    ])
@endsection
