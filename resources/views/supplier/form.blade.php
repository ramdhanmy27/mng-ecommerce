@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "supp_name","Nama Supplier") !!}
	{!! Form::group("text", "contact_name","Nama Kontak") !!}
	{!! Form::group("number", "npwp","NPWP") !!}
	{!! Form::group("textarea", "address","Alamat Supplier") !!}
	{!! Form::group("text", "post_code","Kode Pos") !!}
	{!! Form::group("text", "phone","No. Telepon") !!}
	{!! Form::group("text", "fax","No. Fax") !!}
	{!! Form::group("email", "mail","Alamat E-mail") !!}
	{!! Form::group("textarea", "description","Deskripsi") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}