@extends("app")

@section("title", trans("action.update")." | Supplier")

@section("content")
    @include("supplier.form", [
    	"model" => $model,
    ])
@endsection
