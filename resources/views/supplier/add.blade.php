@extends("app")

@section("title", trans("action.create")." | Supplier")

@section("content")
    @include("supplier.form", [
        "model" => $model,
    ])
@endsection
