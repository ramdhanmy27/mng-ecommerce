@extends("app")

@section("title", trans("action.create")." | Production Job")

@section("content")
    @include("production-job.form", [
        "model" => $model,
    ])
@endsection
