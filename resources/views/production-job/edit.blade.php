@extends("app")

@section("title", trans("action.update")." | Production Job")

@section("content")
    @include("production-job.form", [
    	"model" => $model,
    ])
@endsection
