@extends("app")

@section("title", "Production Job")

@section("content")
    <a href="{{ url("production-job/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("production-job/data") }}">
        <thead>
            <tr>
                                    <th dt-field="duration"> Duration </th>
                                    <th dt-field="finish_date"> Finish Date </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="start_date"> Start Date </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("production-job/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("production-job/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
