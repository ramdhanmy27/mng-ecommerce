@extends("app")

@section("title", "Production Job")

@section("content")
    <div class="row">
        <a href="{{ url("production-job") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("production-job/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("production-job/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Duration</th>
                <td>{{ $model->duration }}</td>
            </tr>
                    <tr>
                <th class="text-right">Finish Date</th>
                <td>{{ $model->finish_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Start Date</th>
                <td>{{ $model->start_date }}</td>
            </tr>
                </table>
    </div>
@endsection