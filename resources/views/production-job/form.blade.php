@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "duration") !!}
	{!! Form::group("text", "finish_date") !!}
	{!! Form::group("text", "start_date") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}