@extends("app")

@section("title", trans("action.create")." | Password Resets")

@section("content")
    @include("password-resets.form", [
        "model" => $model,
    ])
@endsection
