@extends("app")

@section("title", "Password Resets")

@section("content")
    <div class="row">
        <a href="{{ url("password-resets") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("password-resets/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("password-resets/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Created At</th>
                <td>{{ $model->created_at }}</td>
            </tr>
                    <tr>
                <th class="text-right">Email</th>
                <td>{{ $model->email }}</td>
            </tr>
                    <tr>
                <th class="text-right">Token</th>
                <td>{{ $model->token }}</td>
            </tr>
                </table>
    </div>
@endsection