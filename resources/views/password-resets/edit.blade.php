@extends("app")

@section("title", trans("action.update")." | Password Resets")

@section("content")
    @include("password-resets.form", [
    	"model" => $model,
    ])
@endsection
