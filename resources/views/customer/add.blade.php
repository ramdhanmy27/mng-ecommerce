@extends("app")

@section("title", trans("action.create")." | Customer")

@section("content")
    @include("customer.form", [
        "model" => $model,
    ])
@endsection
