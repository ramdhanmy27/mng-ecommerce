@extends("app")

@section("title", "Customer")

@section("content")
    <a href="{{ url("customer/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("customer/data") }}">
        <thead>
            <tr>
                <th dt-field="cust_name"> {{ $model->label("cust_name") }} </th>
                <th dt-field="contact_name"> {{ $model->label("contact_name") }} </th>
                <th dt-field="fax"> {{ $model->label("fax") }} </th>
                <th dt-field="mail"> {{ $model->label("mail") }} </th>
                <th dt-field="phone"> {{ $model->label("phone") }} </th>
                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("customer/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("customer/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
