@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "cust_name") !!}
    {!! Form::group("text", "npwp") !!}
    {!! Form::group("text", "contact_name") !!}
	{!! Form::group("text", "mail") !!}
    {!! Form::group("text", "fax") !!}
    {!! Form::group("text", "phone") !!}
    {!! Form::group("text", "post_code") !!}
    {!! Form::group("textarea", "address") !!}
	{!! Form::group("textarea", "description") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}