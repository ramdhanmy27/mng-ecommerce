@extends("app")

@section("title", trans("action.update")." | Customer")

@section("content")
    @include("customer.form", [
    	"model" => $model,
    ])
@endsection
