@extends("app")

@section("title", trans("action.update")." | Warehouse")

@section("content")
    @include("warehouse.form", [
    	"model" => $model,
    ])
@endsection
