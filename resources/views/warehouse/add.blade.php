@extends("app")

@section("title", trans("action.create")." | Warehouse")

@section("content")
    @include("warehouse.form", [
        "model" => $model,
    ])
@endsection
