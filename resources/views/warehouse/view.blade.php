@extends("app")

@section("title", "Warehouse")

@section("content")
    <div class="row">
        <a href="{{ url("warehouse") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("warehouse/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("warehouse/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Height</th>
                <td>{{ $model->height }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Length</th>
                <td>{{ $model->length }}</td>
            </tr>
                    <tr>
                <th class="text-right">Location</th>
                <td>{{ $model->location }}</td>
            </tr>
                    <tr>
                <th class="text-right">Wh Code</th>
                <td>{{ $model->wh_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Wh Name</th>
                <td>{{ $model->wh_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Width</th>
                <td>{{ $model->width }}</td>
            </tr>
                </table>
    </div>
@endsection