@extends("app")

@section("title", "Warehouse")

@section("content")
    <a href="{{ url("warehouse/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("warehouse/data") }}">
        <thead>
            <tr>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="height"> Height </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="length"> Length </th>
                                    <th dt-field="location"> Location </th>
                                    <th dt-field="wh_code"> Wh Code </th>
                                    <th dt-field="wh_name"> Wh Name </th>
                                    <th dt-field="width"> Width </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("warehouse/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("warehouse/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
