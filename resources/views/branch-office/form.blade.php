@section("input", true)

{!! Form::model($model) !!}
    {!! Form::group("text", "branch_name") !!}
    {!! Form::group("text", "mail") !!}
    {!! Form::group("text", "phone") !!}
    {!! Form::group("text", "post_code") !!}
	{!! Form::group("textarea", "address") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}