@extends("app")

@section("title", trans("action.create")." | Branch Office")

@section("content")
    @include("branch-office.form", [
        "model" => $model,
    ])
@endsection
