@extends("app")

@section("title", "Branch Office")

@section("content")
    <a href="{{ url("branch-office/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("branch-office/data") }}">
        <thead>
            <tr>
                                    <th dt-field="address"> Address </th>
                                    <th dt-field="branch_name"> Branch Name </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="mail"> Mail </th>
                                    <th dt-field="phone"> Phone </th>
                                    <th dt-field="post_code"> Post Code </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("branch-office/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("branch-office/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
