@extends("app")

@section("title", "Branch Office")

@section("content")
    <div class="row">
        <a href="{{ url("branch-office") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("branch-office/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("branch-office/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Address</th>
                <td>{{ $model->address }}</td>
            </tr>
                    <tr>
                <th class="text-right">Branch Name</th>
                <td>{{ $model->branch_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Mail</th>
                <td>{{ $model->mail }}</td>
            </tr>
                    <tr>
                <th class="text-right">Phone</th>
                <td>{{ $model->phone }}</td>
            </tr>
                    <tr>
                <th class="text-right">Post Code</th>
                <td>{{ $model->post_code }}</td>
            </tr>
                </table>
    </div>
@endsection