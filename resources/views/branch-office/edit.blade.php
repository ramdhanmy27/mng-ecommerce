@extends("app")

@section("title", trans("action.update")." | Branch Office")

@section("content")
    @include("branch-office.form", [
    	"model" => $model,
    ])
@endsection
