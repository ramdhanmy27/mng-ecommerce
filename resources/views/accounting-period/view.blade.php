@extends("app")

@section("title", "Accounting Period")

@section("content")
    <div class="row">
        <a href="{{ url("accounting-period") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("accounting-period/delete/$model->year") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("accounting-period/edit/$model->year") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Year</th>
                <td>{{ $model->year }}</td>
            </tr>
                </table>
    </div>
@endsection