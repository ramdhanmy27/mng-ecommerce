@extends("app")

@section("title", trans("action.create")." | Accounting Period")

@section("content")
    @include("accounting-period.form", [
        "model" => $model,
    ])
@endsection
