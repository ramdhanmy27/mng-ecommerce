@extends("app")

@section("title", trans("action.update")." | Accounting Period")

@section("content")
    @include("accounting-period.form", [
    	"model" => $model,
    ])
@endsection
