@extends("app")

@section("title", trans("action.create")." | Ap Transaction")

@section("content")
    @include("ap-transaction.form", [
        "model" => $model,
    ])
@endsection
