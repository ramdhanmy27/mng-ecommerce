@extends("app")

@section("title", "Ap Transaction")

@section("content")
    <a href="{{ url("ap-transaction/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("ap-transaction/data") }}">
        <thead>
            <tr>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="is_debet"> Is Debet </th>
                                    <th dt-field="is_journalized"> Is Journalized </th>
                                    <th dt-field="trans_date"> Trans Date </th>
                                    <th dt-field="trans_no"> Trans No </th>
                                    <th dt-field="trans_type"> Trans Type </th>
                                    <th dt-field="trans_value"> Trans Value </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("ap-transaction/edit/[[trans_no]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("ap-transaction/delete/[[trans_no]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
