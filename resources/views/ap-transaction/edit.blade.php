@extends("app")

@section("title", trans("action.update")." | Ap Transaction")

@section("content")
    @include("ap-transaction.form", [
    	"model" => $model,
    ])
@endsection
