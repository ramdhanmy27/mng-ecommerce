@extends("app")

@section("title", trans("action.update")." | Box Content")

@section("content")
    @include("box-content.form", [
    	"model" => $model,
    ])
@endsection
