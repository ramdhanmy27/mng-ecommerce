@extends("app")

@section("title", trans("action.create")." | Box Content")

@section("content")
    @include("box-content.form", [
        "model" => $model,
    ])
@endsection
