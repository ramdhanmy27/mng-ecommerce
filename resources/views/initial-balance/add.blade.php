@extends("app")

@section("title", trans("action.create")." | Initial Balance")

@section("content")
    @include("initial-balance.form", [
        "model" => $model,
    ])
@endsection
