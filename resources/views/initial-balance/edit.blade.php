@extends("app")

@section("title", trans("action.update")." | Initial Balance")

@section("content")
    @include("initial-balance.form", [
    	"model" => $model,
    ])
@endsection
