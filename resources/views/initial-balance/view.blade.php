@extends("app")

@section("title", "Initial Balance")

@section("content")
    <div class="row">
        <a href="{{ url("initial-balance") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("initial-balance/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("initial-balance/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Init Value</th>
                <td>{{ $model->init_value }}</td>
            </tr>
                </table>
    </div>
@endsection