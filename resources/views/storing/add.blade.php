@extends("app")

@section("title", trans("action.create")." | Storing")

@section("content")
    @include("storing.form", [
        "model" => $model,
    ])
@endsection
