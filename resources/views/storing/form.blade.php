@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("radios", "is_picking[]", "Is Picking", ["yes" => "Yes", "no" => "No"], ["v1"]) !!}
	{!! Form::group("text", "pack_qty","Pack Quantity") !!}
	{!! Form::group("text", "qty","Quantity") !!}
	{!! Form::group("date", "store_date","Store Date") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}