@extends("app")

@section("title", trans("action.update")." | Storing")

@section("content")
    @include("storing.form", [
    	"model" => $model,
    ])
@endsection
