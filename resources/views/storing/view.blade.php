@extends("app")

@section("title", "Storing")

@section("content")
    <div class="row">
        <a href="{{ url("storing") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("storing/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("storing/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Picking</th>
                <td>{{ $model->is_picking }}</td>
            </tr>
                    <tr>
                <th class="text-right">Pack Qty</th>
                <td>{{ $model->pack_qty }}</td>
            </tr>
                    <tr>
                <th class="text-right">Qty</th>
                <td>{{ $model->qty }}</td>
            </tr>
                    <tr>
                <th class="text-right">Store Date</th>
                <td>{{ $model->store_date }}</td>
            </tr>
                </table>
    </div>
@endsection