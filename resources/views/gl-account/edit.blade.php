@extends("app")

@section("title", trans("action.update")." | Gl Account")

@section("content")
    @include("gl-account.form", [
    	"model" => $model,
    ])
@endsection
