@extends("app")

@section("title", trans("action.create")." | Gl Account")

@section("content")
    @include("gl-account.form", [
        "model" => $model,
    ])
@endsection
