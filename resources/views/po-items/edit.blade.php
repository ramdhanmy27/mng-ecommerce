@extends("app")

@section("title", trans("action.update")." | Po Items")

@section("content")
    @include("po-items.form", [
    	"model" => $model,
    ])
@endsection
