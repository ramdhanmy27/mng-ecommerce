@extends("app")

@section("title", trans("action.create")." | PO Items")

@section("content")
    @include("po-items.form", [
        "model" => $model,
    ])
@endsection
