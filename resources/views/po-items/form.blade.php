<?php

use App\Models\Material;
use App\Models\Units;

?>

@section("input", true)

{!! Form::model($model) !!}
	{!! Form::hidden("po_number", $po_number) !!}
    {!! Form::group("select", "material_id", null, Material::lists("mat_name", "id")) !!}
    {!! Form::group("select", "unit_code", null, Units::lists("unit_name", "unit_code")) !!}
    {!! Form::group("text", "item_name") !!}
    {!! Form::group("text", "qty") !!}
	{!! Form::group("text", "subtotal") !!}
	{!! Form::group("text", "unit_price") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function() {
        $("[name='material_id']").change(function() {
            var $this = $(this);

            $.ajax({
                url: fn.url("service/data/material"),
                data: {id: $this.val()},
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $("[name='item_name']").val(data["mat_name"]);
                    $("[name='unit_code']").val(data["unit_code"]).trigger("change");
                },
            })
        });
    })
</script>
