@extends("app")

@section("title", trans("action.create")." | Bill Of Material")

@section("content")
    @include("bill-of-material.form", [
        "model" => $model,
    ])
@endsection
