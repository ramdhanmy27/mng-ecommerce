@extends("app")

@section("title", "Bill Of Material")

@section("content")
    <a href="{{ url("bill-of-material/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("bill-of-material/data") }}">
        <thead>
            <tr>
                                    <th dt-field="bo_m_no"> Bo M No </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="is_multilevel"> Is Multilevel </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("bill-of-material/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("bill-of-material/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
