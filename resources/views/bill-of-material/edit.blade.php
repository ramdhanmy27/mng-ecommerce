@extends("app")

@section("title", trans("action.update")." | Bill Of Material")

@section("content")
    @include("bill-of-material.form", [
    	"model" => $model,
    ])
@endsection
