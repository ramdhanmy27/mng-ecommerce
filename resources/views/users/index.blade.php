@extends("app")

@section("title", "Users")

@section("content")
    <a href="{{ url("users/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("users/data") }}">
        <thead>
            <tr>
                                    <th dt-field="created_at"> Created At </th>
                                    <th dt-field="email"> Email </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="kode"> Kode </th>
                                    <th dt-field="name"> Name </th>
                                    <th dt-field="password"> Password </th>
                                    <th dt-field="remember_token"> Remember Token </th>
                                    <th dt-field="updated_at"> Updated At </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("users/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("users/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
