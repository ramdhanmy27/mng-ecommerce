@extends("app")

@section("title", trans("action.update")." | Users")

@section("content")
    @include("users.form", [
    	"model" => $model,
    ])
@endsection
