@extends("app")

@section("title", "Users")

@section("content")
    <div class="row">
        <a href="{{ url("users") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("users/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("users/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Created At</th>
                <td>{{ $model->created_at }}</td>
            </tr>
                    <tr>
                <th class="text-right">Email</th>
                <td>{{ $model->email }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Kode</th>
                <td>{{ $model->kode }}</td>
            </tr>
                    <tr>
                <th class="text-right">Name</th>
                <td>{{ $model->name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Password</th>
                <td>{{ $model->password }}</td>
            </tr>
                    <tr>
                <th class="text-right">Remember Token</th>
                <td>{{ $model->remember_token }}</td>
            </tr>
                    <tr>
                <th class="text-right">Updated At</th>
                <td>{{ $model->updated_at }}</td>
            </tr>
                </table>
    </div>
@endsection