@extends("app")

@section("title", trans("action.create")." | Users")

@section("content")
    @include("users.form", [
        "model" => $model,
    ])
@endsection
