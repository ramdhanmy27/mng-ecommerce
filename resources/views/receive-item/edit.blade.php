@extends("app")

@section("title", trans("action.update")." | Receive Item")

@section("content")
    @include("receive-item.form", [
    	"model" => $model,
    ])
@endsection
