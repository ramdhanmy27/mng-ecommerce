@extends("app")

@section("title", trans("action.create")." | Receive Item")

@section("content")
    @include("receive-item.form", [
        "model" => $model,
    ])
@endsection
