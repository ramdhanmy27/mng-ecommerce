@extends("app")

@section("title", trans("action.update")." | Color Variation")

@section("content")
    @include("color-variation.form", [
    	"model" => $model,
    ])
@endsection
