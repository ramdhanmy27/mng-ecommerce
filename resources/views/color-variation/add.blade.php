@extends("app")

@section("title", trans("action.create")." | Color Variation")

@section("content")
    @include("color-variation.form", [
        "model" => $model,
    ])
@endsection
