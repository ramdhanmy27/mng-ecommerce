@extends("app")

@section("title", "Color Variation")

@section("content")
    <div class="row">
        <a href="{{ url("color-variation") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("color-variation/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("color-variation/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Color Name</th>
                <td>{{ $model->color_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                </table>
    </div>
@endsection