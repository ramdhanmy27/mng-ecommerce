@extends("app")

@section("title", trans("action.update")." | Material Requirement")

@section("content")
    @include("material-requirement.form", [
    	"model" => $model,
    ])
@endsection
