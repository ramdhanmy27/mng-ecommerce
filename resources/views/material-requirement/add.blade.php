@extends("app")

@section("title", trans("action.create")." | Material Requirement")

@section("content")
    @include("material-requirement.form", [
        "model" => $model,
    ])
@endsection
