@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "cheque_value") !!}
	{!! Form::group("text", "holder_name") !!}
	{!! Form::group("text", "serial_no") !!}
	{!! Form::group("text", "status") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}