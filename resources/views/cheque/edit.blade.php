@extends("app")

@section("title", trans("action.update")." | Cheque")

@section("content")
    @include("cheque.form", [
    	"model" => $model,
    ])
@endsection
