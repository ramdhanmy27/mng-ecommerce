@extends("app")

@section("title", "Cheque")

@section("content")
    <div class="row">
        <a href="{{ url("cheque") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("cheque/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("cheque/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Cheque Value</th>
                <td>{{ $model->cheque_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Holder Name</th>
                <td>{{ $model->holder_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Serial No</th>
                <td>{{ $model->serial_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Status</th>
                <td>{{ $model->status }}</td>
            </tr>
                </table>
    </div>
@endsection