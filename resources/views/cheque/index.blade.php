@extends("app")

@section("title", "Cheque")

@section("content")
    <a href="{{ url("cheque/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("cheque/data") }}">
        <thead>
            <tr>
                                    <th dt-field="cheque_value"> Cheque Value </th>
                                    <th dt-field="holder_name"> Holder Name </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="serial_no"> Serial No </th>
                                    <th dt-field="status"> Status </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("cheque/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("cheque/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
