@extends("app")

@section("title", trans("action.create")." | Cheque")

@section("content")
    @include("cheque.form", [
        "model" => $model,
    ])
@endsection
