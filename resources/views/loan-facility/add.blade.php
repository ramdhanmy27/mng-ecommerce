@extends("app")

@section("title", trans("action.create")." | Loan Facility")

@section("content")
    @include("loan-facility.form", [
        "model" => $model,
    ])
@endsection
