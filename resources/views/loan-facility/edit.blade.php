@extends("app")

@section("title", trans("action.update")." | Loan Facility")

@section("content")
    @include("loan-facility.form", [
    	"model" => $model,
    ])
@endsection
