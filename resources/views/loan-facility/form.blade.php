@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "admin_fee") !!}
	{!! Form::group("text", "agreement_date") !!}
	{!! Form::group("text", "agreement_no") !!}
	{!! Form::group("text", "attached_file") !!}
	{!! Form::group("text", "currency") !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "interest_rate") !!}
	{!! Form::group("text", "plafond_value") !!}
	{!! Form::group("text", "provision_fee") !!}
	{!! Form::group("text", "valid_from") !!}
	{!! Form::group("text", "valid_until") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}