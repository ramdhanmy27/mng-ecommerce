@extends("app")

@section("title", "Loan Facility")

@section("content")
    <a href="{{ url("loan-facility/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("loan-facility/data") }}">
        <thead>
            <tr>
                                    <th dt-field="admin_fee"> Admin Fee </th>
                                    <th dt-field="agreement_date"> Agreement Date </th>
                                    <th dt-field="agreement_no"> Agreement No </th>
                                    <th dt-field="attached_file"> Attached File </th>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="interest_rate"> Interest Rate </th>
                                    <th dt-field="plafond_value"> Plafond Value </th>
                                    <th dt-field="provision_fee"> Provision Fee </th>
                                    <th dt-field="valid_from"> Valid From </th>
                                    <th dt-field="valid_until"> Valid Until </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("loan-facility/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("loan-facility/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
