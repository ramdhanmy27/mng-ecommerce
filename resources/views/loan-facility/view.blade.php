@extends("app")

@section("title", "Loan Facility")

@section("content")
    <div class="row">
        <a href="{{ url("loan-facility") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("loan-facility/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("loan-facility/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Admin Fee</th>
                <td>{{ $model->admin_fee }}</td>
            </tr>
                    <tr>
                <th class="text-right">Agreement Date</th>
                <td>{{ $model->agreement_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Agreement No</th>
                <td>{{ $model->agreement_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attached File</th>
                <td>{{ $model->attached_file }}</td>
            </tr>
                    <tr>
                <th class="text-right">Currency</th>
                <td>{{ $model->currency }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Interest Rate</th>
                <td>{{ $model->interest_rate }}</td>
            </tr>
                    <tr>
                <th class="text-right">Plafond Value</th>
                <td>{{ $model->plafond_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Provision Fee</th>
                <td>{{ $model->provision_fee }}</td>
            </tr>
                    <tr>
                <th class="text-right">Valid From</th>
                <td>{{ $model->valid_from }}</td>
            </tr>
                    <tr>
                <th class="text-right">Valid Until</th>
                <td>{{ $model->valid_until }}</td>
            </tr>
                </table>
    </div>
@endsection