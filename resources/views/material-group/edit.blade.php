@extends("app")

@section("title", trans("action.update")." | Material Group")

@section("content")
    @include("material-group.form", [
    	"model" => $model,
    ])
@endsection
