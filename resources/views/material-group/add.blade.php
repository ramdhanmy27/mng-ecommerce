@extends("app")

@section("title", trans("action.create")." | Material Group")

@section("content")
    @include("material-group.form", [
        "model" => $model,
    ])
@endsection
