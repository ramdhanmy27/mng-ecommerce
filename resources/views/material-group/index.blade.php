@extends("app")

@section("title", "Material Group")

@section("content")
    <a href="{{ url("material-group/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("material-group/data") }}">
        <thead>
            <tr>
                                    <th dt-field="attr1_name"> Attr1 Name </th>
                                    <th dt-field="attr2_name"> Attr2 Name </th>
                                    <th dt-field="attr3_name"> Attr3 Name </th>
                                    <th dt-field="attr4_name"> Attr4 Name </th>
                                    <th dt-field="attr5_name"> Attr5 Name </th>
                                    <th dt-field="grp_code"> Grp Code </th>
                                    <th dt-field="grp_name"> Grp Name </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="more_attr_cnt"> More Attr Cnt </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("material-group/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("material-group/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
