@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "grp_code") !!}
	{!! Form::group("text", "grp_name") !!}
    {!! Form::group("text", "attr1_name") !!}
    {!! Form::group("text", "attr2_name") !!}
    {!! Form::group("text", "attr3_name") !!}
    {!! Form::group("text", "attr4_name") !!}
    {!! Form::group("text", "attr5_name") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}