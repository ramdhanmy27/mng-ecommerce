@extends("app")

@section("title", "Material Group")

@section("content")
    <div class="row">
        <a href="{{ url("material-group") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("material-group/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("material-group/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Attr1 Name</th>
                <td>{{ $model->attr1_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr2 Name</th>
                <td>{{ $model->attr2_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr3 Name</th>
                <td>{{ $model->attr3_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr4 Name</th>
                <td>{{ $model->attr4_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr5 Name</th>
                <td>{{ $model->attr5_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Grp Code</th>
                <td>{{ $model->grp_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Grp Name</th>
                <td>{{ $model->grp_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">More Attr Cnt</th>
                <td>{{ $model->more_attr_cnt }}</td>
            </tr>
                </table>
    </div>
@endsection