@extends("app")

@section("title", trans("action.update")." | Wh Rack")

@section("content")
    @include("wh-rack.form", [
    	"model" => $model,
    ])
@endsection
