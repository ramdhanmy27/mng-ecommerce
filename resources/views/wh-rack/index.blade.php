@extends("app")

@section("title", "Wh Rack")

@section("content")
    <a href="{{ url("wh-rack/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("wh-rack/data") }}">
        <thead>
            <tr>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="height"> Height </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="length"> Length </th>
                                    <th dt-field="rack_code"> Rack Code </th>
                                    <th dt-field="shelf_count"> Shelf Count </th>
                                    <th dt-field="width"> Width </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("wh-rack/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("wh-rack/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
