@extends("app")

@section("title", trans("action.create")." | Wh Rack")

@section("content")
    @include("wh-rack.form", [
        "model" => $model,
    ])
@endsection
