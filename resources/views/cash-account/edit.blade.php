@extends("app")

@section("title", trans("action.update")." | Cash Account")

@section("content")
    @include("cash-account.form", [
    	"model" => $model,
    ])
@endsection
