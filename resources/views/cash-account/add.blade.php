@extends("app")

@section("title", trans("action.create")." | Cash Account")

@section("content")
    @include("cash-account.form", [
        "model" => $model,
    ])
@endsection
