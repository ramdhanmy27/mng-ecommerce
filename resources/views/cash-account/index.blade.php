@extends("app")

@section("title", "Cash Account")

@section("content")
    <a href="{{ url("cash-account/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("cash-account/data") }}">
        <thead>
            <tr>
                                    <th dt-field="account_code"> Account Code </th>
                                    <th dt-field="account_name"> Account Name </th>
                                    <th dt-field="cash_type"> Cash Type </th>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="description"> Description </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("cash-account/edit/[[account_code]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("cash-account/delete/[[account_code]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
