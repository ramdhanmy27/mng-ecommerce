@extends("app")

@section("title", "Product Sales Contract")

@section("content")
    <a href="{{ url("product-sales-contract/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("product-sales-contract/data") }}">
        <thead>
            <tr>
                                    <th dt-field="attached_file"> Attached File </th>
                                    <th dt-field="billing_address"> Billing Address </th>
                                    <th dt-field="buyer_name"> Buyer Name </th>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="delivery_address"> Delivery Address </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="due_date"> Due Date </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="payment_type"> Payment Type </th>
                                    <th dt-field="reff_number"> Reff Number </th>
                                    <th dt-field="sc_date"> Sc Date </th>
                                    <th dt-field="sc_number"> Sc Number </th>
                                    <th dt-field="total_value"> Total Value </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("product-sales-contract/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("product-sales-contract/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
