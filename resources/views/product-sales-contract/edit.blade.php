@extends("app")

@section("title", trans("action.update")." | Product Sales Contract")

@section("content")
    @include("product-sales-contract.form", [
    	"model" => $model,
    ])
@endsection
