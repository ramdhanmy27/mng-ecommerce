@extends("app")

@section("title", "Product Sales Contract")

@section("content")
    <div class="row">
        <a href="{{ url("product-sales-contract") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("product-sales-contract/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("product-sales-contract/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Attached File</th>
                <td>{{ $model->attached_file }}</td>
            </tr>
                    <tr>
                <th class="text-right">Billing Address</th>
                <td>{{ $model->billing_address }}</td>
            </tr>
                    <tr>
                <th class="text-right">Buyer Name</th>
                <td>{{ $model->buyer_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Currency</th>
                <td>{{ $model->currency }}</td>
            </tr>
                    <tr>
                <th class="text-right">Delivery Address</th>
                <td>{{ $model->delivery_address }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Due Date</th>
                <td>{{ $model->due_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Payment Type</th>
                <td>{{ $model->payment_type }}</td>
            </tr>
                    <tr>
                <th class="text-right">Reff Number</th>
                <td>{{ $model->reff_number }}</td>
            </tr>
                    <tr>
                <th class="text-right">Sc Date</th>
                <td>{{ $model->sc_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Sc Number</th>
                <td>{{ $model->sc_number }}</td>
            </tr>
                    <tr>
                <th class="text-right">Total Value</th>
                <td>{{ $model->total_value }}</td>
            </tr>
                </table>
    </div>
@endsection