@extends("app")

@section("title", trans("action.create")." | Product Sales Contract")

@section("content")
    @include("product-sales-contract.form", [
        "model" => $model,
    ])
@endsection
