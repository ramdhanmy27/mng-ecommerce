@extends("app")

@section("title", trans("action.update")." | Guaranted Asset")

@section("content")
    @include("guaranted-asset.form", [
    	"model" => $model,
    ])
@endsection
