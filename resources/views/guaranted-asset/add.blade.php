@extends("app")

@section("title", trans("action.create")." | Guaranted Asset")

@section("content")
    @include("guaranted-asset.form", [
        "model" => $model,
    ])
@endsection
