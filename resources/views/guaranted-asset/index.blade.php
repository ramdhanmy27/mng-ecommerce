@extends("app")

@section("title", "Guaranted Asset")

@section("content")
    <a href="{{ url("guaranted-asset/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("guaranted-asset/data") }}">
        <thead>
            <tr>
                                    <th dt-field="asset_name"> Asset Name </th>
                                    <th dt-field="asset_no"> Asset No </th>
                                    <th dt-field="asset_value"> Asset Value </th>
                                    <th dt-field="avalist_name"> Avalist Name </th>
                                    <th dt-field="id"> Id </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("guaranted-asset/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("guaranted-asset/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
