@extends("app")

@section("title", "Guaranted Asset")

@section("content")
    <div class="row">
        <a href="{{ url("guaranted-asset") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("guaranted-asset/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("guaranted-asset/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Asset Name</th>
                <td>{{ $model->asset_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Asset No</th>
                <td>{{ $model->asset_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Asset Value</th>
                <td>{{ $model->asset_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Avalist Name</th>
                <td>{{ $model->avalist_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                </table>
    </div>
@endsection