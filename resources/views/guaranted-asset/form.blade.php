@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "asset_name") !!}
	{!! Form::group("text", "asset_no") !!}
	{!! Form::group("text", "asset_value") !!}
	{!! Form::group("text", "avalist_name") !!}
            
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}