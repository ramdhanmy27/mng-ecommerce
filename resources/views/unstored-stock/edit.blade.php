@extends("app")

@section("title", trans("action.update")." | Unstored Stock")

@section("content")
    @include("unstored-stock.form", [
    	"model" => $model,
    ])
@endsection
