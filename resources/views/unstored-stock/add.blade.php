@extends("app")

@section("title", trans("action.create")." | Unstored Stock")

@section("content")
    @include("unstored-stock.form", [
        "model" => $model,
    ])
@endsection
