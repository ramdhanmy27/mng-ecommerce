@extends("app")

@section("title", trans("action.update")." | Ready Product")

@section("content")
    @include("ready-product.form", [
    	"model" => $model,
    ])
@endsection
