@extends("app")

@section("title", trans("action.create")." | Ready Product")

@section("content")
    @include("ready-product.form", [
        "model" => $model,
    ])
@endsection
