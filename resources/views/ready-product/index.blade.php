@extends("app")

@section("title", "Ready Product")

@section("content")
    <a href="{{ url("ready-product/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("ready-product/data") }}">
        <thead>
            <tr>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="prod_name"> Prod Name </th>
                                    <th dt-field="total_qty"> Total Qty </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("ready-product/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("ready-product/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
