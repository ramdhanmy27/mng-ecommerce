@extends("app")

@section("title", trans("action.update")." | Loan Transaction")

@section("content")
    @include("loan-transaction.form", [
    	"model" => $model,
    ])
@endsection
