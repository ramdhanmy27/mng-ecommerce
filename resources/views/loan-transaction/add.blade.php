@extends("app")

@section("title", trans("action.create")." | Loan Transaction")

@section("content")
    @include("loan-transaction.form", [
        "model" => $model,
    ])
@endsection
