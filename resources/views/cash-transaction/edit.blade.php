@extends("app")

@section("title", trans("action.update")." | Cash Transaction")

@section("content")
    @include("cash-transaction.form", [
    	"model" => $model,
    ])
@endsection
