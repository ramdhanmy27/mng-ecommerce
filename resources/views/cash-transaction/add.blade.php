@extends("app")

@section("title", trans("action.create")." | Cash Transaction")

@section("content")
    @include("cash-transaction.form", [
        "model" => $model,
    ])
@endsection
