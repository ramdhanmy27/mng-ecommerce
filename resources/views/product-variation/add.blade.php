@extends("app")

@section("title", trans("action.create")." | Product Variation")

@section("content")
    @include("product-variation.form", [
        "model" => $model,
    ])
@endsection
