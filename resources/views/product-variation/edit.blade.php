@extends("app")

@section("title", trans("action.update")." | Product Variation")

@section("content")
    @include("product-variation.form", [
    	"model" => $model,
    ])
@endsection
