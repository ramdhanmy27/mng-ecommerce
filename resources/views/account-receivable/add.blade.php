@extends("app")

@section("title", trans("action.create")." | Account Receivable")

@section("content")
    @include("account-receivable.form", [
        "model" => $model,
    ])
@endsection
