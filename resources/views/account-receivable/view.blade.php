@extends("app")

@section("title", "Account Receivable")

@section("content")
    <div class="row">
        <a href="{{ url("account-receivable") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("account-receivable/delete/$model->ar_code") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("account-receivable/edit/$model->ar_code") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
            <tr>
                <th class="text-right">Ar Code</th>
                <td>{{ $model->ar_code }}</td>
            </tr>
            <tr>
                <th class="text-right">Ar Name</th>
                <td>{{ $model->ar_name }}</td>
            </tr>
            <tr>
                <th class="text-right">Balance</th>
                <td>{{ $model->balance }}</td>
            </tr>
            <tr>
                <th class="text-right">Curr</th>
                <td>{{ $model->curr }}</td>
            </tr>
        </table>
    </div>
@endsection