@extends("app")

@section("title", trans("action.update")." | Account Receivable")

@section("content")
    @include("account-receivable.form", [
    	"model" => $model,
    ])
@endsection
