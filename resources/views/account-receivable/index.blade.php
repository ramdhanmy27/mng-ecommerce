@extends("app")

@section("title", "Account Receivable")

@section("content")
    <a href="{{ url("account-receivable/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("account-receivable/data") }}">
        <thead>
            <tr>
                                    <th dt-field="ar_code"> Ar Code </th>
                                    <th dt-field="ar_name"> Ar Name </th>
                                    <th dt-field="balance"> Balance </th>
                                    <th dt-field="curr"> Curr </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("account-receivable/edit/[[ar_code]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("account-receivable/delete/[[ar_code]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
