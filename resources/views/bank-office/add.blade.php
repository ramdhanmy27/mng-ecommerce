@extends("app")

@section("title", trans("action.create")." | Bank Office")

@section("content")
    @include("bank-office.form", [
        "model" => $model,
    ])
@endsection
