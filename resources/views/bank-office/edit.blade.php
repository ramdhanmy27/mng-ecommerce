@extends("app")

@section("title", trans("action.update")." | Bank Office")

@section("content")
    @include("bank-office.form", [
    	"model" => $model,
    ])
@endsection
