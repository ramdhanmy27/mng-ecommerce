@extends("app")

@section("title", "Bank Office")

@section("content")
    <div class="row">
        <a href="{{ url("bank-office") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("bank-office/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("bank-office/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Address</th>
                <td>{{ $model->address }}</td>
            </tr>
                    <tr>
                <th class="text-right">Contact Person</th>
                <td>{{ $model->contact_person }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Phone</th>
                <td>{{ $model->phone }}</td>
            </tr>
                </table>
    </div>
@endsection