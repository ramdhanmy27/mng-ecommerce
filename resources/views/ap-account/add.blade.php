@extends("app")

@section("title", trans("action.create")." | Ap Account")

@section("content")
    @include("ap-account.form", [
        "model" => $model,
    ])
@endsection
