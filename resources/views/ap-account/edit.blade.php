@extends("app")

@section("title", trans("action.update")." | Ap Account")

@section("content")
    @include("ap-account.form", [
    	"model" => $model,
    ])
@endsection
