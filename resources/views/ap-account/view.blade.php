@extends("app")

@section("title", "Ap Account")

@section("content")
    <div class="row">
        <a href="{{ url("ap-account") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("ap-account/delete/$model->ap_code") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("ap-account/edit/$model->ap_code") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Ap Code</th>
                <td>{{ $model->ap_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Ap Name</th>
                <td>{{ $model->ap_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Balance</th>
                <td>{{ $model->balance }}</td>
            </tr>
                    <tr>
                <th class="text-right">Curr</th>
                <td>{{ $model->curr }}</td>
            </tr>
                </table>
    </div>
@endsection