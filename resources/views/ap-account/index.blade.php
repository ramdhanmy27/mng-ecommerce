@extends("app")

@section("title", "Ap Account")

@section("content")
    <a href="{{ url("ap-account/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("ap-account/data") }}">
        <thead>
            <tr>
                                    <th dt-field="ap_code"> Ap Code </th>
                                    <th dt-field="ap_name"> Ap Name </th>
                                    <th dt-field="balance"> Balance </th>
                                    <th dt-field="curr"> Curr </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("ap-account/edit/[[ap_code]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("ap-account/delete/[[ap_code]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
