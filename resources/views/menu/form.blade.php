@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "created_at") !!}
	{!! Form::group("text", "enable") !!}
	{!! Form::group("text", "order") !!}
	{!! Form::group("textarea", "param") !!}
	{!! Form::group("text", "parent") !!}
	{!! Form::group("text", "title") !!}
	{!! Form::group("text", "updated_at") !!}
	{!! Form::group("textarea", "url") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}