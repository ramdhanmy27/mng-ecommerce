@extends("app")

@section("title", "Menu")

@section("content")
    <div class="row">
        <a href="{{ url("menu") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("menu/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("menu/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Created At</th>
                <td>{{ $model->created_at }}</td>
            </tr>
                    <tr>
                <th class="text-right">Enable</th>
                <td>{{ $model->enable }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Order</th>
                <td>{{ $model->order }}</td>
            </tr>
                    <tr>
                <th class="text-right">Param</th>
                <td>{{ $model->param }}</td>
            </tr>
                    <tr>
                <th class="text-right">Parent</th>
                <td>{{ $model->parent }}</td>
            </tr>
                    <tr>
                <th class="text-right">Title</th>
                <td>{{ $model->title }}</td>
            </tr>
                    <tr>
                <th class="text-right">Updated At</th>
                <td>{{ $model->updated_at }}</td>
            </tr>
                    <tr>
                <th class="text-right">Url</th>
                <td>{{ $model->url }}</td>
            </tr>
                </table>
    </div>
@endsection