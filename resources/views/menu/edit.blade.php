@extends("app")

@section("title", trans("action.update")." | Menu")

@section("content")
    @include("menu.form", [
    	"model" => $model,
    ])
@endsection
