@extends("app")

@section("title", "Menu")

@section("content")
    <a href="{{ url("menu/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("menu/data") }}">
        <thead>
            <tr>
                                    <th dt-field="created_at"> Created At </th>
                                    <th dt-field="enable"> Enable </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="order"> Order </th>
                                    <th dt-field="param"> Param </th>
                                    <th dt-field="parent"> Parent </th>
                                    <th dt-field="title"> Title </th>
                                    <th dt-field="updated_at"> Updated At </th>
                                    <th dt-field="url"> Url </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("menu/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("menu/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
