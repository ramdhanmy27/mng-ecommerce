@extends("app")

@section("title", trans("action.create")." | Menu")

@section("content")
    @include("menu.form", [
        "model" => $model,
    ])
@endsection
