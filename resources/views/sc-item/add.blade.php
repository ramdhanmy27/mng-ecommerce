@extends("app")

@section("title", trans("action.create")." | Sc Item")

@section("content")
    @include("sc-item.form", [
        "model" => $model,
    ])
@endsection
