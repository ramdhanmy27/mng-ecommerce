@extends("app")

@section("title", trans("action.update")." | Sc Item")

@section("content")
    @include("sc-item.form", [
    	"model" => $model,
    ])
@endsection
