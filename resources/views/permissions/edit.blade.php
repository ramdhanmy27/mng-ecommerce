@extends("app")

@section("title", trans("action.update")." | Permissions")

@section("content")
    @include("permissions.form", [
    	"model" => $model,
    ])
@endsection
