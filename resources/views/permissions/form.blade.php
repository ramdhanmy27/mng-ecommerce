@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "created_at") !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "display_name") !!}
	{!! Form::group("text", "name") !!}
	{!! Form::group("text", "updated_at") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}