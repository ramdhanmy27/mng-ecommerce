@extends("app")

@section("title", trans("action.create")." | Permissions")

@section("content")
    @include("permissions.form", [
        "model" => $model,
    ])
@endsection
