@extends("app")

@section("title", trans("action.create")." | Ar Bill Detail")

@section("content")
    @include("ar-bill-detail.form", [
        "model" => $model,
    ])
@endsection
