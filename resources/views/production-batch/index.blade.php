@extends("app")

@section("title", "Production Batch")

@section("content")
    <a href="{{ url("production-batch/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("production-batch/data") }}">
        <thead>
            <tr>
                                    <th dt-field="batch_no"> Batch No </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="mfg_finish_date"> Mfg Finish Date </th>
                                    <th dt-field="mfg_start_date"> Mfg Start Date </th>
                                    <th dt-field="product_name"> Product Name </th>
                                    <th dt-field="total_qty"> Total Qty </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("production-batch/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("production-batch/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
