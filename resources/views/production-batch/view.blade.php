@extends("app")

@section("title", "Production Batch")

@section("content")
    <div class="row">
        <a href="{{ url("production-batch") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("production-batch/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("production-batch/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Batch No</th>
                <td>{{ $model->batch_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Mfg Finish Date</th>
                <td>{{ $model->mfg_finish_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Mfg Start Date</th>
                <td>{{ $model->mfg_start_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Product Name</th>
                <td>{{ $model->product_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Total Qty</th>
                <td>{{ $model->total_qty }}</td>
            </tr>
                </table>
    </div>
@endsection