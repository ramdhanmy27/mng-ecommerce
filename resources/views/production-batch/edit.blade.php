@extends("app")

@section("title", trans("action.update")." | Production Batch")

@section("content")
    @include("production-batch.form", [
    	"model" => $model,
    ])
@endsection
