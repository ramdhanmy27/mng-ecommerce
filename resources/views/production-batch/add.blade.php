@extends("app")

@section("title", trans("action.create")." | Production Batch")

@section("content")
    @include("production-batch.form", [
        "model" => $model,
    ])
@endsection
