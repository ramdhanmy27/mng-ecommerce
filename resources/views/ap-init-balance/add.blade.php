@extends("app")

@section("title", trans("action.create")." | Ap Init Balance")

@section("content")
    @include("ap-init-balance.form", [
        "model" => $model,
    ])
@endsection
