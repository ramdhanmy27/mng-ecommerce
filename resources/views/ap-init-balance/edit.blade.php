@extends("app")

@section("title", trans("action.update")." | Ap Init Balance")

@section("content")
    @include("ap-init-balance.form", [
    	"model" => $model,
    ])
@endsection
