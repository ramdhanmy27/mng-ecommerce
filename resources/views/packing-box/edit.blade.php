@extends("app")

@section("title", trans("action.update")." | Packing Box")

@section("content")
    @include("packing-box.form", [
    	"model" => $model,
    ])
@endsection
