@extends("app")

@section("title", trans("action.create")." | Packing Box")

@section("content")
    @include("packing-box.form", [
        "model" => $model,
    ])
@endsection
