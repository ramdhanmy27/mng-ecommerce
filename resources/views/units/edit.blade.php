@extends("app")

@section("title", trans("action.update")." | Units")

@section("content")
    @include("units.form", [
    	"model" => $model,
    ])
@endsection
