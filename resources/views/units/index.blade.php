@extends("app")

@section("title", "Units")

@section("content")
    <a href="{{ url("units/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("units/data") }}">
        <thead>
            <tr>
                                    <th dt-field="base_conversion"> Base Conversion </th>
                                    <th dt-field="dimension_type"> Dimension Type </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="unit_code"> Unit Code </th>
                                    <th dt-field="unit_name"> Unit Name </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("units/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("units/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
