@extends("app")

@section("title", trans("action.create")." | Units")

@section("content")
    @include("units.form", [
        "model" => $model,
    ])
@endsection
