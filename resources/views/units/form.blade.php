@section("input", true)

{!! Form::model($model) !!}
    {!! Form::group("text", "unit_code") !!}
    {!! Form::group("text", "unit_name") !!}
    {!! Form::group("select", "dimension_type", null, $model->config("type")) !!}
    {!! Form::group("text", "base_conversion") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}