<?php

use App\Models\Supplier;

?>

@section("input", true)

{!! Form::model($model) !!}
    <div class="form-group">
        <div class="col-md-6">
            {!! Form::group("text", "po_number") !!}
            {!! Form::group("select", "po_type", null, $model->config("type")) !!}
            {!! Form::group("date", "po_date") !!}
            {!! Form::group("date", "deliv_due_date") !!}
            {!! Form::group("textarea", "description") !!}
            
            <div class="form-group">
                <div class="col-md-offset-3 col-md-9">
                    {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {!! Form::group("textarea", "deliv_address") !!}
            {!! Form::group("select", "supplier_id", null, Supplier::lists("supp_name", "id")) !!}
            {!! Form::group("select", "pay_method", null, $model->config("method")) !!}
            {!! Form::group("select", "currency", null, config("opt.currency")) !!}
            {!! Form::group("text", "total_value") !!}
        </div>
    </div>
{!! Form::close() !!}