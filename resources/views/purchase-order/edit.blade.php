@extends("app")

@section("title", trans("action.update")." | ".trans("ref.wl.title"))

@section("content")
    @include("purchase-order.form", [
        "model" => $model,
    ])

    <hr>
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#item" data-toggle="tab">
                    <i class="fa fa-check"></i> PO Items
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="item">
                @include("purchase-order.list-item", [
                    "po_id" => $model->id,
                    "model" => new App\Models\POItems, 
                ])
            </div>
        </div>
    </div>
@endsection
