<a href="{{ url("po-items/$id/add/") }}" class="btn btn-primary" modal>
    <i class="fa fa-plus"></i> @lang("action.create")
</a>
<hr>

<table class="table table-bordered" datatable="{{ url("po-items/$id/data") }}">
    <thead>
        <tr>
            <th dt-field="item_name"> Item Name </th>
            <th dt-field="qty"> Qty </th>
            <th dt-field="unit_price"> Unit Price </th>
            <th dt-field="subtotal"> Sub Total </th>
            <th dt-col="#dt-action" sort="false" search="false"> </th>
        </tr>
    </thead>
    
    <dt-template>
        <div id="dt-action">
            <a href="{{ url("po-items/$id/edit/[[id]]") }}" modal
                tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
                <i class="fa fa-edit"></i>
            </a>
            <a href="{{ url("po-items/$id/delete/[[id]]") }}" 
                tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
                confirm="@lang("confirm.delete")">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </dt-template>
</table>