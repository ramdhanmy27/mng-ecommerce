@extends("app")

@section("title", trans("action.create")." | Purchase Order")

@section("content")
    @include("purchase-order.form", [
        "model" => $model,
    ])
@endsection
