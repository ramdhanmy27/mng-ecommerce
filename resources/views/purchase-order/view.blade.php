@extends("app")

@section("title", "Purchase Order")

@section("content")
    <div class="text-right">
        <a href="{{ url("purchase-order/edit/$model->id") }}" class="btn btn-warning">
            <i class="fa fa-edit"></i> Ubah
        </a>
        <a href="{{ url("purchase-order/delete/$model->id") }}" class="btn btn-danger"
            method="post" confirm="{{ trans("confirm.delete") }}">
            <i class="fa fa-trash"></i> Hapus
        </a>
        <a href="{{ url("purchase-order") }}" class="btn btn-primary">
            <i class="fa fa-reply"></i> Kembali
        </a>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("po_number") }}</b>
                </label>
                <div class="col-md-8">{{ $model->po_number }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("po_type") }}</b>
                </label>
                <div class="col-md-8">{{ $model->config("type.$model->po_type") }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("po_date") }}</b>
                </label>
                <div class="col-md-8">{{ $model->po_date }}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("deliv_due_date") }}</b>
                </label>
                <div class="col-md-8">{{ $model->deliv_due_date }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("description") }}</b>
                </label>
                <div class="col-md-8">{{ $model->description }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("deliv_address") }}</b>
                </label>
                <div class="col-md-8">{{ $model->deliv_address }}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("po_status") }}</b>
                </label>
                <div class="col-md-8">{{ $model->config("status.$model->po_status", "Open") }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("pay_method") }}</b>
                </label>
                <div class="col-md-8">{{ $model->config("method.$model->pay_method") }}</div>
            </div>
            <div class="row">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label("total_value") }}</b>
                </label>
                <div class="col-md-8">{{ $model->currency }} {{ $model->total_value }}</div>
            </div>
        </div>
    </div>

    {{-- PO Items --}}
    <?php $model = $po_items->first(); ?>
    <br>
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>{{ $model->label("item_name") }}</th>
                <th>{{ $model->label("unit_name") }}</th>
                <th>{{ $model->label("qty") }}</th>
                <th>{{ $model->label("unit_price") }}</th>
                <th>{{ $model->label("subtotal") }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($po_items as $item)
            <tr>
                <td>{{ $item->item_name }}</td>
                <td>{{ $item->unit_name }}</td>
                <td>{{ $item->qty }}</td>
                <td>{{ $item->unit_price }}</td>
                <td>{{ $item->subtotal }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection