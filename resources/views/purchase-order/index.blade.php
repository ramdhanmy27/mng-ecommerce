@extends("app")

@section("title", "Purchase Order")

@section("content")
    <a href="{{ url("purchase-order/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("purchase-order/data") }}">
        <thead>
            <tr>
                <th dt-field="po_number"> Number </th>
                <th dt-field="po_type"> Type </th>
                <th dt-field="po_date"> Date </th>
                <th dt-field="deliv_due_date"> Due Date </th>
                <th dt-field="total_value"> Total Value </th>
                <th dt-field="po_status"> Status </th>
                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
                <a href="{{ url("purchase-order/close/[[id]]") }}" 
                    tooltip="Close" class="btn btn-sm btn-default"
                    confirm="Apakah anda yakin ?">
                    <i class="fa fa-flag"></i>
                </a>
                <a href="{{ url("purchase-order/view/[[id]]") }}" 
                    tooltip="@lang("action.view")" class="btn btn-sm btn-info">
                    <i class="fa fa-eye"></i>
                </a>
	            <a href="{{ url("purchase-order/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("purchase-order/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
