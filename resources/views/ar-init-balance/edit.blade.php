@extends("app")

@section("title", trans("action.update")." | Ar Init Balance")

@section("content")
    @include("ar-init-balance.form", [
    	"model" => $model,
    ])
@endsection
