@extends("app")

@section("title", trans("action.update")." | Ar Bill")

@section("content")
    @include("ar-bill.form", [
    	"model" => $model,
    ])
@endsection
