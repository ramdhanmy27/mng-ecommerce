@extends("app")

@section("title", trans("action.create")." | Ar Bill")

@section("content")
    @include("ar-bill.form", [
        "model" => $model,
    ])
@endsection
