@extends("app")

@section("title", "Ar Bill")

@section("content")
    <div class="row">
        <a href="{{ url("ar-bill") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("ar-bill/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("ar-bill/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Bill Date</th>
                <td>{{ $model->bill_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Bill No</th>
                <td>{{ $model->bill_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Bill Value</th>
                <td>{{ $model->bill_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Due Date</th>
                <td>{{ $model->due_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Paid</th>
                <td>{{ $model->is_paid }}</td>
            </tr>
                </table>
    </div>
@endsection