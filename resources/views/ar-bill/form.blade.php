@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "bill_date") !!}
	{!! Form::group("text", "bill_no") !!}
	{!! Form::group("text", "bill_value") !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "due_date") !!}
	{!! Form::group("text", "is_paid") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}