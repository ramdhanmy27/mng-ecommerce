@extends("app")

@section("title", trans("action.create")." | Mat Trans Item")

@section("content")
    @include("mat-trans-item.form", [
        "model" => $model,
    ])
@endsection
