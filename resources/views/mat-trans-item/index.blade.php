@extends("app")

@section("title", "Mat Trans Item")

@section("content")
    <a href="{{ url("mat-trans-item/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("mat-trans-item/data") }}">
        <thead>
            <tr>
                                    <th dt-field="external_qty"> External Qty </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="pack_qty"> Pack Qty </th>
                                    <th dt-field="qty"> Qty </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("mat-trans-item/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("mat-trans-item/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
