@extends("app")

@section("title", trans("action.update")." | Mat Trans Item")

@section("content")
    @include("mat-trans-item.form", [
    	"model" => $model,
    ])
@endsection
