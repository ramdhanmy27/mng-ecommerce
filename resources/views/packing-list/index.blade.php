@extends("app")

@section("title", "Packing List")

@section("content")
    <a href="{{ url("packing-list/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("packing-list/data") }}">
        <thead>
            <tr>
                                    <th dt-field="delivery_date"> Delivery Date </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="total_qty"> Total Qty </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("packing-list/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("packing-list/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
