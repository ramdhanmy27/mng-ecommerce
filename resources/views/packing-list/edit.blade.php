@extends("app")

@section("title", trans("action.update")." | Packing List")

@section("content")
    @include("packing-list.form", [
    	"model" => $model,
    ])
@endsection
