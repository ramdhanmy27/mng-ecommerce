@extends("app")

@section("title", trans("action.create")." | Packing List")

@section("content")
    @include("packing-list.form", [
        "model" => $model,
    ])
@endsection
