@extends("app")

@section("title", trans("action.update")." | Roles")

@section("content")
    @include("roles.form", [
    	"model" => $model,
    ])
@endsection
