@extends("app")

@section("title", trans("action.create")." | Roles")

@section("content")
    @include("roles.form", [
        "model" => $model,
    ])
@endsection
