@extends("app")

@section("title", trans("action.create")." | Supplier Bill")

@section("content")
    @include("supplier-bill.form", [
        "model" => $model,
    ])
@endsection
