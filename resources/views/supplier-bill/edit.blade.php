@extends("app")

@section("title", trans("action.update")." | Supplier Bill")

@section("content")
    @include("supplier-bill.form", [
    	"model" => $model,
    ])
@endsection
