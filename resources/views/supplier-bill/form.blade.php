@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "attached_file") !!}
	{!! Form::group("text", "bill_date") !!}
	{!! Form::group("text", "bill_no") !!}
	{!! Form::group("text", "currency") !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "due_date") !!}
	{!! Form::group("text", "payment_type") !!}
	{!! Form::group("text", "total_value") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}