@extends("app")

@section("title", "Supplier Bill")

@section("content")
    <a href="{{ url("supplier-bill/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("supplier-bill/data") }}">
        <thead>
            <tr>
                                    <th dt-field="attached_file"> Attached File </th>
                                    <th dt-field="bill_date"> Bill Date </th>
                                    <th dt-field="bill_no"> Bill No </th>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="due_date"> Due Date </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="payment_type"> Payment Type </th>
                                    <th dt-field="total_value"> Total Value </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("supplier-bill/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("supplier-bill/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
