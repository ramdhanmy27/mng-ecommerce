@extends("app")

@section("title", "Cheque Book")

@section("content")
    <div class="row">
        <a href="{{ url("cheque-book") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("cheque-book/delete/$model->serial_no") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("cheque-book/edit/$model->serial_no") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Is Activated</th>
                <td>{{ $model->is_activated }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Bilyet</th>
                <td>{{ $model->is_bilyet }}</td>
            </tr>
                    <tr>
                <th class="text-right">Page Count</th>
                <td>{{ $model->page_count }}</td>
            </tr>
                    <tr>
                <th class="text-right">Serial No</th>
                <td>{{ $model->serial_no }}</td>
            </tr>
                </table>
    </div>
@endsection