@extends("app")

@section("title", "Cheque Book")

@section("content")
    <a href="{{ url("cheque-book/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("cheque-book/data") }}">
        <thead>
            <tr>
                                    <th dt-field="is_activated"> Is Activated </th>
                                    <th dt-field="is_bilyet"> Is Bilyet </th>
                                    <th dt-field="page_count"> Page Count </th>
                                    <th dt-field="serial_no"> Serial No </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("cheque-book/edit/[[serial_no]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("cheque-book/delete/[[serial_no]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
