@extends("app")

@section("title", trans("action.update")." | Cheque Book")

@section("content")
    @include("cheque-book.form", [
    	"model" => $model,
    ])
@endsection
