@extends("app")

@section("title", trans("action.create")." | Cheque Book")

@section("content")
    @include("cheque-book.form", [
        "model" => $model,
    ])
@endsection
