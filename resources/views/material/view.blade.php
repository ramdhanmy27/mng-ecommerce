@extends("app")

@section("title", "Material")

@section("content")
    <div class="row">
        <a href="{{ url("material") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("material/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("material/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Attr1 Value</th>
                <td>{{ $model->attr1_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr2 Value</th>
                <td>{{ $model->attr2_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr3 Value</th>
                <td>{{ $model->attr3_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr4 Value</th>
                <td>{{ $model->attr4_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Attr5 Value</th>
                <td>{{ $model->attr5_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Mat Code</th>
                <td>{{ $model->mat_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Mat Name</th>
                <td>{{ $model->mat_name }}</td>
            </tr>
                </table>
    </div>
@endsection