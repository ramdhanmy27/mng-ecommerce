@extends("app")

@section("title", trans("action.create")." | Material")

@section("content")
    @include("material.form", [
        "model" => $model,
    ])
@endsection
