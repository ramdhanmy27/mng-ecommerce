@extends("app")

@section("title", trans("action.update")." | Material")

@section("content")
    @include("material.form", [
    	"model" => $model,
    ])
@endsection
