<?php

use App\Models\MaterialGroup;
use App\Models\Units;

?>

@section("input", true)

{!! Form::model($model, ["id" => "material"]) !!}
    {!! Form::hidden("id", $model->id) !!}
    {!! Form::group("select", "grp_code", null, MaterialGroup::lists("grp_name", "grp_code")) !!}
    {!! Form::group("text", "mat_code", "Kode Barang") !!}
    {!! Form::group("text", "mat_name", "Nama Barang") !!}
    {!! Form::group("select", "unit_code", "Unit", Units::lists("unit_name", "unit_code")) !!}

    <div v-for="(item, name) in attributes" class="form-group">
        <label :for="name" class="col-md-3 control-label">@{{ item.label }}</label>
        <div class="col-md-6">
            <input type="text" class="form-control" :id="name" :name="name" :value="item.value" />
        </div>
    </div>

    {!! Form::group("textarea", "description", "Deskripsi") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}

@push("app-script")
<script type="text/javascript" src="{{ asset("js/lib/vue.min.js") }}"> </script>
<script type="text/javascript">
    var v = new Vue({
        el: "#material",
        data: {
            attributes: {},
        },
        methods: {}
    });

    $(document).ready(function() {
        $("[name='grp_code']").change(function() {
            var $this = $(this);

            Vue.set(v, "attributes", {});

            $.ajax({
                url: fn.url("service/data/material-group"),
                data: {
                    id: $this.val(),
                    material_id: $("[name='id']").val(),
                },
                type: "POST",
                dataType: "json",
                success: function(data) {
                    Vue.nextTick(function() {
                        v.attributes = {};
                        console.log(data)

                        for (var i = 1; i <= 5; i++) {
                            if (!fn.empty(data["attr"+i+"_name"]))
                                v.attributes["attr"+i+"_value"] = {
                                    label: data["attr"+i+"_name"],
                                    value: data["attr"+i+"_value"]
                                }
                        }
                    })
                }
            })
        })
        .trigger("change")
    })
</script>
@endpush