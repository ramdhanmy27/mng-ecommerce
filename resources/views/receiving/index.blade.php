@extends("app")

@section("title", "Receiving")

@section("content")
    <a href="{{ url("receiving/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("receiving/data") }}">
        <thead>
            <tr>
                <th dt-field="rcv_number"> Number </th>
                <th dt-field="rcv_date"> Date </th>
                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
                <a href="{{ url("receiving/view/[[id]]") }}" 
                    tooltip="@lang("action.view")" class="btn btn-sm btn-info">
                    <i class="fa fa-eye"></i>
                </a>
	            <a href="{{ url("receiving/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("receiving/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
