<a href="{{ url("receive-item/$id/add") }}" class="btn btn-primary" modal>
    <i class="fa fa-plus"></i> @lang("action.create")
</a>
<hr>

<table class="table table-bordered" datatable="{{ url("receive-item/$id/data") }}">
    <thead>
        <tr>
            <th dt-field="mat_name"> Item Name </th>
            <th dt-field="qty"> Qty </th>
            <th dt-field="unit_price"> Unit Price </th>
            <th dt-col="#dt-action" sort="false" search="false"> </th>
        </tr>
    </thead>
    
    <dt-template>
        <div id="dt-action">
            <a href="{{ url("receive-item/$id/edit/[[id]]") }}" modal
                tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
                <i class="fa fa-edit"></i>
            </a>
            <a href="{{ url("receive-item/$id/delete/[[id]]") }}" 
                tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
                confirm="@lang("confirm.delete")">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </dt-template>
</table>