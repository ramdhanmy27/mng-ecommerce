<?php

use App\Models\PurchaseOrder;

?>

@section("input", true)

{!! Form::model($model) !!}
    {!! Form::group("select", "po_number", null, PurchaseOrder::lists("po_number", "po_number")) !!}
	{!! Form::group("text", "rcv_number") !!}
    {!! Form::group("date", "rcv_date") !!}
    {!! Form::group("textarea", "description") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}