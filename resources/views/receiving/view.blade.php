@extends("app")

@section("title", "Receiving")

@section("content")
    <div class="row text-right">
        <a href="{{ url("receiving/edit/$model->id") }}" class="btn btn-warning">
            <i class="fa fa-edit"></i> Ubah
        </a>
        <a href="{{ url("receiving/delete/$model->id") }}" class="btn btn-danger"
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus
        </a>
        <a href="{{ url("receiving") }}" class="btn btn-primary">
            <i class="fa fa-reply"></i> Kembali
        </a>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-4">
            <label class="col-md-4 control-label text-right">
                <b>{{ $model->label("rcv_number") }}</b>
            </label>
            <div class="col-md-8">
                <b>:</b> {{ $model->rcv_number }}
            </div>
        </div>
        <div class="col-md-4">
            <label class="col-md-4 control-label text-right">
                <b>{{ $model->label("rcv_date") }}</b>
            </label>
            <div class="col-md-8">
                <b>:</b> {{ Date::parse($model->rcv_date) }}
            </div>
        </div>
        <div class="col-md-4">
            <label class="col-md-4 control-label text-right">
                <b>{{ $model->label("description") }}</b>
            </label>
            <div class="col-md-8">
                <b>:</b> {{ $model->description }}
            </div>
        </div>
    </div>

    <br>
    <?php $item = $items->first(); ?>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr> 
                    <th>{{ $item->label("mat_code") }}</th> 
                    <th>{{ $item->label("mat_name") }}</th> 
                    <th>{{ $item->label("unit_name") }}</th> 
                    <th>{{ $item->label("unit_price") }}</th> 
                    <th>{{ $item->label("qty") }}</th> 
                    <th>{{ $item->label("total") }}</th> 
                </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr> 
                    <td>{{ $item->mat_code }}</td> 
                    <td>{{ $item->mat_name }}</td> 
                    <td>{{ $item->unit_name }}</td> 
                    <td>{{ currency($item->unit_price) }}</td> 
                    <td>{{ $item->qty }}</td> 
                    <td>{{ currency($item->qty * $item->unit_price) }}</td> 
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection