@extends("app")

@section("title", trans("action.update")." | Receiving")

@section("content")
    @include("receiving.form", [
    	"model" => $model,
    ])
    
    <hr>
    <div class="tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#item" data-toggle="tab">
                    <i class="fa fa-check"></i> Receive Items
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="item">
                @include("receiving.list-item", [
                    "rcv_number" => $model->rcv_number,
                    "model" => new App\Models\ReceiveItem, 
                ])
            </div>
        </div>
    </div>
@endsection
