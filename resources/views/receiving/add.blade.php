@extends("app")

@section("title", trans("action.create")." | Receiving")

@section("content")
    @include("receiving.form", [
        "model" => $model,
    ])
@endsection
