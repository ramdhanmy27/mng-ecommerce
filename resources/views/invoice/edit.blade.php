@extends("app")

@section("title", trans("action.update")." | Invoice")

@section("content")
    @include("invoice.form", [
    	"model" => $model,
    ])
@endsection
