@extends("app")

@section("title", trans("action.create")." | Invoice")

@section("content")
    @include("invoice.form", [
        "model" => $model,
    ])
@endsection
