@extends("app")

@section("title", "Invoice")

@section("content")
    <a href="{{ url("invoice/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("invoice/data") }}">
        <thead>
            <tr>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="due_date"> Due Date </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="invoice_date"> Invoice Date </th>
                                    <th dt-field="invoice_no"> Invoice No </th>
                                    <th dt-field="payment_bank_account"> Payment Bank Account </th>
                                    <th dt-field="payment_type"> Payment Type </th>
                                    <th dt-field="total_value"> Total Value </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("invoice/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("invoice/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
