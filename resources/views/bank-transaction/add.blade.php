@extends("app")

@section("title", trans("action.create")." | Bank Transaction")

@section("content")
    @include("bank-transaction.form", [
        "model" => $model,
    ])
@endsection
