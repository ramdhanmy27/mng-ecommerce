@extends("app")

@section("title", trans("action.update")." | Bank Transaction")

@section("content")
    @include("bank-transaction.form", [
    	"model" => $model,
    ])
@endsection
