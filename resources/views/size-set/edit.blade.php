@extends("app")

@section("title", trans("action.update")." | Size Set")

@section("content")
    @include("size-set.form", [
    	"model" => $model,
    ])
@endsection
