@extends("app")

@section("title", trans("action.create")." | Size Set")

@section("content")
    @include("size-set.form", [
        "model" => $model,
    ])
@endsection
