@extends("app")

@section("title", trans("action.create")." | Wh Stock")

@section("content")
    @include("wh-stock.form", [
        "model" => $model,
    ])
@endsection
