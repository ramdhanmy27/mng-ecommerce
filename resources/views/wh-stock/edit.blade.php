@extends("app")

@section("title", trans("action.update")." | Wh Stock")

@section("content")
    @include("wh-stock.form", [
    	"model" => $model,
    ])
@endsection
