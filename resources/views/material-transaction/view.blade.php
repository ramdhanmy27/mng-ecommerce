@extends("app")

@section("title", "Material Transaction")

@section("content")
    <div class="row">
        <a href="{{ url("material-transaction") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("material-transaction/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("material-transaction/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Debet</th>
                <td>{{ $model->is_debet }}</td>
            </tr>
                    <tr>
                <th class="text-right">Reff No</th>
                <td>{{ $model->reff_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Trans Date</th>
                <td>{{ $model->trans_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Trans No</th>
                <td>{{ $model->trans_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Trans Type</th>
                <td>{{ $model->trans_type }}</td>
            </tr>
                </table>
    </div>
@endsection