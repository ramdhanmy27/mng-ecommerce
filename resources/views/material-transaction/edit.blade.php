@extends("app")

@section("title", trans("action.update")." | Material Transaction")

@section("content")
    @include("material-transaction.form", [
    	"model" => $model,
    ])
@endsection
