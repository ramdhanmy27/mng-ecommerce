@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "is_debet") !!}
	{!! Form::group("text", "reff_no") !!}
	{!! Form::group("text", "trans_date") !!}
	{!! Form::group("text", "trans_no") !!}
	{!! Form::group("text", "trans_type") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}