@extends("app")

@section("title", trans("action.create")." | Material Transaction")

@section("content")
    @include("material-transaction.form", [
        "model" => $model,
    ])
@endsection
