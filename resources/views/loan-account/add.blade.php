@extends("app")

@section("title", trans("action.create")." | Loan Account")

@section("content")
    @include("loan-account.form", [
        "model" => $model,
    ])
@endsection
