@extends("app")

@section("title", "Loan Account")

@section("content")
    <a href="{{ url("loan-account/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("loan-account/data") }}">
        <thead>
            <tr>
                                    <th dt-field="account_name"> Account Name </th>
                                    <th dt-field="account_no"> Account No </th>
                                    <th dt-field="balance"> Balance </th>
                                    <th dt-field="currency"> Currency </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("loan-account/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("loan-account/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
