@extends("app")

@section("title", trans("action.update")." | Loan Account")

@section("content")
    @include("loan-account.form", [
    	"model" => $model,
    ])
@endsection
