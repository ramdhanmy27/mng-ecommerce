@extends("app")

@section("title", "Loan Account")

@section("content")
    <div class="row">
        <a href="{{ url("loan-account") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("loan-account/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("loan-account/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Account Name</th>
                <td>{{ $model->account_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Account No</th>
                <td>{{ $model->account_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Balance</th>
                <td>{{ $model->balance }}</td>
            </tr>
                    <tr>
                <th class="text-right">Currency</th>
                <td>{{ $model->currency }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                </table>
    </div>
@endsection