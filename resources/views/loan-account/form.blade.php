@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "account_name") !!}
	{!! Form::group("text", "account_no") !!}
	{!! Form::group("text", "balance") !!}
	{!! Form::group("text", "currency") !!}
	{!! Form::group("text", "description") !!}
            
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}