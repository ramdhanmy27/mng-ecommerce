@extends("app")

@section("title", trans("action.create")." | Ar Transaction")

@section("content")
    @include("ar-transaction.form", [
        "model" => $model,
    ])
@endsection
