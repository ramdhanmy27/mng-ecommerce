@extends("app")

@section("title", trans("action.update")." | Ar Transaction")

@section("content")
    @include("ar-transaction.form", [
    	"model" => $model,
    ])
@endsection
