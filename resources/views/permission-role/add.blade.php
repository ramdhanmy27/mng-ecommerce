@extends("app")

@section("title", trans("action.create")." | Permission Role")

@section("content")
    @include("permission-role.form", [
        "model" => $model,
    ])
@endsection
