@extends("app")

@section("title", "Permission Role")

@section("content")
    <div class="row">
        <a href="{{ url("permission-role") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("permission-role/delete/$model->role_id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("permission-role/edit/$model->role_id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Permission Id</th>
                <td>{{ $model->permission_id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Role Id</th>
                <td>{{ $model->role_id }}</td>
            </tr>
                </table>
    </div>
@endsection