@extends("app")

@section("title", trans("action.update")." | Permission Role")

@section("content")
    @include("permission-role.form", [
    	"model" => $model,
    ])
@endsection
