@extends("app")

@section("title", "Rack Slot")

@section("content")
    <a href="{{ url("rack-slot/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("rack-slot/data") }}">
        <thead>
            <tr>
                                    <th dt-field="capacity"> Capacity </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="shelf_level"> Shelf Level </th>
                                    <th dt-field="slot_code"> Slot Code </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("rack-slot/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("rack-slot/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
