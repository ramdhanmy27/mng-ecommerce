@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "capacity") !!}
	{!! Form::group("text", "shelf_level") !!}
	{!! Form::group("text", "slot_code") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}