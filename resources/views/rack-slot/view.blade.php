@extends("app")

@section("title", "Rack Slot")

@section("content")
    <div class="row">
        <a href="{{ url("rack-slot") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("rack-slot/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("rack-slot/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Capacity</th>
                <td>{{ $model->capacity }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Shelf Level</th>
                <td>{{ $model->shelf_level }}</td>
            </tr>
                    <tr>
                <th class="text-right">Slot Code</th>
                <td>{{ $model->slot_code }}</td>
            </tr>
                </table>
    </div>
@endsection