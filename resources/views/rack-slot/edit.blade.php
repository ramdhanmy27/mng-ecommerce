@extends("app")

@section("title", trans("action.update")." | Rack Slot")

@section("content")
    @include("rack-slot.form", [
    	"model" => $model,
    ])
@endsection
