@extends("app")

@section("title", trans("action.create")." | Rack Slot")

@section("content")
    @include("rack-slot.form", [
        "model" => $model,
    ])
@endsection
