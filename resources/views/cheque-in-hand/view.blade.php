@extends("app")

@section("title", "Cheque In Hand")

@section("content")
    <div class="row">
        <a href="{{ url("cheque-in-hand") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("cheque-in-hand/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("cheque-in-hand/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Account Owner</th>
                <td>{{ $model->account_owner }}</td>
            </tr>
                    <tr>
                <th class="text-right">Cheque Date</th>
                <td>{{ $model->cheque_date }}</td>
            </tr>
                    <tr>
                <th class="text-right">Cheque Value</th>
                <td>{{ $model->cheque_value }}</td>
            </tr>
                    <tr>
                <th class="text-right">Giver Name</th>
                <td>{{ $model->giver_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Bilyet</th>
                <td>{{ $model->is_bilyet }}</td>
            </tr>
                    <tr>
                <th class="text-right">Serial No</th>
                <td>{{ $model->serial_no }}</td>
            </tr>
                    <tr>
                <th class="text-right">Status</th>
                <td>{{ $model->status }}</td>
            </tr>
                </table>
    </div>
@endsection