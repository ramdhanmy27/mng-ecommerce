@extends("app")

@section("title", trans("action.create")." | Cheque In Hand")

@section("content")
    @include("cheque-in-hand.form", [
        "model" => $model,
    ])
@endsection
