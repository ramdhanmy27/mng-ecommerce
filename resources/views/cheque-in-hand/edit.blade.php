@extends("app")

@section("title", trans("action.update")." | Cheque In Hand")

@section("content")
    @include("cheque-in-hand.form", [
    	"model" => $model,
    ])
@endsection
