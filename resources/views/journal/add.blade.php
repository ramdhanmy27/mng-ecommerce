@extends("app")

@section("title", trans("action.create")." | Journal")

@section("content")
    @include("journal.form", [
        "model" => $model,
    ])
@endsection
