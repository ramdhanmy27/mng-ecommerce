@extends("app")

@section("title", trans("action.update")." | Journal")

@section("content")
    @include("journal.form", [
    	"model" => $model,
    ])
@endsection
