@extends("app")

@section("title", "Components")

@section("content")
    <a href="{{ url("components/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("components/data") }}">
        <thead>
            <tr>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="is_has_detail"> Is Has Detail </th>
                                    <th dt-field="is_helper_material"> Is Helper Material </th>
                                    <th dt-field="partname"> Partname </th>
                                    <th dt-field="required_qty"> Required Qty </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("components/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("components/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
