@extends("app")

@section("title", trans("action.create")." | Components")

@section("content")
    @include("components.form", [
        "model" => $model,
    ])
@endsection
