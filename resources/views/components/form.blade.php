@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "is_has_detail") !!}
	{!! Form::group("text", "is_helper_material") !!}
	{!! Form::group("text", "partname") !!}
	{!! Form::group("text", "required_qty") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}