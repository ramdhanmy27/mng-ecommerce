@extends("app")

@section("title", "Components")

@section("content")
    <div class="row">
        <a href="{{ url("components") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("components/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("components/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Has Detail</th>
                <td>{{ $model->is_has_detail }}</td>
            </tr>
                    <tr>
                <th class="text-right">Is Helper Material</th>
                <td>{{ $model->is_helper_material }}</td>
            </tr>
                    <tr>
                <th class="text-right">Partname</th>
                <td>{{ $model->partname }}</td>
            </tr>
                    <tr>
                <th class="text-right">Required Qty</th>
                <td>{{ $model->required_qty }}</td>
            </tr>
                </table>
    </div>
@endsection