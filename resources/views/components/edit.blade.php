@extends("app")

@section("title", trans("action.update")." | Components")

@section("content")
    @include("components.form", [
    	"model" => $model,
    ])
@endsection
