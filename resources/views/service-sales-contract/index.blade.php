@extends("app")

@section("title", "Service Sales Contract")

@section("content")
    <a href="{{ url("service-sales-contract/add") }}" class="btn btn-primary">
        <i class="fa fa-plus"></i> @lang("action.create")
    </a>
    <hr>

    <table class="table table-bordered" datatable="{{ url("service-sales-contract/data") }}">
        <thead>
            <tr>
                                    <th dt-field="attached_file"> Attached File </th>
                                    <th dt-field="buyer_name"> Buyer Name </th>
                                    <th dt-field="description"> Description </th>
                                    <th dt-field="due_date"> Due Date </th>
                                    <th dt-field="id"> Id </th>
                                    <th dt-field="reff_number"> Reff Number </th>
                                    <th dt-field="sc_date"> Sc Date </th>
                                    <th dt-field="sc_number"> Sc Number </th>
                                    <th dt-field="scope_of_work"> Scope Of Work </th>
                                    <th dt-field="total_value"> Total Value </th>
                                <th dt-col="#dt-action" sort="false" search="false"> </th>
            </tr>
        </thead>
        
	    <dt-template>
	        <div id="dt-action">
	            <a href="{{ url("service-sales-contract/edit/[[id]]") }}" 
                    tooltip="@lang("action.update")" class="btn btn-sm btn-warning">
	                <i class="fa fa-edit"></i>
	            </a>
	            <a href="{{ url("service-sales-contract/delete/[[id]]") }}" 
                    tooltip="@lang("action.delete")" class="btn btn-sm btn-danger"
	            	confirm="@lang("confirm.delete")">
	                <i class="fa fa-trash"></i>
	            </a>
	        </div>
	    </dt-template>
    </table>
@endsection
