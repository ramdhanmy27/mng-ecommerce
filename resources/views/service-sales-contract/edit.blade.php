@extends("app")

@section("title", trans("action.update")." | Service Sales Contract")

@section("content")
    @include("service-sales-contract.form", [
    	"model" => $model,
    ])
@endsection
