@extends("app")

@section("title", trans("action.create")." | Service Sales Contract")

@section("content")
    @include("service-sales-contract.form", [
        "model" => $model,
    ])
@endsection
