@extends("app")

@section("title", "Bank Account")

@section("content")
    <div class="row">
        <a href="{{ url("bank-account") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("bank-account/delete/$model->account_code") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("bank-account/edit/$model->account_code") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Account Code</th>
                <td>{{ $model->account_code }}</td>
            </tr>
                    <tr>
                <th class="text-right">Account Name</th>
                <td>{{ $model->account_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Account Number</th>
                <td>{{ $model->account_number }}</td>
            </tr>
                    <tr>
                <th class="text-right">Currency</th>
                <td>{{ $model->currency }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                </table>
    </div>
@endsection