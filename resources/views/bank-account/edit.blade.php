@extends("app")

@section("title", trans("action.update")." | Bank Account")

@section("content")
    @include("bank-account.form", [
    	"model" => $model,
    ])
@endsection
