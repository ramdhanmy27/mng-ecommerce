@extends("app")

@section("title", trans("action.create")." | Bank Account")

@section("content")
    @include("bank-account.form", [
        "model" => $model,
    ])
@endsection
