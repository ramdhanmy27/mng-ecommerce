@extends("app")

@section("title", "Brand")

@section("content")
    <div class="row">
        <a href="{{ url("brand") }}" class="btn btn-primary mt-sm pull-right">
            <i class="fa fa-reply"></i> Kembali        </a>

        <a href="{{ url("brand/delete/$model->id") }}" 
            class="btn btn-tertiary mt-sm ml-sm pull-right" 
            method="post" confirm="{{ trans("action.confirm") }}">
            <i class="fa fa-trash"></i> Hapus        </a>

        <a href="{{ url("brand/edit/$model->id") }}" 
            class="btn btn-secondary mt-sm ml-sm pull-right">
            <i class="fa fa-edit"></i> Ubah        </a>
    </div>
    <hr>

    <div class="table-responsive">
        <table class="table table-border table-striped">
                    <tr>
                <th class="text-right">Id</th>
                <td>{{ $model->id }}</td>
            </tr>
                    <tr>
                <th class="text-right">Brand Name</th>
                <td>{{ $model->brand_name }}</td>
            </tr>
                    <tr>
                <th class="text-right">Description</th>
                <td>{{ $model->description }}</td>
            </tr>
                </table>
    </div>
@endsection