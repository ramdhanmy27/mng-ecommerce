@extends("app")

@section("title", trans("action.create")." | Brand")

@section("content")
    @include("brand.form", [
        "model" => $model,
    ])
@endsection
