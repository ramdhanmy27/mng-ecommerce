@extends("app")

@section("title", trans("action.update")." | Brand")

@section("content")
    @include("brand.form", [
    	"model" => $model,
    ])
@endsection
