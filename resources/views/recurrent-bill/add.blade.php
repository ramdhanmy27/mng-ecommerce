@extends("app")

@section("title", trans("action.create")." | Recurrent Bill")

@section("content")
    @include("recurrent-bill.form", [
        "model" => $model,
    ])
@endsection
