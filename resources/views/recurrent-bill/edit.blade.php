@extends("app")

@section("title", trans("action.update")." | Recurrent Bill")

@section("content")
    @include("recurrent-bill.form", [
    	"model" => $model,
    ])
@endsection
