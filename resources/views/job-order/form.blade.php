@section("input", true)

{!! Form::model($model) !!}
	{!! Form::group("text", "description") !!}
	{!! Form::group("text", "job_date") !!}
	{!! Form::group("text", "job_no") !!}
	{!! Form::group("text", "qty") !!}
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
        </div>
    </div>
{!! Form::close() !!}