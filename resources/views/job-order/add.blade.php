@extends("app")

@section("title", trans("action.create")." | Job Order")

@section("content")
    @include("job-order.form", [
        "model" => $model,
    ])
@endsection
