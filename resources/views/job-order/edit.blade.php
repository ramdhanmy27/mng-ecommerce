@extends("app")

@section("title", trans("action.update")." | Job Order")

@section("content")
    @include("job-order.form", [
    	"model" => $model,
    ])
@endsection
