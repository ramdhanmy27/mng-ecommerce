@extends("app")

@section("title", trans("action.create")." | Bill Component")

@section("content")
    @include("bill-component.form", [
        "model" => $model,
    ])
@endsection
