@extends("app")

@section("title", trans("action.update")." | Bill Component")

@section("content")
    @include("bill-component.form", [
    	"model" => $model,
    ])
@endsection
