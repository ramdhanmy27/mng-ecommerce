<?php

return [
    "lob" => [
        "title" => "Line of Business",
        "column" => [
            "code" => "Line of Business Code",
            "name" => "Line of Business Name",
        ],
    ],
    "lo" => [
        "title" => "Level Organization",
        "common" => [
            "active" => "Active"
        ],
    	"column" => [
    		"code" => "Level Organization Code",
            "name" => "Level Organization Name",
            "parent_id" => "Level Organization Base",
            "lotype_id" => "Level Organization Type",
            "location_id" => "Location",
    		"is_active" => "Status"
    	]
    ],
    "lot" => [
        "title" => "Level Organization Type",
    	"column" => [
    		"code" => "Level Organization Type Code",
    		"name" => "Level Organization Type Name",
            "parent_id" => "Level"
    	]
    ],

    "l" => [
        "column" => [
            "code" => "Location Code",
            "name" => "Location Name",
            "no" => "Number",
            "address" => "Address",
            "zip_code" => "Zip Code",
            "is_active" => "Is this location active?",
            "company_information_id" => "Company Information",
            "company_level_id" => "Company Level",
            "company_sector_id" => "Company Sector",
            "regency_id" => "Regency",
            "country_id" => "Country",
            "province_id" => "Province",
            "line_of_business_id" => "Line of Business"
        ]
    ],
    "wl" => [
        "title" => "Work Location",
        "column" => [
            "name" => "Location Name",
            "code" => "Location Code",
            "is_active" => "Active",
            "tax_name" => "Tax Name",
            "tax_number" => "Tax Number",
            "company_information_id" => "Company",
        ],
    ],
    "factory" => [
        "title" => "Factory",
        "column" => [
            "name" => "Factory Name",
            "code" => "Factory Code",
            "address" => "Address",
            "email" => "Email",
            "fax" => "Fax",
            "phone" => "Phone",
            "zipcode" => "Zip Code",
            "work_location_id" => "Work Location",
            "country_id" => "Country",
            "province_id" => "Province",
            "regency_id" => "Regency",
            "district_id" => "District",
            "village_id" => "Village",
        ],
    ],
    "ci" => [
        "title" => "Company Information",
        "column" => [
            "address" => "Address",
            "alias_name" => "Official Name",
            "code" => "Company Code",
            "email" => "Email",
            "fax" => "Fax",
            "name" => "Company Name",
            "mission" => "Mission",
            "phone" => "Phone",
            "vision" => "Vision",
            "zipcode" => "Zip Code",
            "line_of_business_id" => "Line of Business",
            "currency_id" => "Currency",
            "country_id" => "Country",
            "province_id" => "Province",
            "regency_id" => "Regency",
            "district_id" => "District",
            "village_id" => "Village",

            "logo" => "Company Logo",
        ],
    ],
    "ei" => [
        "title" => "Employee Information",
        "common" => [
            "contact_info" => "Contact Information",
            "bank_info" => "Bank Information",
            "education_info" => "Education Information",
            "id_info" => "ID Information",
            "employee_info" => "Employee Information",
            "men" => "Men",
            "women" => "Women",
        ],
        "column" => [
            "employee" => [
                "employee_id" => "Employee ID",
                "name" => "Name",
                "status" => "Status",
                "ethnic" => "Ethnic",
                "birth_place" => "Birth Place",
                "birth_date" => "Birth Date",
                "photo" => "Photo",
                "gender" => "Gender",
                "join_date" => "Join Date",
                "marital_status" => "Marital Status",
                "npwp_date" => "NPWP Date",
                "npwp_no" => "NPWP Number",
                "pay_calc_method" => "Pay Calculation Method",
                "pay_slip" => "Pay Slip",
                "religion" => "Religion",
                "tax_method" => "Tax Method",
                "tax_status" => "Tax Status",
                "tax_type" => "Tax Type",
                "workday" => "Workday",
                "address" => "Address",
                "email" => "E-mail",
                "is_same_address" => "Is this same address?",
                "status_owner" => "Ownership Status",
                "stay_status" => "Residence Status",
                "zip_code" => "Zip Code",
                "country_code" => "Country",
                "province_code" => "Province",
                "regency_code" => "Regency",
                "district_code" => "District",
                "village_code" => "Village",
            ],
            "bank_info" => [
                "account_name" => "Account Name",
                "account_no" => "Account Number",
                "is_default" => "Is this default?",
                "no" => "Number",
                "saving_type" => "Saving Type",
            ],
            "contact_info" => [
            ],
            "education_info" => [
                "certificate_date" => "Certificate Date",
                "certificate_attach" => "Certificate Attachment",
                "certificate_no" => "Certificate Number",
                "degree" => "Degree",
                "education" => "Education",
                "start_year" => "Year Began",
                "end_year" => "Year Ends",
                "gpa" => "GPA",
                "institution" => "Institution",
                "is_certificate" => "Is any certificate?",
                "major" => "Major",
                "scale" => "Scale",
            ],
            "id_info" => [
                "exp_date" => "Expired Date",
                "doc_attach" => "Document Attachment",
                "id_number" => "ID Number",
                "type_id" => "ID Type",
            ]
        ]
    ]
];
