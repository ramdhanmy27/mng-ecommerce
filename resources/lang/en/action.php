<?php

return [
	"view" => "View",
	"update" => "Update",
	"delete" => "Delete",
	"select" => "Select",
    "save" => "Save",
    "create" => "New",

    // confirmation
    "yes" => "Yes",
    "no" => "No",

    // account
    "logout" => "Log Out",
];