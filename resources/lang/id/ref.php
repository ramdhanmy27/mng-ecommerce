<?php

return [
    // Organization
    "lob" => [
        "title" => "Sektor Usaha",
        "column" => [
            "code" => "Kode Sektor Usaha",
            "name" => "Nama Sektor Usaha",
        ],
    ],
    "lo" => [
        "title" => "Tingkatan Organisasi",
        "common" => [
            "active" => "Aktif"
        ],
    	"column" => [
    		"code" => "Kode Tingkatan Organisasi",
            "name" => "Nama Tingkatan Organisasi",
            "parent_id" => "Dasar Tingkatan Organisasi",
            "lotype_id" => "Tipe Tingkatan Organisasi",
            "location_id" => "Lokasi",
            "is_active" => "Status"
    	]
    ],
    "lot" => [
        "title" => "Tipe Tingkatan Organisasi",
        "column" => [
            "code" => "Kode Tipe Level Organisasi",
            "name" => "Nama Tipe Level Organisasi",
            "parent_id" => "Tingkatan",
        ]
    ],
    "wl" => [
        "title" => "Lokasi Kerja",
        "column" => [
            "name" => "Nama Lokasi",
            "code" => "No Lokasi",
            "is_active" => "Aktif",
            "tax_name" => "Nama Data Pajak",
            "tax_number" => "No Data Pajak",
            "company_information_id" => "Perusahaan",
        ],
    ],
    "factory" => [
        "title" => "Factory",
        "column" => [
            "name" => "Nama Factory",
            "code" => "Kode Factory",
            "address" => "Alamat",
            "email" => "Email",
            "fax" => "Fax",
            "phone" => "Telepon",
            "zipcode" => "Kode Pos",
            "work_location_id" => "Lokasi Kerja",
            "country_id" => "Negara",
            "province_id" => "Provinsi",
            "regency_id" => "Kota/Kabupaten",
            "district_id" => "Kecamatan",
            "village_id" => "Desa/Kelurahan",
        ],
    ],
    "ci" => [
        "title" => "Info Perusahaan",
        "column" => [
            "address" => "Alamat",
            "alias_name" => "Nama Official",
            "code" => "Kode Perusahaan",
            "email" => "Email",
            "fax" => "Fax",
            "name" => "Nama Perusahaan",
            "mission" => "Misi",
            "phone" => "Telepon",
            "vision" => "Visi",
            "zipcode" => "Kode Pos",
            "line_of_business_id" => "Sektor Usaha",
            "currency_id" => "Mata Uang",
            "country_id" => "Negara",
            "province_id" => "Provinsi",
            "regency_id" => "Kota/Kabupaten",
            "district_id" => "Kecamatan",
            "village_id" => "Desa/Kelurahan",

            "logo" => "Logo Perusahaan",
        ],
    ],

    "l" => [
        "column" => [
            "code" => "Kode Lokasi",
            "name" => "Nama Lokasi",
            "no" => "Nomor",
            "address" => "Alamat",
            "zip_code" => "Kode Pos",
            "is_active" => "Apakah lokasi ini aktif?",
            "company_information_id" => "Informasi Perusahaan",
            "company_level_id" => "Tingkatan Perusahaan",
            "company_sector_id" => "Sektor Perusahaan",
            "regency_id" => "Kota",
            "country_id" => "Negara",
            "province_id" => "Provinsi",
            "line_of_business_id" => "Garis Bisnis"
        ]
    ],

    // Employee
    "ei" => [
        "title" => "Informasi Pegawai",
        "common" => [
            "contact_info" => "Informasi Kontak",
            "bank_info" => "Informasi Bank",
            "education_info" => "Informasi Pendidikan",
            "id_info" => "Informasi Identitas",
            "employee_info" => "Informasi Pegawai",
            "men" => "Laki-laki",
            "women" => "Perempuan"
        ],
        "column" => [
            "employee" => [
                "employee_id" => "ID Employee",
                "name" => "Nama",
                "status" => "Status",
                "ethnic" => "Etnis",
                "birth_place" => "Tempat Lahir",
                "birth_date" => "Tanggal Lahir",
                "photo" => "Foto",
                "gender" => "Jenis Kelamin",
                "join_date" => "Tanggal Bergabung",
                "marital_status" => "Status Perkawinan",
                "npwp_date" => "Tanggal NPWP",
                "npwp_no" => "Nomor NPWP",
                "pay_calc_method" => "Metode Perhitungan Gaji",
                "pay_slip" => "Slip Gaji",
                "religion" => "Agama",
                "tax_method" => "Metode Pajak",
                "tax_status" => "Status Pajak",
                "tax_type" => "Tipe Pajak",
                "workday" => "Hari Kerja",
                "address" => "Alamat",
                "email" => "Alamat Surat Elektronik",
                "is_same_address" => "Apakah ini alamat yang sama?",
                "status_owner" => "Status Kepemilikan",
                "stay_status" => "Status Kediaman",
                "zip_code" => "Kode Pos",
                "country_code" => "Negara",
                "province_code" => "Provinsi",
                "regency_code" => "Kota/Kabupaten",
                "district_code" => "Kecamatan",
                "village_code" => "Desa/Kelurahan",
            ],
            "bank_info" => [
                "account_name" => "Nama Akun",
                "account_no" => "Nomor Akun",
                "is_default" => "Apakah ini default?",
                "no" => "Nomor",
                "saving_type" => "Tipe Penyimpanan",
            ],
            "contact_info" => [
            ],
            "education_info" => [
                "certificate_date" => "Tanggal Sertifikat",
                "certificate_attach" => "Lampiran Sertifikat",
                "certificate_no" => "Nomor Sertifikat",
                "degree" => "Gelar",
                "education" => "Pendidikan",
                "start_year" => "Tahun Mulai",
                "end_year" => "Tahun Selesai",
                "gpa" => "IPK",
                "institution" => "Institusi",
                "is_certificate" => "Apakah ini berupa sertifikat?",
                "major" => "Jurusan",
                "scale" => "Skala",
            ],
            "id_info" => [
                "exp_date" => "Tanggal Kadaluarsa",
                "doc_attach" => "Lampiran Dokumen",
                "id_number" => "Nomor ID",
                "type_id" => "Tipe ID",
            ]
        ]
    ]
];
