<?php

return [
	// Bank Group
	'bank_group' => [
		'code' => 'Kode',
		'name' => 'Nama Grup',
	],

	// Bank
	'bank' => [
		'branch' => 'Cabang',
		'code' => 'Kode Bank',
		'name' => 'Nama Bank',
		'address' => 'Alamat',
		'bank_group' => 'Grup Bank',
		'zip_code' => 'Kode Pos',
		'regency_code' => 'Kota/Kab',
		'bank_group_code' => 'Grup Bank',
	],
];