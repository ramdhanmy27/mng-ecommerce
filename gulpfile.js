var fs = require('fs');
var path = require('path');
var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

/** Core Assets */
elixir(function(mix) {
    mix
        // application theme
        .less('all.less', "./resources/assets/css/tmp/all.css")
        .styles([
            "theme/theme.css",
            "theme/skin.css",

            // all less
            "tmp/all.css",
        ], "public/css/app.css")
        .scripts([
            "ui/jquery.dataTables.min.js",
            "theme/theme.js",

            // App JS
            "fn.js",
            "angular.init.js",
            "app/event.js",
            "app/init.js",
        ], "public/js/app.js")
        
        // vendor
        .styles([
            "bootstrap.min.css",
            "font-awesome.min.css",
            "ui/jquery.dataTables.min.css",
            "ui/datatables.css",
            "ui/pnotify.custom.css",
        ], "public/css/vendor.css")
        .scripts([
            // "vue.min.js",
            "jquery.min.js",
            "bootstrap.min.js",
            // "angular.min.js",
            "theme/nanoscroller.js",
            "theme/modernizr.js",
            "ui/pnotify.custom.js",
        ], "public/js/vendor.js")

        // form input
        .styles([
            "input/datepicker3.css",
            "input/bootstrap-timepicker.min.css",
            "input/dropzone/dropzone.css",
            "input/bootstrap-fileupload.min.css",
            "input/select2.css",
        ], "public/css/input.css")
        .scripts([
            "input/validator.min.js",
            "input/bootstrap-maxlength.js",
            "input/bootstrap-datepicker.js",
            "input/bootstrap-timepicker.js",
            "input/bootstrap-fileupload.min.js",
            "input/dropzone.min.js",
            "input/select2.min.js",
            "input/ios7-switch.js",
        ], "public/js/input.js")
});

/** Themes */
var themes = ["backend", "front"];

// execute theme's gulp task
for (var i in themes) {
	// compile gulp themes
	// require("./resources/theme/"+ themes[i] +"/gulpfile.js")();
}

/** Modular Assets */
var mlixir = require('./app/Apps/modulixir');

elixir(function(mix) {
	var dir = "app/Apps";

	// collecting module directories
	fs.readdirSync(dir)
		.filter(function(file) {
			return fs.statSync(path.join(dir, file)).isDirectory();
		})
		.map(function(module) {
			try {
				var gulpfile = path.join(dir, module, "gulpfile.js");
				var gulp_exists = fs.statSync(gulpfile).isFile();

				// execute custom gulpfile per module
				if (gulp_exists) 
					require("./"+ gulpfile)();
			}
			catch(err) {}

			// custom gulpfile does not exists
			// copy all assets into public/app
			if (!gulp_exists)
				mix.copy(
					path.join(dir, module, "assets"), 
					path.join("./public/app", mlixir.camel2id(module))
				);
		});
});